<?php 
if(isset($_GET["empresa"])){
	$id_empresa = $_GET["empresa"];
    $query_em = mysqli_query ($conn,"select * from empresa where id_empresa = '$id_empresa'");
	while($row = mysqli_fetch_array($query_em)){
		$nombre_em = $row["nombre_empresa"];
		$razon_em = $row["razon"];
		$logo_em = $row["logo"];
	}
}

?>
<!-- MODAL ACTUALIZAR EMPRESA-->
					<div id="modal_up_empresa" class="modal fade" role="dialog">
                    <div class="modal-dialog">
					<!-- Modal content-->
                    <div class="modal-content">
                    <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Actualizar Empresa <?php echo $nombre_em;?></h4> </div>
                    <div class="modal-body">
                    <form class="cliente" name="contact" enctype="multipart/form-data" id="parametro" method="post" action="accion_empresa.php">
		            <div class="row">
		            <div class="col-md-9">
		            <div class="form-group">
			        <input type="text" name="nombre" class="form-control" placeholder="Nombre" value="<?php echo $nombre_em; ?>">
		            </div>
		            
	                <div class="form-group">
			        <input type="text" name="razon" class="form-control" placeholder="Razon Social" value="<?php echo $razon_em;?>">
		            </div>
					
					<div class="form-group">
<div class="col-md-16">
<div class="input-group">
                <span class="input-group-btn">
                    <span class="btn btn-default btn-file">
					<span class="glyphicon glyphicon-folder-open"></span>
                    <b>&nbsp;Logo</b>                   
						<input type="file" name="logo" id="logo"/>
                    </span>
                </span>
                <input type="text" class="form-control" readonly>
            </div>
</div>
</div>

					</div>
					</div></div>
                    <div class="modal-footer">
	  	            <div class="col-md-6 text-left col-md-offset-7">
					<input type="hidden" name="id_empresa" value="<?php echo $id_empresa;?>"/>
	                <button class="btn btn-primary" type="submit" value="Guardar" name="up_empresa"><i class="glyphicon glyphicon-check"></i> Guardar</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
		            </form>
					</div>
					</div>
					</div>
					</div>
					</div>
					
<script type="text/javascript">


//introducir logo formulario
$(document).on('change', '.btn-file :file', function() {
  var input = $(this),
      numFiles = input.get(0).files ? input.get(0).files.length : 1,
      label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
  input.trigger('fileselect', [numFiles, label]);
});

$(document).ready( function() {
    $('.btn-file :file').on('fileselect', function(event, numFiles, label) {
        
        var input = $(this).parents('.input-group').find(':text'),
            log = numFiles > 1 ? numFiles + ' files selected' : label;
        
        if( input.length ) {
            input.val(log);
        } else {
            if( log ) alert(log);
        }
        
    });
});



</script>

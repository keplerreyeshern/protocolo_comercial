<?php

//asignar datos del usuario si se accede a este archivo dinamicamente
include 'set.php';
$error = "";

if(isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == 1){
 $id_usuario = $_SESSION['id_usuario'];
 $nombre = $_SESSION['nombre_usuario'];
 $tipo_usuario = $_SESSION['tipo_usuario'] ;
 $empresa_usuario =$_SESSION['empresa_usuario'];
 $id_empresa = $_SESSION['id_empresa'];
}

else {
	session_start();
	$id_usuario = $_SESSION['id_usuario'];
	$nombre = $_SESSION['nombre_usuario'];
	$tipo_usuario = $_SESSION['tipo_usuario'] ;
	$empresa_usuario =$_SESSION['empresa_usuario'];
	$id_empresa = $_SESSION['id_empresa'];
}

//consultas de busqueda normal y busqueda dinamica
if(isset($_GET['s']) || isset($_GET['d_search'])){

$search = $_GET['s'];

//si el search esta vacio, asigna variable de search dinamico
if(!isset($search) && isset($_GET['d_search'])){$search = $_GET['d_search'];}

//echo "soy usuario ".$tipo_usuario."<b>esto es lo que estoy buscando: </b>".$search;

if($tipo_usuario == 1 && $id_empresa == 1){
$sql1 = mysqli_query($conn,"select * from cliente where nombre like '%$search%' order by nombre");
$sql1a = mysqli_query($conn,"select * from cliente where razon like '%$search%' order by razon");
$sql1b = mysqli_query($conn,"select * from cliente where rfc like '%$search%' order by rfc");
$sqlr = mysqli_query($conn,"select * from cliente where oficina like '%$search%' order by oficina");
$sql1c = mysqli_query($conn,"select * from cliente where clave_telefono like '%$search%' order by clave_telefono");
$sql1d = mysqli_query($conn,"select * from general_cliente where contactos like '%$search%' order by contactos");
$sql1e = mysqli_query($conn,"select * from general_cliente where calle like '%$search%'order by calle");
$sql1f = mysqli_query($conn,"select * from general_cliente where interno like '%$search%' order by interno");
$sql1g = mysqli_query($conn,"select * from general_cliente where externo like '%$search%' order by externo");
$sql1h = mysqli_query($conn,"select * from general_cliente where colonia like '%$search%' order by colonia");
$sql1i = mysqli_query($conn,"select * from general_cliente where municipio like '%$search%' order by municipio");
$sql1j = mysqli_query($conn,"select * from general_cliente where estado like '%$search%' order by estado");
$sql1k = mysqli_query($conn,"select * from general_cliente where localidad like '%$search%' order by localidad");
$sql1l = mysqli_query($conn,"select * from general_cliente where postal like '%$search%' order by postal");
$sql1m = mysqli_query($conn,"select * from general_cliente where telefono like '%$search%' order by telefono");
$sql1n = mysqli_query($conn,"select * from general_cliente where email like '%$search%' order by email");
$sql2 = mysqli_query($conn,"select * from protocolo_cliente where descripcion like '%$search%'");
$sql2a = mysqli_query($conn,"select * from colaboradores where nombre like '%$search%'");
$sql2b = mysqli_query($conn,"select * from protocolo_cliente where slogan like '%$search%'");
$sql2c = mysqli_query($conn,"select * from protocolo_cliente where contestacion like '%$search%'");
$sql3 = mysqli_query($conn,"select * from mensajes where asunto like '%$search%' or contenido like '%$search%'");
$sql4 = mysqli_query($conn,"SELECT * FROM recordatorios WHERE asunto !=  'Por paquete' AND (contenido LIKE  '%$search%' OR asunto LIKE  '%$search%')UNION SELECT * FROM recordatorios WHERE asunto ='Por paquete'AND cliente LIKE '%$search%' order by fecha asc");
}

if(($tipo_usuario == 1 && $id_empresa !=1) | ( $tipo_usuario == 2 | $tipo_usuario == 3)){
$sql1 = mysqli_query($conn,"select * from cliente where nombre like '%$search%' and id_empresa='$id_empresa' order by nombre");
$sql1a = mysqli_query($conn,"select * from cliente where razon like '%$search%' and id_empresa='$id_empresa' order by razon ");
$sql1b = mysqli_query($conn,"select * from cliente where rfc like '%$search%' and id_empresa='$id_empresa' order by rfc ");
$sqlr = mysqli_query($conn,"select * from cliente where oficina like '%$search%' and id_empresa='$id_empresa' order by oficina");
$sql1c = mysqli_query($conn,"select * from cliente where clave_telefono like '%$search%' and id_empresa='$id_empresa' order by clave_telefono ");
$sql1d = mysqli_query($conn,"select * from general_cliente where contactos like '%$search%' and id_empresa='$id_empresa'");
$sql1e = mysqli_query($conn,"select * from general_cliente where calle like '%$search%' and id_empresa='$id_empresa'order by calle");
$sql1f = mysqli_query($conn,"select * from general_cliente where interno like '%$search%' and id_empresa='$id_empresa' order by interno");
$sql1g = mysqli_query($conn,"select * from general_cliente where externo like '%$search%' and id_empresa='$id_empresa' order by externo");
$sql1h = mysqli_query($conn,"select * from general_cliente where colonia like '%$search%' and id_empresa='$id_empresa' order by colonia");
$sql1i = mysqli_query($conn,"select * from general_cliente where municipio like '%$search%' and id_empresa='$id_empresa' order by municipio");
$sql1j = mysqli_query($conn,"select * from general_cliente where estado like '%$search%' and id_empresa='$id_empresa' order by estado");
$sql1k = mysqli_query($conn,"select * from general_cliente where localidad like '%$search%' and id_empresa='$id_empresa' order by localidad");
$sql1l = mysqli_query($conn,"select * from general_cliente where postal like '%$search%' and id_empresa='$id_empresa' order by postal");
$sql1m = mysqli_query($conn,"select * from general_cliente where telefono like '%$search%' and id_empresa='$id_empresa' order by telefono");
$sql1n = mysqli_query($conn,"select * from general_cliente where email like '%$search%' and id_empresa='$id_empresa' order by email");
$sql2 = mysqli_query($conn,"select * from protocolo_cliente where descripcion like '%$search%' and id_empresa='$id_empresa'");
$sql2a = mysqli_query($conn,"select * from colaboradores where nombre like '%$search%' and id_empresa='$id_empresa'");
$sql2b = mysqli_query($conn,"select * from protocolo_cliente where slogan like '%$search%' and id_empresa='$id_empresa'");
$sql2c = mysqli_query($conn,"select * from protocolo_cliente where contestacion like '%$search%' and id_empresa='$id_empresa'");
$sql3 = mysqli_query($conn,"select * from mensajes where (asunto like '%$search%' or contenido like '%$search%') and id_empresa='$id_empresa'");
$sql4 = mysqli_query($conn,"SELECT * FROM recordatorios WHERE asunto != 'Por paquete' and id_empresa='$id_empresa' AND (contenido LIKE  '%$search%' OR asunto LIKE  '%$search%') UNION SELECT * FROM recordatorios WHERE asunto =  'Por paquete' and id_empresa='$id_empresa' AND cliente LIKE  '%$search%' order by fecha asc");
}

				//muestra los resultados de busqueda dinamicamente en formato html
 				if(isset($_GET['d_search'])){

					echo '<table class="table table-hover table-search">';

					//opciones para exportar clientes

					  echo '<tr><td><form action="exportar.php" method="post">
					  <select name="exporta_cliente">
            <option value="0">TODOS</option>';
              $sqlsg = mysqli_query($conn,"select * FROM grupos WHERE id_empresa='$id_empresa' AND nombre_grupo!='' ORDER BY id ASC");
              	while($sqg = mysqli_fetch_array($sqlsg)){
								  echo "<option value=".$sqg['id'].">".$sqg['nombre_grupo']."</option>";
              }
					  echo '</select>
					  <input type="hidden" name="empresa_clientes" value="'.$empresa_usuario.'"/>
					  <input type="hidden" name="id_empresa_clientes" value="'.$id_empresa.'"/>
					  <input type="hidden" name="modificado_por" value="'.$nombre.'"/>

					  <button type="submit" class="btn btn-success btn-xs" name="exportacli">
					  <i class="glyphicon glyphicon-export"></i> Exportar clientes</button>
					  </form>
					  </td></tr>';

					 if (mysqli_num_rows($sql1)!= ""){
						 echo '<tr><td>	<button type="button" class="btn btn-xs btn-primary icono" aria-label="Left Align">
				  <i class="glyphicon glyphicon-user"></i>
                     </button> <b>Clientes</b></td></tr>' ;

					 while($r_cli = mysqli_fetch_array($sql1)) {

                        echo '<tr><td><a class="hallazgo" href="brm.php?cl='.$r_cli['id_cliente'].'&carp=cli&file=datos">'.$r_cli['nombre'].' - ';
						if($r_cli['estatus']== "si" or $r_cli['estatus']== "Si"){
							echo '<u>Activo</u></a></td></tr>';
						}

						if($r_cli['estatus']== "no" or $r_cli['estatus']== "No"){
							echo '<u>Inactivo</u></a></td></tr>';
						}else{

							$busqueda_estatus = $r_cli['estatus'];
							$rquerybus_estatus = mysqli_query($conn, "SELECT * FROM estatus WHERE id = '$busqueda_estatus'");
								while($bus_estatus = mysqli_fetch_array($rquerybus_estatus)){
									echo '<u>'.$bus_estatus['nombre_estatus'].'</u></a></td></tr>';
								}
						}
					 }
					 }
						 if (mysqli_num_rows($sql1a)!= ""){
						 echo '<tr><td>	<button type="button" class="btn btn-xs btn-primary icono" aria-label="Left Align">
				  <i class="glyphicon glyphicon-user"></i>
                     </button> <b>Razon Social</b></td></tr>' ;

					  while($r_cli2 = mysqli_fetch_array($sql1a)) {

						echo '<tr><td><a class="hallazgo" href="brm.php?cl='.$r_cli2['id_cliente'].'&carp=cli&file=datos">'.$r_cli2['razon'].' - <u>'.$r_cli2['nombre'].'</u></a></td></tr>';
						}

						 }
                     if (mysqli_num_rows($sql1b)!= ""){
						 echo '<tr><td>	<button type="button" class="btn btn-xs btn-primary icono" aria-label="Left Align">
				  <i class="glyphicon glyphicon-user"></i>
                     </button> <b>Número RFC</b></td></tr>' ;

					  while($r_cli3 = mysqli_fetch_array($sql1b)) {

						echo '<tr><td><a class="hallazgo" href="brm.php?cl='.$r_cli3['id_cliente'].'&carp=cli&file=datos">'.$r_cli3['rfc'].' - <u>'.$r_cli3['nombre'].'</u></a></td></tr>';
						}

						 }

						 if (mysqli_num_rows($sqlr)!= ""){
						 echo '<tr><td>	<button type="button" class="btn btn-xs btn-primary icono" aria-label="Left Align">
				  <i class="glyphicon glyphicon-user"></i>
                     </button> <b>Oficina</b></td></tr>' ;

					  while($r_clir = mysqli_fetch_array($sqlr)) {

						echo '<tr><td><a class="hallazgo" href="brm.php?cl='.$r_clir['id_cliente'].'&carp=cli&file=datos">'.$r_clir['oficina'].' - <u>'.$r_clir['nombre'].'</u></a></td></tr>';
						}

						 }

                         if (mysqli_num_rows($sql1c)!= ""){
						 echo '<tr><td>	<button type="button" class="btn btn-xs btn-primary icono" aria-label="Left Align">
				  <i class="glyphicon glyphicon-user"></i>
                     </button> <b>Clave Telefono</b></td></tr>' ;

					  while($r_cli4 = mysqli_fetch_array($sql1c)) {

						echo '<tr><td><a class="hallazgo" href="brm.php?cl='.$r_cli4['id_cliente'].'&carp=cli&file=datos">'.$r_cli4['clave_telefono'].' - <u>'.$r_cli4['nombre'].'</u></a></td></tr>';
						}

						 }

					 if (mysqli_num_rows($sql1d)!= ""){
							 echo '<tr><td>	<button type="button" class="btn btn-xs btn-primary icono" aria-label="Left Align">
				  <i class="glyphicon glyphicon-user"></i>
						</button> <b>Personas Contacto</b></td></tr>' ;
					    while($r_cli5 = mysqli_fetch_array($sql1d)) {
						$id_cli5 = $r_cli5['id_cliente'];
						$sql1d1 = mysqli_query($conn,"select nombre from cliente where id_cliente='$id_cli5'");
						$rsql1d1 = mysqli_fetch_assoc($sql1d1);
						$cli5 = $rsql1d1['nombre'];
						echo '<tr><td><a class="hallazgo" href="brm.php?cl='.$r_cli5['id_cliente'].'&carp=cli&file=local">'.$r_cli5['contactos'].' - <u>'.$cli5.'</a></td></tr>';

						}
					 }

					  if (mysqli_num_rows($sql1e)!= ""){
							 echo '<tr><td>	<button type="button" class="btn btn-xs btn-primary icono" aria-label="Left Align">
				  <i class="glyphicon glyphicon-user"></i>
						</button> <b>Calle</b></td></tr>' ;
					    while($r_cli6 = mysqli_fetch_array($sql1e)) {
						$id_cli6 = $r_cli6['id_cliente'];
						$sql1e1 = mysqli_query($conn,"select nombre from cliente where id_cliente='$id_cli6'");
						$rsql1e1 = mysqli_fetch_assoc($sql1e1);
						$cli6 = $rsql1e1['nombre'];
						echo '<tr><td><a class="hallazgo" href="brm.php?cl='.$r_cli6['id_cliente'].'&carp=cli&file=local">'.$r_cli6['calle'].' - <u>'.$cli6.'</a></td></tr>';

						}
					 }

					 if (mysqli_num_rows($sql1f)!= ""){
							 echo '<tr><td>	<button type="button" class="btn btn-xs btn-primary icono" aria-label="Left Align">
				  <i class="glyphicon glyphicon-user"></i>
						</button> <b>Número Interno</b></td></tr>' ;
					    while($r_cli7 = mysqli_fetch_array($sql1f)) {
						$id_cli7 = $r_cli7['id_cliente'];
						$sql1f1 = mysqli_query($conn,"select nombre from cliente where id_cliente='$id_cli7'");
						$rsql1f1 = mysqli_fetch_assoc($sql1f1);
						$cli7 = $rsql1f1['nombre'];
						echo '<tr><td><a class="hallazgo" href="brm.php?cl='.$r_cli7['id_cliente'].'&carp=cli&file=local">'.$r_cli7['interno'].' - <u>'.$cli7.'</a></td></tr>';

						}
					 }

					 if (mysqli_num_rows($sql1g)!= ""){
							 echo '<tr><td>	<button type="button" class="btn btn-xs btn-primary icono" aria-label="Left Align">
				  <i class="glyphicon glyphicon-user"></i>
						</button> <b>Número Externo</b></td></tr>' ;
					    while($r_cli8 = mysqli_fetch_array($sql1g)) {
						$id_cli8 = $r_cli8['id_cliente'];
						$sql1g1 = mysqli_query($conn,"select nombre from cliente where id_cliente='$id_cli8'");
						$rsql1g1 = mysqli_fetch_assoc($sql1g1);
						$cli8 = $rsql1g1['nombre'];
						echo '<tr><td><a class="hallazgo" href="brm.php?cl='.$r_cli8['id_cliente'].'&carp=cli&file=local">'.$r_cli8['externo'].' - <u>'.$cli8.'</a></td></tr>';

						}
					 }

					 if (mysqli_num_rows($sql1h)!= ""){
							 echo '<tr><td>	<button type="button" class="btn btn-xs btn-primary icono" aria-label="Left Align">
				  <i class="glyphicon glyphicon-user"></i>
						</button> <b>Colonia</b></td></tr>' ;
					    while($r_cli9 = mysqli_fetch_array($sql1h)) {
						$id_cli9 = $r_cli9['id_cliente'];
						$sql1h1 = mysqli_query($conn,"select nombre from cliente where id_cliente='$id_cli9'");
						$rsql1h1 = mysqli_fetch_assoc($sql1h1);
						$cli9 = $rsql1h1["nombre"];
						echo '<tr><td><a class="hallazgo" href="brm.php?cl='.$r_cli9['id_cliente'].'&carp=cli&file=local">'.$r_cli9['colonia'].' - <u>'.$cli9.'</a></td></tr>';

						}
					 }
					 if (mysqli_num_rows($sql1i)!= ""){
							 echo '<tr><td>	<button type="button" class="btn btn-xs btn-primary icono" aria-label="Left Align">
				  <i class="glyphicon glyphicon-user"></i>
						</button> <b>Municipio</b></td></tr>' ;
					    while($r_cli10 = mysqli_fetch_array($sql1i)) {
						$id_cli10 = $r_cli10['id_cliente'];
						$sql1i1 = mysqli_query($conn,"select nombre from cliente where id_cliente='$id_cli10'");
						$rsql1i1 = mysqli_fetch_assoc($sql1i1);
						$cli10 = $rsql1i1["nombre"];
						echo '<tr><td><a class="hallazgo" href="brm.php?cl='.$r_cli10['id_cliente'].'&carp=cli&file=local">'.$r_cli10['municipio'].' - <u>'.$cli10.'</a></td></tr>';

						}
					 }

					  if (mysqli_num_rows($sql1j)!= ""){
							 echo '<tr><td>	<button type="button" class="btn btn-xs btn-primary icono" aria-label="Left Align">
				  <i class="glyphicon glyphicon-user"></i>
						</button> <b>Estado</b></td></tr>' ;
					    while($r_cli11 = mysqli_fetch_array($sql1j)) {
						$id_cli11 = $r_cli11['id_cliente'];
						$sql1j1 = mysqli_query($conn,"select nombre from cliente where id_cliente='$id_cli11'");
						$rsql1j1 = mysqli_fetch_assoc($sql1j1);
						$cli11 = $rsql1j1["nombre"];
						echo '<tr><td><a class="hallazgo" href="brm.php?cl='.$r_cli11['id_cliente'].'&carp=cli&file=local">'.$r_cli11['estado'].' - <u>'.$cli11.'</a></td></tr>';

						}
					 }

					 if (mysqli_num_rows($sql1k)!= ""){
							 echo '<tr><td>	<button type="button" class="btn btn-xs btn-primary icono" aria-label="Left Align">
				  <i class="glyphicon glyphicon-user"></i>
						</button> <b>Localidad</b></td></tr>' ;
					    while($r_cli12 = mysqli_fetch_array($sql1k)) {
						$id_cli12 = $r_cli12['id_cliente'];
						$sql1k1 = mysqli_query($conn,"select nombre from cliente where id_cliente='$id_cli12'");
						$rsql1k1 = mysqli_fetch_assoc($sql1k1);
						$cli12 = $rsql1k1["nombre"];
						echo '<tr><td><a class="hallazgo" href="brm.php?cl='.$r_cli12['id_cliente'].'&carp=cli&file=local">'.$r_cli12['localidad'].' - <u>'.$cli12.'</a></td></tr>';

						}
					 }

					  if (mysqli_num_rows($sql1l)!= ""){
							 echo '<tr><td>	<button type="button" class="btn btn-xs btn-primary icono" aria-label="Left Align">
				  <i class="glyphicon glyphicon-user"></i>
						</button> <b>Postal</b></td></tr>' ;
					    while($r_cli13 = mysqli_fetch_array($sql1l)) {
						$id_cli13 = $r_cli13['id_cliente'];
						$sql1l1 = mysqli_query($conn,"select nombre from cliente where id_cliente='$id_cli13'");
						$rsql1l1 = mysqli_fetch_assoc($sql1l1);
						$cli13 = $rsql1l1['nombre'];
						echo '<tr><td><a class="hallazgo" href="brm.php?cl='.$r_cli13['id_cliente'].'&carp=cli&file=local">'.$r_cli13['postal'].' - <u>'.$cli13.'</a></td></tr>';

						}
					 }

					if (mysqli_num_rows($sql1m)!= ""){
							 echo '<tr><td>	<button type="button" class="btn btn-xs btn-primary icono" aria-label="Left Align">
				  <i class="glyphicon glyphicon-user"></i>
						</button> <b>Teléfono</b></td></tr>' ;
					    while($r_cli14 = mysqli_fetch_array($sql1m)) {
						$id_cli14 = $r_cli14['id_cliente'];
						$sql1m1 = mysqli_query($conn,"select nombre from cliente where id_cliente='$id_cli14'");
						$rsql1m1 = mysqli_fetch_assoc($sql1m1);
						$cli14 = $rsql1m1["nombre"];
						echo '<tr><td><a class="hallazgo" href="brm.php?cl='.$r_cli14['id_cliente'].'&carp=cli&file=datos">'.$r_cli14['telefono'].' - <u>'.$cli14.'</a></td></tr>';

						}
					 }

                     if (mysqli_num_rows($sql1n)!= ""){
							 echo '<tr><td>	<button type="button" class="btn btn-xs btn-primary icono" aria-label="Left Align">
				  <i class="glyphicon glyphicon-user"></i>
						</button> <b>Email</b></td></tr>' ;
					    while($r_cli15 = mysqli_fetch_array($sql1n)) {
						$id_cli15 = $r_cli15['id_cliente'];
						$sql1n1 = mysqli_query($conn,"select nombre from cliente where id_cliente='$id_cli15'");
						$rsql1n1 = mysqli_fetch_assoc($sql1n1);
						$cli15 = $rsql1n1["nombre"];
						echo '<tr><td><a class="hallazgo" href="brm.php?cl='.$r_cli15['id_cliente'].'&carp=cli&file=local">'.$r_cli15['email'].' - <u>'.$cli15.'</a></td></tr>';

						}
					 }

					 if (mysqli_num_rows($sql2)!= ""){
						 echo '<tr><td>	<button type="button" class="btn btn-xs btn-primary icono" aria-label="Left Align">
				  <i class="glyphicon glyphicon-th-list"></i>
                     </button> <b>Tratamiento al cliente</b></td></tr>' ;

					 while($r_pro = mysqli_fetch_array($sql2)) {
						$id_cliente_pro =  $r_pro['id_cliente'];
						$r_pro1 = mysqli_query($conn,"select nombre from cliente where id_cliente = '$id_cliente_pro'");
                        $rr_pro1 = mysqli_fetch_assoc($r_pro1);
						$n_pro = $rr_pro1["nombre"];
                        echo '<tr><td><a class="hallazgo" href="brm.php?cl='.$r_pro['id_cliente'].'&carp=prot&file=trata">'.$r_pro['descripcion'].' - <u>'.$n_pro.'</a></td></tr>';

					 }}
					if (mysqli_num_rows($sql2a)!= ""){
						 echo '<tr><td>	<button type="button" class="btn btn-xs btn-primary icono" aria-label="Left Align">
				  <i class="glyphicon glyphicon-th-list"></i>
                     </button> <b>Colaboradores</b></td></tr>' ;

					 while($r_col = mysqli_fetch_array($sql2a)) {
						$id_cliente_col =  $r_col['id_cliente'];
						$r_col1 = mysqli_query($conn,"select nombre from cliente where id_cliente = '$id_cliente_col'");
                        $rr_col1 = mysqli_fetch_assoc($r_col1);
						$n_col = $rr_col1["nombre"];
						$col2 = $r_col['nombre'];

                        	 echo '<tr><td><a class="hallazgo" href="brm.php?cl='.$r_col['id_cliente'].'&carp=prot&file=cola">'.$col2. ' - <u>'.$n_col.'</a></td></tr>';

					 }}

					  if (mysqli_num_rows($sql2b)!= ""){
						 echo '<tr><td>	<button type="button" class="btn btn-xs btn-primary icono" aria-label="Left Align">
				  <i class="glyphicon glyphicon-th-list"></i>
                     </button> <b>Atributos</b></td></tr>' ;

					 while($r_atr = mysqli_fetch_array($sql2b)) {
						$id_cliente_atr =  $r_atr['id_cliente'];
						$r_atr1 = mysqli_query($conn,"select nombre from cliente where id_cliente = '$id_cliente_atr'");
                        $rr_atr1 = mysqli_fetch_assoc($r_atr1);
						$n_atr = $rr_atr1["nombre"];
                        echo '<tr><td><a class="hallazgo" href="brm.php?cl='.$r_atr['id_cliente'].'&carp=prot&file=slogan">'.$r_atr['slogan'].' - <u>'.$n_atr.'</a></td></tr>';

					 }}

					  if (mysqli_num_rows($sql2c)!= ""){
						 echo '<tr><td>	<button type="button" class="btn btn-xs btn-primary icono" aria-label="Left Align">
				  <i class="glyphicon glyphicon-th-list"></i>
                     </button> <b>Contestación Telefónica</b></td></tr>' ;

					 while($r_contes = mysqli_fetch_array($sql2c)) {
						$id_cliente_contes =  $r_contes['id_cliente'];
						$r_contes1 = mysqli_query($conn,"select nombre from cliente where id_cliente = '$id_cliente_contes'");
                        $rr_contes1 = mysqli_fetch_assoc($r_contes1);
						$n_contes = $rr_contes1['nombre'];
                        echo '<tr><td><a class="hallazgo" href="brm.php?cl='.$r_contes['id_cliente'].'&carp=prot&file=contes">'.$r_contes['contestacion'].' - <u>'.$n_contes.'</a></td></tr>';

					 }}

					 if (mysqli_num_rows($sql3) != ""){
						 echo '<tr><td>	<button type="button" class="btn btn-xs btn-primary icono" aria-label="Left Align">
				  <i class="glyphicon glyphicon-envelope"></i>
                     </button> <b>Mensajes</b></td></tr>' ;

					 while($r_msj = mysqli_fetch_array($sql3)) {

						$id_cliente_msj =  $r_msj['id_cliente'];
						$r_msj1 = mysqli_query($conn,"select nombre from cliente where id_cliente = '$id_cliente_msj'");
                        $rr_msj1 = mysqli_fetch_assoc($r_msj1);
						$n_msj = $rr_msj1['nombre'];
                        echo '<tr><td><a class="hallazgo" href="brm.php?cl='.$r_msj['id_cliente'].'&carp=msj&file='.$r_msj['id_mensaje'].'">'.$r_msj['asunto'].' - <u>'.$n_msj.'</a></td></tr>';

					 }}

					 if (mysqli_num_rows($sql4) != ""){
						 echo '<tr><td>	<button type="button" class="btn btn-xs btn-primary icono" aria-label="Left Align">
				  <i class="glyphicon glyphicon-calendar"></i>
                     </button> <b>Recordatorios</b></td></tr>' ;

					 while($r_rec = mysqli_fetch_array($sql4)) {
						 $id_cliente_rec =  $r_rec['id_cliente'];
						$r_rec1 = mysqli_query($conn,"select nombre from cliente where id_cliente = '$id_cliente_rec'");
                        $rr_rec1 = mysqli_fetch_assoc($r_rec1);
						$n_rec = $rr_rec1['nombre'];
                        echo '<tr><td><a class="hallazgo" href="brm.php?cl='.$r_rec['id_cliente'].'&carp=recor&file='.$r_rec['id_recordatorio'].'">Recordatorio '.$r_rec['id_recordatorio'].' - <u>'.$n_rec.'</a></td></tr>';

							}}
							echo "</table>";
						}


}


$sql5 = mysqli_query($conn,"select logo from empresa where id_empresa = '$id_empresa'");


if(isset($_GET['cl'])){
    $cliente = $_GET['cl'];
	$sql6 = mysqli_query($conn,"select nombre,razon from cliente where id_cliente='$cliente'");
}
if(isset($_GET['carp'])){
    $carpeta = $_GET['carp'];

	if(isset($cliente)){
	$sql7 = mysqli_query($conn,"select * from cliente where id_cliente='$cliente'");
	$sql8 = mysqli_query($conn,"select * from general_cliente where id_cliente='$cliente'");

	if($tipo_usuario == 1 && $id_empresa == 1){
	$sql9 = mysqli_query($conn,"select * from mensajes where id_cliente='$cliente'");
	$sql10= mysqli_query($conn,"select * from recordatorios where id_cliente='$cliente' order by fecha ASC, hora_exp ASC");
	}

	if(($tipo_usuario == 1 && $id_empresa != 1) | ($tipo_usuario == 2 || $tipo_usuario == 3)){
	$sql9 = mysqli_query($conn,"select * from mensajes where id_cliente='$cliente'");
	$sql10= mysqli_query($conn,"select * from recordatorios where id_cliente='$cliente' order by fecha ASC , hora_exp ASC");
	}
	/*
	if($tipo_usuario == 2){
	$sql9 = mysqli_query($conn,"select * from mensajes where id_usuario = '$id_usuario' and id_cliente='$cliente'");
	$sql10= mysqli_query($conn,"select * from recordatorios where id_usuario = '$id_usuario' and id_cliente='$cliente' order by fecha ASC, hora_exp ASC");
	}*/

	$sql12 = mysqli_query($conn,"select * from protocolo_cliente where id_cliente='$cliente'");
	$sql13 = mysqli_query($conn,"select * from cliente_archivos where id_cliente='$cliente'");
	}

    if($tipo_usuario == 1 && $id_empresa == 1){
	$sqlparam = mysqli_query($conn,"select distinct nombre_parametro,id_parametro from parametros order by nombre_parametro");
    }

	if(($tipo_usuario == 1 && $id_empresa !=1) | ( $tipo_usuario == 2 | $tipo_usuario == 3)){
	$sqlparam = mysqli_query($conn,"select distinct nombre_parametro,id_parametro from parametros where id_empresa = '$id_empresa' order by nombre_parametro");
	}

	 if($tipo_usuario == 1 && $id_empresa == 1){
	$sqlcompl = mysqli_query($conn,"select distinct nombre_complemento, id_complemento from complementos order by nombre_complemento");
    }

	if(($tipo_usuario == 1 && $id_empresa !=1) | ( $tipo_usuario == 2 | $tipo_usuario == 3)){
	$sqlcompl = mysqli_query($conn,"select distinct nombre_complemento, id_complemento from complementos where id_empresa = '$id_empresa' order by nombre_complemento");
	}

	if($tipo_usuario == 1 && $id_empresa == 1){
	$sqlale1 = mysqli_query($conn,"(SELECT id_cliente,id_mensaje, asunto, fecha, 'mensaje', `status` as hora_exp  FROM mensajes WHERE (`status`= 'no')) UNION (SELECT id_cliente, id_recordatorio, asunto, fecha,'recordatorio', hora_exp FROM recordatorios WHERE (`status`= 'no') ) ORDER BY fecha ASC, hora_exp ASC");
	}

	if(($tipo_usuario == 1 && $id_empresa !=1) || ( $tipo_usuario == 2 | $tipo_usuario == 3)){
	$sqlale1 = mysqli_query($conn,"(SELECT id_cliente,id_mensaje, asunto, fecha, 'mensaje', `status` as hora_exp FROM mensajes WHERE (id_empresa='$id_empresa' and `status`= 'no')) UNION  (SELECT id_cliente, id_recordatorio, asunto, fecha,'recordatorio',hora_exp FROM recordatorios WHERE (id_empresa='$id_empresa' and `status`= 'no'))ORDER BY fecha ASC, hora_exp ASC");
	//$sqlale1 = mysqli_query($conn,"select * from mensajes where status='no' and id_empresa='$id_empresa' order by fecha asc");
	//$sqlale2 = mysqli_query($conn,"select * from recordatorios where status='no' and id_empresa='$id_empresa' order by fecha asc");

   }
}

if(isset($_GET['file'])){
    $file = $_GET['file'];
		if(isset($cliente)){
	$sql14 = mysqli_query($conn,"select * from cliente where id_cliente='$cliente'");
	$sql15 = mysqli_query($conn,"select * from general_cliente where id_cliente='$cliente'");
	$sql16 = mysqli_query($conn,"select * from mensajes where id_cliente='$cliente' and id_mensaje='$file'");
	$sql17= mysqli_query($conn,"select  * from recordatorios where id_cliente='$cliente' and id_recordatorio='$file'");
	$sql18b = mysqli_query($conn,"SELECT * FROM referencias where id_parametro='$file' and id_cliente = '$cliente'  ORDER BY `orden` ASC");
	$sql19 = mysqli_query($conn,"select * from protocolo_cliente where id_cliente='$cliente'");
	$sql19_col = mysqli_query($conn,"select * from colaboradores where id_cliente='$cliente' order by orden");
	$sql20 = mysqli_query($conn,"select * from cliente_archivos where id_cliente='$cliente' and documentos <> ''");
	$sql20a = mysqli_query($conn,"select * from cliente_archivos where id_cliente='$cliente' and fotos <> ''");
	$sql31b = mysqli_query($conn,"SELECT * FROM referencias_complementos where id_complemento='$file' and id_cliente = '$cliente'  ORDER BY `orden` ASC");
		}

	$sql18 = mysqli_query($conn,"select * from parametros where id_parametro='$file'");
    $sql18c = mysqli_query($conn,"SELECT * FROM parametros where nombre_parametro='$file' and valor <> '' ORDER BY id_parametro ASC");
	$sql21 = mysqli_query($conn,"select * from mensajes where id_mensaje='$file'");
	$sql22 = mysqli_query($conn,"select * from recordatorios where id_recordatorio='$file'");

	$sql31 = mysqli_query($conn,"select * from complementos where id_complemento='$file'");
    $sql31c = mysqli_query($conn,"SELECT * FROM complementos where nombre_complemento='$file' and valor <> '' ORDER BY id_complemento ASC");

	$option ="";
    if(isset($_GET['option_grupo']) && !empty($_GET['option_grupo'])){

		$option_grupo = $_GET['option_grupo'];
		$option_estatus = "";
		$option = "AND t2.grupo = '$option_grupo'";

		if(!empty($_GET['option_estatus'])){
			 $option_estatus = $_GET['option_estatus'];
			 $option = "AND t2.estatus = '$option_estatus'";
		}
	}
	else{
		$option_grupo="";
		$option_estatus="";
	}

	$fecha_desde = "";
	$fecha_hasta = "";
	$option_fecha ="";

	if(isset($_GET['fecha_hasta']) && !empty($_GET['fecha_hasta'])){

		$fecha_desde = $_GET['fecha_desde'];
		$fecha_hasta = $_GET['fecha_hasta'];
		$option_fecha = "AND DATE(t1.fecha) BETWEEN '$fecha_desde' AND '$fecha_hasta'";

	}

	if($tipo_usuario==1 && $id_empresa==1){
	//consulta estadisticas de indicadores y controles para usuario admin
	$qpsql30 = "select distinct t2.id_cliente, t2.nombre, t2.razon,t2.estatus, t3.cliente_padre from referencias t1 inner join cliente t2 on t1.id_cliente = t2.id_cliente INNER JOIN relacion_clientes t3 ON t2.id_cliente=t3.cliente_hijo where t1.id_parametro ='$file' $option $option_fecha";
	$sql30 = mysqli_query($conn,$qpsql30);
	$qpsql32 = "select distinct t2.id_cliente, t2.nombre, t2.razon,t2.estatus,t1.referencia,t1.naturaleza,t1.valor, t1.fecha, t3.cliente_padre, t3.cliente_hijo from referencias_complementos t1 inner join cliente t2 on t1.id_cliente = t2.id_cliente INNER JOIN relacion_clientes t3 ON t2.id_cliente=t3.cliente_hijo where t1.id_complemento ='$file' $option $option_fecha";
	$sql32 = mysqli_query($conn,$qpsql32);



	}else {//consulta estadisticas de indicadores y controles para otros usuarios

	$qpsql30 = "select distinct t2.id_cliente, t2.nombre, t2.razon,t2.estatus,t3.cliente_padre, t3.cliente_hijo from referencias t1 inner join cliente t2 on t1.id_cliente = t2.id_cliente INNER JOIN relacion_clientes t3 ON t2.id_cliente=t3.cliente_hijo where t1.id_parametro ='$file' and t2.id_empresa='$id_empresa' $option $option_fecha";
	$sql30 = mysqli_query($conn,$qpsql30);
	$qpsql32 = "select distinct t2.id_cliente, t2.nombre, t2.razon,t2.estatus,t1.referencia,t1.naturaleza,t1.valor,t1.fecha, t3.cliente_padre, t3.cliente_hijo from referencias_complementos t1 inner join cliente t2 on t1.id_cliente = t2.id_cliente INNER JOIN relacion_clientes t3 ON t2.id_cliente=t3.cliente_hijo where t1.id_complemento ='$file' and t2.id_empresa='$id_empresa' $option $option_fecha";
	$sql32 = mysqli_query($conn,$qpsql32);
	//$xf="select distinct t2.id_cliente, t2.nombre, t2.razon,t2.estatus,t1.referencia,t1.naturaleza,t1.valor from referencias_complementos t1 inner join cliente t2 on t1.id_cliente = t2.id_cliente where t1.id_complemento ='$file' and t2.id_empresa='$id_empresa' $option";

	//echo $qpsql30;
	}
}

if(isset($_GET['s2'])){


$search2 = $_GET['s2'];

if(isset($_GET['cl'])){
		$cliente = $_GET['cl'];
		$sql26 = mysqli_query($conn,"select * from mensajes where (asunto like '%$search2%' or contenido like '%$search2%') and id_cliente = '$cliente' order by fecha asc");
		$sql27 = mysqli_query($conn,"SELECT * FROM recordatorios WHERE asunto != 'Por paquete' and id_cliente='$cliente' AND (contenido LIKE  '%$search2%' OR asunto LIKE  '%$search2%') UNION SELECT * FROM recordatorios WHERE asunto =  'Por paquete' and id_cliente='$cliente'  AND cliente LIKE  '%$search2%' order by fecha asc");
			}

		if($tipo_usuario == 1 && $id_empresa == 1){
		$sql28 = mysqli_query($conn,"select * from mensajes where `status`= 'no' and (asunto like '%$search2%' or contenido like '%$search2%')");
		$sql29 = mysqli_query($conn,"SELECT * FROM recordatorios WHERE asunto != 'Por paquete' and `status`= 'no' AND (contenido LIKE  '%$search2%' OR asunto LIKE  '%$search2%') UNION SELECT * FROM recordatorios WHERE asunto =  'Por paquete' and `status`= 'no' AND cliente LIKE  '%$search2%' order by fecha asc");

		}

		if(($tipo_usuario == 1 && $id_empresa !=1) | ( $tipo_usuario == 2 | $tipo_usuario == 3)){
		$sql28 = mysqli_query($conn,"select * from mensajes where `status`= 'no' and id_empresa='$id_empresa' and (asunto like '%$search2%' or contenido like '%$search2%')");
		$sql29 = mysqli_query($conn,"SELECT * FROM recordatorios WHERE asunto != 'Por paquete' and id_empresa='$id_empresa' and `status`= 'no' AND (contenido LIKE  '%$search2%' OR asunto LIKE  '%$search2%') UNION SELECT * FROM recordatorios WHERE asunto =  'Por paquete' and id_empresa='$id_empresa' and `status`= 'no' AND cliente LIKE  '%$search2%' order by fecha asc");
		}

}
?>

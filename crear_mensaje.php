<?php 
 error_reporting(E_ALL);
ini_set('display_errors', 1);
include 'set.php';
 session_start();
   $error = "";  
	date_default_timezone_set ("America/Mexico_City");
   $fecha = date('Y-m-d');
   $hora = date('H:i:s');

if(isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == 1){ 
 $id_usuario = $_SESSION['id_usuario'];
 $nombre = $_SESSION['nombre_usuario'];
 $tipo_usuario = $_SESSION['tipo_usuario'] ;
 $empresa_usuario =$_SESSION['empresa_usuario']; 
   $id_empresa = $_SESSION['id_empresa'];
}


//Guarda mensaje 
if(isset($_POST["crear_msj"]))
{    
     $cliente = $_POST["cliente"];
	 $asunto = $_POST["asunto"];
     $msj = $_POST["mensaje"];
	
	
	$query = "Insert into mensajes (id_cliente, id_usuario,id_empresa, asunto, contenido,fecha, hora,status)
	values('$cliente','$id_usuario','$id_empresa','$asunto','$msj','$fecha','$hora','no')";

	   mysqli_query($conn,$query) or die (mysqli_error());
    
   header('location:brm.php?cl='.$cliente.'&carp=msj&msj=mensaje creado');
}


//Guarda mensaje y lo envia 
if(isset($_POST["enviar_obj2"])) {

	  $cliente = $_POST["cliente"];
	 $asunto = $_POST["asunto"];
     $msj = $_POST["mensaje"];
	
	
	$q_guardar = "Insert into mensajes (id_cliente, id_usuario,id_empresa, asunto, contenido,fecha, hora,status)
	values('$cliente','$id_usuario','$id_empresa','$asunto','$msj','$fecha','$hora','no')";

	   mysqli_query($conn,$q_guardar) or die (mysqli_error());

     
		$id_obj = mysqli_insert_id($conn);
     

	 // Armar url de redirecion
	$url = 'location:brm.php?';
	
	if(!empty($_POST["cliente"])){
		$url .= 'cl='.$_POST["cliente"].'&';
	}
	
	if(!empty($_POST["carpeta"])){
		$url .= 'carp='.$_POST["carpeta"].'&';
	}
	
	if(!empty($_POST["file"]) && !empty($_POST["cliente"])){
		$url .= 'file='.$_POST["file"].'&';
	}
	
	//Verifica si por lo menos hay un destinatario 
	if(!isset($_POST["check_cto"]) && !isset($_POST["helper_namec"]) && !isset($_POST["masivo"]) ){
			     $url .= 'msj=ningun destinatario seleccionado';	
                    header($url);
                    exit();
	}
	
	//Verifica si hay por lo menos un metodo de envio seleccionado 
		if(!isset($_POST["tw"]) && !isset($_POST["email"]) && !isset($_POST["sms"]) ){
                   $url .= 'msj=ningun metodo de envio seleccionado';	
                    header($url);
                    exit();				
		}
	
	
			//ACCIONES PARA ENVIAR EMAILS
           if(isset($_POST["email"])){
                  
				  
				  require 'PHPMailer/PHPMailerAutoload.php';
				  require ($_SERVER['DOCUMENT_ROOT'].'/PHPMailer/class.phpmailer.php');

					$mail = new PHPMailer;
				
				 
                 $msg= "";				 
				  
				//Archivos adjuntos
				if (array_key_exists('anexos', $_FILES)) {
				for ($ct = 0; $ct < count($_FILES['anexos']['tmp_name']); $ct++) {
					$uploadfile = tempnam(sys_get_temp_dir(), sha1($_FILES['anexos']['name'][$ct]));
					$filename = $_FILES['anexos']['name'][$ct];
					if (move_uploaded_file($_FILES['anexos']['tmp_name'][$ct], $uploadfile)) {
						$mail->addAttachment($uploadfile, $filename);
					} else {
						$msg .= 'Failed to move file to ' . $uploadfile;
					}
				}}			
						//Cabeceras			 

						 $FromName = $_POST["from"];
						 $FromEmail = $_POST["remit"];

					     $mail->setFrom($FromEmail, $FromName);
					  
						$mail->addReplyTo($FromEmail, $FromName);
									 
				  // email para persona contacto 
		  if(isset($_POST["email_ctoc"])  && isset($_POST["check_cto"])){
					   
                 $pcto = $_POST["email_ctoc"];
                 $asunto = utf8_decode($_POST["asunto"]);
                 $msj = "At'n, ".$_POST["pcontact"]."\n\n".$_POST["mensaje"]."\n\n Atentamente \n\n".$_POST['usuario']."\n\n".$_POST["from"];

                $mail->addAddress($pcto); 	
				$mail->isHTML(false);                                  // Set email format to HTML
				$mail->Subject = $asunto;
				$mail->Body    = $msj;
				$mail->CharSet = 'UTF-8';

							if(!$mail->send()) {
								//echo 'Mailer Error: ' . $mail->ErrorInfo;
							} else {
								
								$rcto = 1;
							}
				
				   }//fin email persona contacto 
				  
			
                    // email para colaboradores   				  
				    if(isset($_POST["helper_namec"])){
                    
					$arrc = $_POST["helper_namec"];
					
				   foreach ($arrc as $ac){
							
					$ename = "cola_".str_replace(' ', '', $ac);
					
					
					if(isset($_POST[$ename])){
						
						$email_col = $_POST[$ename];
						
					}
	
					$asunto = utf8_decode($_POST["asunto"]);
			     	//cuerpo del email

						$msj = "At'n, ".$ac."\n\n".$_POST["mensaje"]."\n\n Atentamente \n\n".$_POST['usuario']."\n\n".$_POST["from"];
						$mail->ClearAddresses();
						$mail->addAddress($email_col);
						$mail->isHTML(false);                                  // Set email format to HTML
						$mail->Subject = $asunto;
						$mail->Body    = $msj;
						$mail->CharSet = 'UTF-8';

							if(!$mail->send()) {
								//echo 'Mailer Error: ' . $mail->ErrorInfo;
							} else {
								$rcola = 1;
							}
						}					
				   } //fin email para colaboradores
				  
				  //email para clientes activos
				    if(isset($_POST["masivo"]) && $_POST["masivo"] == "activos"){
					
					
					$id_empresa = $_POST["id_empresa"];
					$queryac = mysqli_query($conn,"select * from cliente t1 inner join general_cliente t2 on  t2.id_cliente = t1.id_cliente where t1.estatus='Si' and t1.id_empresa='$id_empresa'");
		            while ($r_ac = mysqli_fetch_array($queryac)){ 
                    
					
					if ($_POST["id_cliente"] != $r_ac["id_cliente"]){
					
					$act = $r_ac["email"];
					 $asunto = utf8_decode($_POST["asunto"]);
					//cuerpo del email

				$msj = "At'n, ".$r_ac["contactos"]."\n\n".$_POST["mensaje"]."\n\n Atentamente \n\n".$_POST['usuario']."\n\n".$_POST["from"];

				$mail->ClearAddresses();		
				$mail->addAddress($act); 								
				$mail->isHTML(false);                                  // Set email format to HTML
				$mail->Subject = $asunto;
				$mail->Body    = $msj;
				$mail->CharSet = 'UTF-8';
   
							if(!$mail->send()) {
								//echo 'Mailer Error: ' . $mail->ErrorInfo;
							} else {
								$ract = 1;
							}
																		  
							}
						}					
				   }//Fin email clientes activos 
				   
				   //email para clientes inactivos
				   if(isset($_POST["masivo"]) && $_POST["masivo"] == "inactivos"){
                    	
					$id_empresa = $_POST["id_empresa"];
					$queryin = mysqli_query($conn,"select * from cliente t1 inner join general_cliente t2 on  t2.id_cliente = t1.id_cliente where t1.estatus='No' and t1.id_empresa='$id_empresa'");
		            while ($r_in = mysqli_fetch_array($queryin)){ 
                      
					if ($_POST["id_cliente"] != $r_in["id_cliente"]){
					 $ina = $r_in["email"];
					 $asunto = utf8_decode($_POST["asunto"]);
					 //cuerpo del email
	           $msj = "At'n, ".$r_in["contactos"]."\n\n".$_POST["mensaje"]."\n\n Atentamente \n\n".$_POST['usuario']."\n\n".$_POST["from"];

				$mail->ClearAddresses();		
				$mail->addAddress($ina); 							
				$mail->isHTML(false);                                  // Set email format to HTML
				$mail->Subject = $asunto;
				$mail->Body    = $msj;
				$mail->CharSet = 'UTF-8';
			
						 if(!$mail->send()) {
								//echo 'Mailer Error: ' . $mail->ErrorInfo;
								   } else {
								$ract = 1;
									}
					
							}			
						}					
				   }//Fin email clientes inactivos 
				   
				   
				    //email para todos los clientes de la empresa
				    if(isset($_POST["masivo"]) && $_POST["masivo"] == "todos"){
                    
					  	
					$id_empresa = $_POST["id_empresa"];
					$queryt = mysqli_query($conn,"select * from cliente t1 inner join general_cliente t2 on  t2.id_cliente = t1.id_cliente where t1.id_empresa='$id_empresa'");
		            while ($r_t = mysqli_fetch_array($queryt)){ 
					
                   	if ($_POST["id_cliente"] != $r_t["id_cliente"]){
					 $t = $r_t["email"];
					 $asunto = utf8_decode($_POST["asunto"]);
					 //cuerpo del email

						$msj = "At'n, ".$r_t["contactos"]."\n\n".$_POST["mensaje"]."\n\n Atentamente \n\n".$_POST['usuario']."\n\n".$_POST["from"];

				$mail->ClearAddresses();		
				$mail->addAddress($t); 							
				$mail->isHTML(false);                                  // Set email format to HTML
				$mail->Subject = $asunto;
				$mail->Body    = $msj;
				$mail->CharSet = 'UTF-8';
					   
						if(!$mail->send()) {
							//echo 'Mailer Error: ' . $mail->ErrorInfo;
						} else {
							$rt = 1;
						}
				
							}	
						}					
				   }//fin email clientes todos 
  

			if(isset($rcto) || isset($rcola) || isset($ract) || isset($rin) || isset($rt)){
					
					$query = "update mensajes set status='si' where id_mensaje ='$id_obj'";
					mysqli_query($conn,$query) or die (mysqli_error());

					$query1 = "update recordatorios set status='si' where id_recordatorio ='$id_obj'";
					mysqli_query($conn,$query1) or die (mysqli_error());
					
	
					//si no hay mas metodos redireccionar 
					if(!isset($_POST["tw"]) && !isset($_POST["sms"])){
					$url .= 'msj=mensaje enviado por email';	
                    header($url);
                    exit();					
					}
					
					//Si hay algun otro metodo de envio solo añadir al mensaje 
					if(isset($_POST["tw"]) || isset($_POST["sms"])){
						$url .= 'msj=mensaje enviado por email';	
					}		

				}
 
					else {
					$url .= 'msj=fallo en el envio de email, mensaje no enviado';	
                    header($url);
                    exit();		
					}
			}

		//ACCIONES PARA ENVIAR SMS
		if(isset($_POST["sms"])){

				 if (isset($_POST["success_sms"])){ // SI se envio algun SMS 
						
					$query_sms = "update mensajes set status='si' where id_mensaje ='$id_obj'";
					mysqli_query($conn,$query_sms) or die (mysqli_error());

					$query1_sms = "update recordatorios set status='si' where id_recordatorio ='$id_obj'";
					mysqli_query($conn,$query1_sms) or die (mysqli_error());
					
					
					if(!isset($_POST["tw"])){ // se envio los SMS y no hay envio de TWITTER seleccionado
					$url .= 'msj=SMS enviados';
                    header($url);
                    exit();					
					
					} else { // Hay envio de twitter seleccionado 
						$url .= 'msj=SMS enviados';	
					} 
	
				 } // Fin SMS enviados
				 else { // Se selecciono SMS pero no se enviaron 
					 
					 if(!isset($_POST["tw"])){ //no se envio los SMS y no hay envio de TWITTER seleccionado
					$url .= 'msj=los SMS no pudieron ser enviados';
                    header($url);
                    exit();					
					
					} else { // Hay envio de twitter seleccionado 
						$url .= 'msj=los SMS no pudieron ser enviados';
					}
				 } // Fin SMS no enviados
					
		} // Fin acciones SMS



		//ACCIONES PARA ENVIAR TWEETS 

		if(isset($_POST["tw"])){
	
	        unset($_SESSION['ut']);
			unset($_SESSION['ut1']);
			unset($_SESSION['ut2']);
			unset($_SESSION['ut3']);
			unset($_SESSION['ut4']);
			unset($_SESSION['urlt']);
			
	       //tweet para persona contacto
			if(!empty($_POST["tw_cto"]) &&  isset($_POST["check_cto"])){
             $tweets= array();
            
			  $tweets['user'][] =str_replace ( '@', '' ,$_POST["tw_cto"]);
              $tweets['mensaje'][] = "At'n,".$_POST["pcontact"].' '.$_POST['mensaje'];
			  
			$_SESSION['ut'] = $tweets;
			
			$query = "update mensajes set status='si' where id_mensaje ='$id_obj'";
			mysqli_query($conn,$query) or die (mysqli_error());

			$query1 = "update recordatorios set status='si' where id_recordatorio ='$id_obj'";
			mysqli_query($conn,$query1) or die (mysqli_error());
         
	
			}
			
			//tweet para colaboradores
			  if(isset($_POST["helper_namec"])){
				  
				    $arrc = $_POST["helper_namec"];
					$tweets1 = array();

				    foreach ($arrc as $ac){
					$col_tw = mysqli_query($conn,"select twitter from colaboradores where nombre='$ac'");
					
					
						while ($r_coltw = mysqli_fetch_array($col_tw)){
					$ctwn = $ac;
					$ctw = $r_coltw["twitter"];
					 
						}
				
				 if(isset($ctw)){
				$tweets1['user'][] = str_replace ( '@', '' , $ctw);
                $tweets1['mensaje'][] = "At'n,".$ctwn.' '.$_POST['mensaje'];
				 }
				 
			       }
                 $_SESSION['ut1'] = $tweets1;
			  }
				
              // tweets para clientes activos				
			  if(isset($_POST["masivo"]) && $_POST["masivo"] == "activos"){
					
					 $tweets2 = array();
					
					$id_empresa = $_POST["id_empresa"];
					$querytac = mysqli_query($conn,"select * from cliente t1 inner join general_cliente t2 on  t2.id_cliente = t1.id_cliente where t1.estatus='Si' and t1.id_empresa='$id_empresa'");
		            while ($r_act = mysqli_fetch_array($querytac)){ 

			   if ($_POST["id_cliente"] != $r_act["id_cliente"] && isset($r_act["twitter"])){
				$tweets2['user'][] = str_replace ( '@', '' , $r_act["twitter"]);
				$tweets2['mensaje'][] = "At'n,".$r_act["contactos"].' '.$_POST['mensaje'];
					}
					
			   }
	            $_SESSION['ut2'] = $tweets2;
			   }
			
			// tweets para clientes inactivos			
			  if(isset($_POST["masivo"]) && $_POST["masivo"] == "inactivos"){
					
					 $tweets3 = array();
					
					$id_empresa = $_POST["id_empresa"];
					$querytin = mysqli_query($conn,"select * from cliente t1 inner join general_cliente t2 on  t2.id_cliente = t1.id_cliente where t1.estatus='No' and t1.id_empresa='$id_empresa'");
		            while ($r_tin = mysqli_fetch_array($querytin)){ 

			   if ($_POST["id_cliente"] != $r_tin["id_cliente"] && isset($r_tin["twitter"])){
				$tweets3['user'][] = str_replace ( '@', '' , $r_tin["twitter"]);
                $tweets3['mensaje'][] = "At'n,".$r_tin["contactos"].' '.$_POST['mensaje'];				
				}
					
			   }
                       $_SESSION['ut3'] = $tweets3;
			   }
			
			
						// tweets para todos los clientes	
			   if(isset($_POST["masivo"]) && $_POST["masivo"] == "todos"){
					
					 $tweets4 = array();
					
					$id_empresa = $_POST["id_empresa"];
					$querytod = mysqli_query($conn,"select * from cliente t1 inner join general_cliente t2 on  t2.id_cliente = t1.id_cliente where t1.id_empresa='$id_empresa'");
		            while ($r_tod = mysqli_fetch_array($querytod)){ 

			   if ($_POST["id_cliente"] != $r_tod["id_cliente"] && isset($r_tod["twitter"])){
				$tweets4['user'][] = str_replace ( '@', '' , $r_tod["twitter"]);	 
				$tweets4['mensaje'][] = "At'n,".$r_tod["contactos"].' '.$_POST['mensaje'];	
					}
					
			   }
                   $_SESSION['ut4'] = $tweets4;
			   }
			
				
				//Definir url de redirecion despues de enviar tweet
				
				$urlt ="";
				
				if(!empty($_POST["cliente"])){
					$urlt .= 'cl='.$_POST["cliente"].'&';
				}
				
				if(!empty($_POST["carpeta"])){
					$urlt .= 'carp='.$_POST["carpeta"].'&';
				}
				
				if(!empty($_POST["file"]) && !empty($_POST["cliente"])){
					$urlt .= 'file='.$_POST["file"].'&';
				}
				
			
			$_SESSION['urlt'] = $urlt;
            header('location:twitter/index.php');
			exit();				

				}

		
		
}


if(isset($_POST["msj_up"]))
{    
   

	$id_msj = $_POST["id"];
 
	 $asunto = $_POST["asunto"];
     $msj = $_POST["mensaje"];
	 
	 if(isset($_POST["cliente"])){
	 $cliente = $_POST["cliente"];
	 $query = "update mensajes set id_cliente = '$cliente', id_usuario = '$id_usuario', asunto ='$asunto', contenido='$msj',
	 fecha = '$fecha', hora = '$hora',status='no' where id_mensaje='$id_msj'";
	
	mysqli_query($conn,$query) or die (mysqli_error());
	header('location:brm.php?carp=ale');
	
	}
	
	 if(isset($_POST["cl"])){
	 	$cl = $_POST["cl"];
	$query = "update mensajes set id_cliente = '$cl', id_usuario = '$id_usuario', asunto ='$asunto', contenido='$msj',
	fecha = '$fecha', hora = '$hora',status='no' where id_mensaje='$id_msj'";


	mysqli_query($conn,$query) or die (mysqli_error());
 
	header( 'location:brm.php?cl='.$cl.'cl&carp=msj');

	}
	
}


if(isset($_POST["msj_del"]))
{    
    $id_msj = $_POST["id"];
	$query1 = "delete from mensajes where id_mensaje='$id_msj'";
	mysqli_query($conn,$query1) or die (mysqli_error());
     
	 if(isset($_POST["cl"])){
		 $cl = $_POST["cl"];
		 header( 'location:brm.php?cl='.$cl.'cl&carp=msj');
	 }
else {	 
  header('location:brm.php?carp=ale');
}


}

if(isset($_POST["tratado_msj"]))
{    
	$id_msj = $_POST["id_mensaje"];
	
	$query = "update mensajes set status='si' where id_mensaje='$id_msj'";

	mysqli_query($conn,$query) or die (mysqli_error());
	
	if(isset($_POST['cliente']) && $_POST['cliente'] != ""){
	
	$cl = $_POST['cliente'];	
	header('location:brm.php?cl='.$cl.'&carp=msj&file='.$id_msj.'&msj=mensaje marcado como tratado');	
	
	}

	else {
	header('location:brm.php?carp=ale&msj=mensaje marcado como tratado');
	}
}


?>
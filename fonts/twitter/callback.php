<?php
 error_reporting(E_ALL);
ini_set('display_errors', 1);

require 'TwitterOAuthHelper.php';

if(!empty($_GET['oauth_token']) && !empty($_SESSION['oauth_token']) && !empty($_SESSION['oauth_token_secret'])){
	$_SESSION['twitter_access_token'] = TwitterOAuthHelper::getAccessToken();
	header('Location:index.php'); //index.php
} else {
	header('Location:index.php');
}
<?php 

   include 'set.php';
   date_default_timezone_set('America/Mexico_City');
   session_start();
   $error = "";

if(isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == 1){ 
 $id_usuario = $_SESSION['id_usuario'];
 $nombre = $_SESSION['nombre_usuario'];
 $tipo_usuario = $_SESSION['tipo_usuario'] ;
 $empresa_usuario =$_SESSION['empresa_usuario']; 
 $id_empresa = $_SESSION['id_empresa'];

}

else {header('location:index.php');}   
   
   
$query = mysqli_query($conn,"select * from empresa");

	if(isset($_GET['empresa']))
	{
	 $q_empresa = $_GET['empresa']; 
	  	 
		 if(isset($_GET['relacionadas']))
		     {$query_rel = mysqli_query($conn,"select * from empresa where id_empresa!=1 and id_empresa!='$id_empresa'");}
		  else
		    {$query1 = mysqli_query($conn,"select * from usuario where id_empresa = '$q_empresa'");}
		  
	 $_SESSION['id_empresa']= $q_empresa;
	 }

	 else {$_SESSION['id_empresa'] = 1;} 
 

 if(isset($_POST['s_session'])){	
		//Actualiza el tiempo en segundos de la duracion de la sesion 
		$duracion = $_POST['segundos'];
		$sql_conf = "update configuraciones set duracion_sesion ='$duracion' where id_conf='1'";	
		mysqli_query($conn,$sql_conf) or die (mysqli_error());
	}

require 'header.php';
?>
    <body class="body-admin">
  <div class="wrapper"> 
  <div class="container">
     <div class="row">
	 
	          <div class="col-md-4">
              <h2>Administrador</h2>
              </div> 
			  
			  <div class="col-md-2"></br>
			   <a data-toggle="modal" data-target="#modal_up_admin" class="btn btn-primary"><i class="glyphicon glyphicon-user"></i>
					Mi perfil</a>
		      </div>
			  
			  <div class="col-md-3">
			  <form method="post" action="admin.php">
			  <label class="control-label">Tiempo límite de sesión inactiva</label>
			  <?php //Imprime la duracion en segundos de la sesion
			  $qseg=mysqli_query($conn,"SELECT duracion_sesion FROM configuraciones WHERE id_conf= '1'");
			  $r1qseg = mysqli_fetch_assoc($qseg);
			  $value = $r1qseg['duracion_sesion']; ?>
			  <input type="number" name="segundos" value="<?php echo $value;?>" maxlength="6" size="6" onkeypress="return isNumberKey(this,event)"/>
			  <button type="submit" name="s_session" class="btn btn-primary"> <i class="glyphicon glyphicon-refresh"></i> Cambiar </button>
			  </form>
			  </div>

			  <div class="col-md-2"></br>
	          <a href="logout.php?id_user=1" class="btn btn-primary"><i class="glyphicon glyphicon-log-out"></i>
					Cerrar Sesión</a>
			  </div>
	 </div>     
	 <div class="row">
			  <hr class="hr-primary" />
			   
			   <div class="col-md-10"> 
              <h2>Empresas</h2>
              </div> 
			  
			  <div class="col-md-2"></br>
			   <a href="registro_empresa.php" class="btn btn-primary"><i class="glyphicon glyphicon-plus"></i>
			    Nueva Empresa</a>
		      </div>
			  
			
			  
	</div>	<br>
	
    <div class="row well">
    
          <form action="#" class="form-horizontal">
		  <div class="form-group panel-empresa">
		<!--  <label class="col-sm-2 control-label" for="empresa">Nombre de Empresa</label>-->
		  
		  <div class="col-sm-2">
      <div class="dropdown">
  <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">Seleccione Empresa
  <span class="caret"></span></button>
  <ul class="dropdown-menu">
     <?php 
    while($row = mysqli_fetch_array($query))
                 {
				  if($row["id_empresa"] != 1){	 
                  echo '<li><a href="admin.php?empresa='.$row["id_empresa"].'">'.$row["nombre_empresa"].'</a></li>'; 
				  }}?>
  </ul>
</div>
</div>

		  </form>

		   
		   <?php if(isset($_GET['empresa'])){
			   
				$idempresa = $_GET['empresa'];
				$query2 = mysqli_query($conn,"select nombre_empresa from empresa where id_empresa = '$idempresa'");
				$rquery2 = mysqli_fetch_assoc($query2);
				$nempresa = $rquery2['nombre_empresa'];
				$_SESSION['empresa_usuario'] = $nempresa;

				$query2_count = mysqli_query($conn,"select count(*) from usuario where id_empresa ='$idempresa'");
				$rquery2_count = mysqli_fetch_assoc($query2_count);
				$conteo = $rquery2_count['count(*)'];
			?>
			
		   <div class="col-sm-2">
			<?php echo '<a data-toggle="modal" data-target="#modal_up_empresa" class="btn btn-primary btn-sm"> <i class="glyphicon glyphicon-pencil"></i> '.$nempresa.' </a>&nbsp;';?>
		    </div>
			<div class="col-sm-2 text-right">
			<?php 
			if(isset($_GET['relacionadas'])){
			 echo '<a href="admin.php?empresa='.$q_empresa.'" class="btn btn-primary btn-sm"> <i class="glyphicon glyphicon-eye-open"></i> Usuarios</a>&nbsp;';
			}	
			else if(isset($conteo) && $conteo >= 100){
			echo '<a href="registro_usuario.php?empresa='.$q_empresa.'" class="btn btn-primary disabled btn-sm"> <i class="glyphicon glyphicon-user"></i> Crear Usuario</a>&nbsp;';
			}
			else {
				echo '<a href="registro_usuario.php?empresa='.$q_empresa.'" class="btn btn-primary btn-sm"> <i class="glyphicon glyphicon-user"></i> Crear Usuario</a>&nbsp;';
			}
			?>
			 
			</div>
			
			  <div class="col-sm-2">
			   <a  href="admin.php?empresa=<?php echo $_GET['empresa'];?>&relacionadas=<?php echo $_GET['empresa'];?>" class="btn btn-primary btn-sm"><i class="glyphicon glyphicon-eye-open"></i>
			    Empresas Relacionadas</a>
		      </div>
			  
			  <div class="col-sm-2">
			   <a  href="admin_grupos.php?empresa=<?php echo $_GET['empresa'];?>" class="btn btn-primary btn-sm"><i class="glyphicon glyphicon-th-large"></i>
			    Grupos</a>
		      </div>
			  
			<div class="col-sm-2">
			<a data-toggle="modal" class="btn btn-primary btn-sm"  data-target="#del_emp_<?php echo $q_empresa; ?>"><i class="glyphicon glyphicon-remove"></i> Eliminar Empresa</a>
		   </div>
		   
					<!-- MODAL ELIMINAR EMPRESA-->
					<div id="del_emp_<?php echo $q_empresa; ?>" class="modal fade" role="dialog">
                    <div class="modal-dialog">
					<!-- Modal content-->
                    <div class="modal-content">
                    <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Eliminar Empresa </h4> </div>
                    <div class="modal-body">
                    <form method="post" action="accion_empresa.php">
		            <div class="row">
					<div class="col-md-12">
		          
		             <p> ¿ Esta seguro que desea eliminar la empresa <?php echo $nempresa; ?>? </p>
				   </div>
				   </div></div>
                    <div class="modal-footer">
	  	            <div class="col-md-6 text-left col-md-offset-8">
					<input type="hidden" name="id_empresa" value="<?php echo $q_empresa; ?>"/> 
					<button type="submit" name="del_empresa" class="btn btn-primary"> <i class="glyphicon glyphicon-remove"></i> Eliminar </button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
		            </form></div></div></div></div></div>
		   
		    <?php } else {$_SESSION['empresa_usuario'] = 'adminbrm';}?>
		
		     </div>
          
		</div>
		<div class="row">
        <table class="table table-hover">
          <thead>
		  	<?php if (isset($query1)){?>
            <tr>
              <th class="header">
                #
              </th>
			  <th>
                Usuario
              </th>
              <th>
                Nombre
              </th>
			  <th>
                Email
              </th>
			  <th>
                Tipo
              </th>
              <th class="text-center">
                Acción
              </th>
            </tr>
          </thead>
          <tbody>
		  <?php 
		    $n = 1;
			
		    while($row1 = mysqli_fetch_array($query1))
                 { 
			 
			 ?>
				
                  <!-- MODAL ACTUALIZAR USUARIO DESDE ADMIN-->
<div id="<?php echo $row1['nombre'];?>" class="modal fade " role="dialog">
<div class="modal-dialog">

<div class="modal-content">

<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">Actualizar usuario</h4> 
</div>

<div class="modal-body">
<div class="row">
<form id="up_user_admin" method="post" action="accion_usuario.php">
 <div class="col-md-6">

<div class="form-group">
 <label class="col-lg-3 control-label">Datos</label>
<input type="text" class="form-control" name="username" placeholder="Nombre de usuario" value="<?php echo $row1['nombre'];?>" />
</div>

<div class="form-group">
<input type="text" class="form-control" name="nombre" placeholder="Nombre Completo" value="<?php echo $row1['nombre_completo'];?>"/>
</div>

<div class="form-group">
<input type="email" class="form-control" name="email" placeholder="Email" value="<?php echo $row1['email'];?>"/>
</div>
</div>

  <div class="col-md-6">

<div class="form-group"> 
 <label class="col-lg-10 control-label">Tipo de Usuario</label>
<select name="tipo" class="form-control">

<option value="2" <?php if($row1['tipo'] == "2"){echo 'selected';}?>>Usuario General</option>
<option value="3" <?php if($row1['tipo'] == "3"){echo 'selected';}?>>Supervisor</option>

</select>
</div>

 <label class="col-lg-10 control-label">Cambiar Password</label>
<div class="form-group pass_1">
<input type="password" class="form-control" id="pass_1" name="password" placeholder="Nuevo password" value="<?php echo $row1['password'];?>"/>
<small class='help-block' style="display:none">la contraseña y su confimación no son iguales</small>
</div>

<div class="form-group pass_2">
<input type="password" class="form-control" id="pass_2" name="confirmPassword" placeholder="Confirma nuevo password" value="<?php echo $row1['password'];?>" />
</div>


</div>
</div>
</div>
<div class="modal-footer">
<div class="form-group">
<div class="col-lg-9 col-lg-offset-3">
<input type="hidden" name="id_usuario" value="<?php echo $row1['id_usuario'];?>"/>
<input type="hidden" name="id_empresa" value="<?php echo $row1['id_empresa'];?>"/>
<button type="submit" class="btn btn-primary guardar" name="up_user_admin"><i class="glyphicon glyphicon-check"></i> Guardar</button>

<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>

</div>
</div>

</div>
</div>
</div>
</div>	
	

	
			<?php
			      
                  echo '<tr>';?>
				  <form action="accion_usuario.php" method="post">
				  <?php 
				  echo '<td>'.$n.'</td>';
				  echo '<td>'.$row1["nombre"].'</td>';
				  echo '<td>'.$row1["nombre_completo"].'</td>';
				  echo '<td>'.$row1["email"].'</td>';
				  if($row1["tipo"] == 2){echo '<td>Usuario</td>';}
				  if($row1["tipo"] == 3){echo '<td>Supervisor</td>';}
		          echo '<td width=350>';
				
				  /**/
				  if($row1["status"]=="activo"){
				echo '<a data-toggle="modal" data-target="#'.$row1['nombre'].'" class="btn btn-info"> <i class="glyphicon glyphicon-pencil"></i> Editar</a>&nbsp;';	  
				  echo '<input type="hidden" name="status" value="bloqueado"/>';  
				  echo '<button type="submit" class="btn btn-info" name="bloquear">
				  <i class="glyphicon glyphicon-lock"></i> Bloquear</button>&nbsp;';
				  echo '<button type="submit" class="btn btn-info" name="baja_user">
				  <i class="glyphicon glyphicon-remove"></i> Eliminar</button>';
				  }
				   if($row1["status"]=="bloqueado"){
					echo '<input type="hidden" name="status" value="activo"/>';  
					echo '<button type="submit" class="btn btn-info" name="bloquear">
				  <i class="glyphicon glyphicon-lock"></i> Desbloquear</button>&nbsp;';
                   echo '<button type="submit" class="btn btn-info" name="baja_user">
				  <i class="glyphicon glyphicon-remove"></i> Eliminar</button>';				  
				  }
				   if($row1["status"]=="cancelado"){
					
					echo '<button type="submit" class="btn btn-info" name="habl_user">
				  <i class="glyphicon glyphicon-check"></i> Habilitar</button>';  
				  }
				   echo '<input type="hidden" name="usuario" value="'.$row1['id_usuario'].'"/>'; 
				 echo '<input type="hidden" name="empresa" value="'.$q_empresa.'"/>';  
				  echo '</td>';
				  ?>
				  
				  </form>
				  <?php 
				  echo '</tr>'; 
				  $n ++;
			}}
			    
			//Selecciona la empresa y muestra las relacionadas	
			else if(isset($query_rel)){ ?>
				<form action="relacion_empresa.php" method="post">
				<tr>
              <th class="header">#</th>
			  <th>Nombre</th>
			  <th>Razon Social</th>
			  <th class="text-center">Relación</th> 
			  </tr>
			  <?php 
			   $n = 1;?>
			   
			    <tbody>
			  			
                     <?php $cn =0;
					 //Recorre las relaciones que existen
					  while($rel = mysqli_fetch_array($query_rel)){ 
					  
						  $emp_rel = $rel['id_empresa'];
						  $query_rel2 = mysqli_query($conn,"SELECT * FROM relacion_empresas WHERE (empresa_a ='$id_empresa' OR empresa_b='$id_empresa') AND (empresa_a='$emp_rel' OR empresa_b='$emp_rel')");
						  
					  if(mysqli_num_rows($query_rel2) != 0){ ?>
							
						   <tr><td><?php echo $n; ?></td>
						   <td><?php echo $rel['nombre_empresa']; ?></td>
						   <td><?php echo $rel['razon']; ?></td>
						   <td class="text-center">
						   <input type="hidden" name="no_escogidos[]" value="<?php echo $rel['id_empresa'];?>"/>
						   <input type="checkbox" name="escogidos[]" checked="checked" value="<?php echo $rel['id_empresa'];?>"/>
						  </td></tr>	
					  <?php   $cn++;
								$n++;}
								 }
					
					  if($cn==0){?>
						  <tr><td  colspan="3" class="text-center">Ninguna empresa relacionada</td></tr>
						  
					  <?php } 
					   $n=1;?>
					     <tr><th class="separador" colspan="4">Otras Empresas</th></tr>
						 <tr>
						  <th class="header">#</th>
						  <th>Nombre</th>
						  <th>Razon Social</th>
						  <th class="text-center">Relación</th> 
						  </tr>
						<?php mysqli_data_seek( $query_rel, 0 );
						while($t = mysqli_fetch_array($query_rel)){ 

						   $emp_rel = $t['id_empresa'];
						   $qt = mysqli_query($conn,"SELECT * FROM relacion_empresas WHERE (empresa_a ='$id_empresa' OR empresa_b='$id_empresa') AND (empresa_a='$emp_rel' OR empresa_b='$emp_rel')");
						  
					  if(mysqli_num_rows($qt) == 0){ ?>
							 
					      <tr><td><?php echo $n; ?></td>
						   <td><?php echo $t['nombre_empresa']; ?></td>
						   <td><?php echo $t['razon']; ?></td>
						   <td class="text-center">
						   <input type="hidden" name="no_escogidos[]" value="<?php echo $t['id_empresa'];?>"/>
						   <input type="checkbox" name="escogidos[]" value="<?php echo $t['id_empresa'];?>"/>
						  </td></tr>
					 <?php } $n++;	 
						} ?>
						<tr><td colspan="4" align="right">
						<input type="hidden" name="p_relacion" value="<?php echo $id_empresa;?>"/>
						<input type="submit" name="guarda_relacion" value="Guardar" class="btn btn-primary"/></td></tr>
			<?php } //Fin tabla de empresas relacionadas ?>
			
        
          </tbody>
		  </form>
         </table>
        </div>
	   </br></br></br>

	   
	   <div class="row">
	     <div class="col-md-2 col-md-offset-10">
		   <a href="brm.php" class="btn btn-lg btn-primary"><i class="glyphicon glyphicon-expand"></i> Sistema BRM</a>
		   </div>
		</div> 
       </div>		
    </div>
	

<script> 
$(function(){
   $('#pass_1').keyup(function(){
		var _this = $('#pass_1');
		var pass_1 = $('#pass_1').val();
                _this.attr('style', 'background:white');
		if(pass_1.charAt(0) == ' '){
			$('.pass_1').attr('class','form-group pass_1 has-error');
			$('.guardar').prop('disabled',true);
		}
 
		if(_this.val() == ''){
		$('.pass_1').attr('class','form-group pass_1 has-error');
		$('.guardar').prop('disabled',true);
		}
	});
 
	$('#pass_2').keyup(function(){
		var pass_1 = $('#pass_1').val();
		var pass_2 = $('#pass_2').val();
		var _this = $('#pass_2');
                _this.attr('style', 'background:white');
		if(pass_1 != pass_2 && pass_2 != ''){
			$('.pass_1').attr('class','form-group pass_1 has-error');
			$('.pass_2').attr('class','form-group pass_2 has-error');
			$('.guardar').prop('disabled',true);
			$('.help-block').attr('style','display:block');
		}
		if(pass_1 == pass_2 && pass_1 != '' && pass_2 != ''){
			$('.pass_1').attr('class','form-group pass_1');
			$('.pass_2').attr('class','form-group pass_2');
			$('.guardar').prop('disabled',false);
			$('.help-block').attr('style','display:none');
			
		}
		
	});

		});
</script>	
	
	<script>
  //Mascara numerica para campo de minutos de sesion inactiva
      function isNumberKey(el,evt)
       {      var charCode = (evt.which) ? evt.which : event.keyCode;
				var number = el.value.split('.');
				if (charCode > 31 && (charCode < 48 || charCode > 57)) {
					return false;
				}
				//just one dot
				if(number.length>1 && charCode == 46){
					 return false;
				}
						
    </SCRIPT>
<?php 
include 'empresa.php';
include 'usuario.php';
?>

</body>
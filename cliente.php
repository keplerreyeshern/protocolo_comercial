<?php 
if(isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == 1){ 
	$id_empresa = $_SESSION['id_empresa'];
}

$query_grupo = mysqli_query($conn,"select * from grupos where id_empresa = '$id_empresa'");


?>


<!--- MODAL DE CLIENTES-->		
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-cliente">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Nuevo Cliente</h4>
      </div>
      <div class="modal-body">
         <form class="cliente" name="contact" enctype="multipart/form-data" id="cliente" method="post" action="crear_cliente.php">
		 <div class="row">
		 <div class="col-md-3">
		 <fieldset>
		  <legend class="the-legend">General</legend>
		  <div class="form-group">
			 <input id="nombre" type="text" name="nombre" class="form-control" placeholder="Nombre">
			 <div id="Info"></div>
		</div>
		 
         <div class="form-group">		
			<input type="text" name="razon" class=" form-control" placeholder="Razón">
		</div>
         <div class="form-group">		
			<input type="text" name="rfc" class=" form-control" placeholder="Número RFC">
		</div>
         <div class="form-group">		
           <input type="text" name="oficina" class=" form-control" placeholder="Oficina">
		</div>
		 <div class="form-group">	
        <input type="text" name="clave" class="form-control" placeholder="Claves Telefónicas"><br>	
        </div>		
		 <div class="form-group">	
		 <label>Grupos</label>
		 <select name="grupo" class="form-control" id="selectgrupo">
			<option value="0" selected>-- Seleccionar --</selected>
			<?php 
			
			while($qgrupo = mysqli_fetch_array($query_grupo)){ 
				if(!empty($qgrupo['nombre_grupo'])){ ?>
					<option value="<?php echo $qgrupo['id'];?>"><?php echo $qgrupo['nombre_grupo'];?></option>	
			<?php 	} }?>
		</select>
		</div>
		 <div class="form-group">		
			<label for="status">Estatus</label>
			<select name="status" id="selectestatus" class="form-control"></select>			
		</div>	
		<div class="form-group">		
			<label for="status">Cliente Relacionado</label>
			<select name="cliente_padre" id="cliente_padre" class="form-control">
				<option value="" selected>-- Seleccionar -- </option>	
			<?php $lclientes = mysqli_query($conn,"SELECT * FROM cliente WHERE id_empresa='$id_empresa'");
			while($rlclientes = mysqli_fetch_array($lclientes)){ ?>				
					<option value="<?php echo $rlclientes['id_cliente'];?>"><?php echo $rlclientes['nombre'];?></option>	
			<?php	} ?>
			</select>			
		</div>
        </fieldset>
		</div>
		<div class="form-group col-md-3">
		 <fieldset>
		  <legend class="the-legend">Localización</legend>
		     
			 <input type="text" name="calle" class="form-control" placeholder="Calle"><br>
			 <input type="text" name="externo" class="form-control" placeholder="Número Externo"><br>
            <input type="text" name="interno" class="form-control" placeholder="Número Interno"><br>
            <input type="text" name="colonia" class="form-control" placeholder="Colonia"><br>
			<input type="text" name="municipio" class=" form-control" placeholder="Municipio"><br>
			<input type="text" name="localidad" class=" form-control" placeholder="Localidad"><br>
			<input type="text" name="estado" class=" form-control" placeholder="Estado"><br>
			<input type="text" name="postal" class="form-control" placeholder="Código Postal"><br>
			
         </fieldset>
		</div>
		
				<div class="form-group col-md-3">
		 <fieldset>
		  <legend class="the-legend">Contacto</legend>
		  			<input type="text" name="contacto" class=" form-control" placeholder="Persona Contacto"><br>
			 <input type="text" name="telefono" class="form-control" placeholder="Teléfono"><br>
            <input type="email" name="email" class="form-control" placeholder="Email"><br>
			 <input type="text" name="facebook" class="form-control" placeholder="Facebook"><br>
            <input type="textl" name="twitter" class="form-control" placeholder="Twitter sin @"><br>
			<div class="input-group">
                <span class="input-group-btn">
                    <span class="btn btn-default btn-file">
					<span class="glyphicon glyphicon-folder-open"></span>
                    <b>&nbsp;Doc.</b>                   
						<input type="file" name="doc[]" id="doc" multiple="true"/>
                    </span>
                </span>
                <input type="text" class="form-control" readonly>
            </div>
         </fieldset>
		</div>
			<div class="form-group col-md-3">
			 <fieldset>
		  <legend class="the-legend">Protocolo</legend>
		    <textarea name="descripcion" class="form-control" rows="4" placeholder="Descripción de tratamiento a clientes"></textarea></br>
			<textarea name="slogan" class="form-control" rows="4" placeholder="Atributos"></textarea></br>
            <textarea name="contestacion" class="form-control" rows="4" placeholder="Contestación"></textarea></br>
	<!--	<div id="contenedor">
			<div class="col-md-5">
			<input type="text" name="colaborador[]" class="form-control" placeholder="Colaborador"/> </br>
			</div>
			<div class="col-md-5">
			<input type="text" name="jerarquia[]" class="form-control" placeholder="Jerarquia"/>
			</div>
			<a id="agregarCampo" class="btn  btn-primary" href="#"><span class="glyphicon glyphicon-plus"></span></a></br>
			</div></br></br>
			<div class="input-group">
                <span class="input-group-btn">
                    <span class="btn btn-default btn-file">
					<span class="glyphicon glyphicon-folder-open"></span>
                    <b>&nbsp;Fotos</b>                   
						<input type="file" name="fotos[]" id="fotos" multiple="true"/>
                    </span>
                </span>
                <input type="text" class="form-control" readonly>
            </div>-->
			</fieldset>
		</div>
        </div>
    
      </div>
      <div class="modal-footer">
	  	<div class="col-md-3 text-left">
	     <button class="btn btn-primary" type="submit" name="crear"><i class="glyphicon glyphicon-check"></i> Crear</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
		 </form>
		</div>
		
		<?php if($tipo_usuario == 3 || $tipo_usuario == 1) {?>
	  <div class="col-md-3 col-md-offset-4">
 <form class="cliente_excel" name="contact" enctype="multipart/form-data" id="cliente" method="post" action="importar.php">
	  	<div class="input-group">
                <span class="input-group-btn">
                    <span class="btn btn-default btn-file">
					<span class="glyphicon glyphicon-folder-open"></span>
                    <b>&nbsp;Excel</b>                   
						<input type="file" name="excel" id="excel"/>
                    </span>
                </span>
                <input type="text" class="form-control" readonly>
				
            </div>
			</div>
			<div class="col-md-1">
			<button type="submit" class="btn btn-success" name="importar" id="submit"><i class="glyphicon glyphicon-import"></i> Importar</button>
			</form>
			</div>
		<?php } ?>
      </div>
	     
    </div>

  </div>
</div>



<script>
$(function () {
    $("input#submit").click(function(){
        $.ajax({
            type: "POST",
            url: "brm.php", //process to mail
            data: $('form.cliente').serialize(),
            success: function(msg){
                $("#thanks").html(msg) //hide button and show thank you
                $("#form-content").modal('hide'); //hide popup  
            },
            error: function(){
                alert("failure");
            }
        });
    });
});

		
		


//introducir logo formulario
$(document).on('change', '.btn-file :file', function() {
  var input = $(this),
      numFiles = input.get(0).files ? input.get(0).files.length : 1,
      label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
  input.trigger('fileselect', [numFiles, label]);
});

$(document).ready( function() {
    $('.btn-file :file').on('fileselect', function(event, numFiles, label) {
        
        var input = $(this).parents('.input-group').find(':text'),
            log = numFiles > 1 ? numFiles + ' files selected' : label;
        
        if( input.length ) {
            input.val(log);
        } else {
            if( log ) alert(log);
        }
        
    });
});
/*
$(function () {
    $('#cliente').bootstrapValidator({
        message: 'El valor introducido no es válido',
        fields: {
			    nombre: {
                message: 'El nombre de empresa no es válido',
                validators: {
                    notEmpty: {
                        message: 'El nombre de empresa no puede estar vacío'
                    },
                }
            },
			
             alias: {
                message: 'La razón social no es válida',
                validators: {
                    notEmpty: {
                        message: 'El campo logo no puede estar vacío'
                    },
                    
                }
            },
			grupo: {
                validators: {
                    notEmpty: {
                        message: 'El campo logo no puede estar vacío'
                    },
                    
                }
            },
			
			razon: {
                validators: {
                    notEmpty: {
                        message: 'El campo logo no puede estar vacío'
                    },
                    
                }
            },
			rfc: {
                validators: {
                    notEmpty: {
                        message: 'El campo logo no puede estar vacío'
                    },
                    
                }
            },
			
			contacto: {
                validators: {
                    notEmpty: {
                        message: 'El campo logo no puede estar vacío'
                    },
                    
                }
            },
			
        }
    });
});
*/

</script>	

<script>
//Verifica si el nombre del cliente ya existe - formulario de nuevo cliente
$(document).ready(function() {    
    $('#nombre').blur(function(){

        var username = $(this).val();        
		var empresa = '<?php echo $id_empresa;?>';

        $.ajax({
            type: "POST",
            url: "accion_cliente.php",
            data: {username:username, empresa:empresa},
            success: function(data) {
                $('#Info').fadeIn(1000).html(data);
            }
        });
    });              
});    
</script>

<script>
$(document).ready(function() {

    var MaxInputs       = 8; //Número Maximo de Campos
    var contenedor       = $("#contenedor"); //ID del contenedor
    var AddButton       = $("#agregarCampo"); //ID del Botón Agregar

    //var x = número de campos existentes en el contenedor
    var x = $("#contenedor div").length + 1;
    var FieldCount = x-1; //para el seguimiento de los campos

    $(AddButton).click(function (e) {
        if(x <= MaxInputs) //max input box allowed
        {
            FieldCount++;
            //agregar campo
            $(contenedor).append('<br><div class="col-md-5"><input type="text" name="colaborador[]"  class="form-control"  id="campo_'+ FieldCount +'" placeholder="Colaborador"/></div>');
            $(contenedor).append('<div class="col-md-5"><input type="text" name="jerarquia[]"   class="form-control" id="campo_'+ FieldCount +'" placeholder="Jerarquia"/></div></br>');
			x++; //text box increment
        }
        return false;
    });

	/*
    $("body").on("click",".eliminar", function(e){ //click en eliminar campo
        if( x > 1 ) {
            $(this).parent('div').remove(); //eliminar el campo
            x--;
        }
        return false;
    });*/
	
	$("#selectgrupo").change(function(e){
		
       
        var idgrupo = $(this).val();        

        $.ajax({
            type: "POST",
            url: "accion_cliente.php",
            data: {idgrupo:idgrupo},
            success: function(data) {                
				$('#selectestatus').html(data);
            }
        });
			
	});
});
</script>
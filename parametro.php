<!--- MODAL DE PARAMETROS-->		
<!-- Modal -->
<div id="modal_param" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Nuevo Indicador</h4>
      </div>
      <div class="modal-body">
         <form class="cliente" name="contact" enctype="multipart/form-data" id="parametro" method="post" action="crear_parametro.php">
		 <div class="row">
		 <div class="col-md-6" id="col1">
		
		  <div class="form-group">
			 <input type="text" name="nombre" class="form-control" placeholder="Nombre">
			 </div>
			 <!-- <div class="form-group">
			  <input type="text" name="referencia" class="form-control" placeholder="Referencia">
		</div>-->
		</div>
		<div class="col-md-6" id="col2">
			<div class="form-group">
			 <select class="form-control" name="tipo" id="nat">
			<option value="hh:mm" selected>hh:mm</option>
			<option value="$">$</option>
			<option value="nn">nn</option>
			<option value="%">%</option>
			</select>
			</div>
		</div>
             <div class="col-md-2" id="active">
                 <div class="row">
                     <input type="checkbox" id="active" name="active">
                     <label for="active">Subtotal</label>
                 </div>
             </div>
		
		
			
			<!--<div class="form-group">
			<input type="text" name="valor" class="form-control" placeholder="Valor">
		</div>
		-->
		
		<div class="col-md-6">
			<div id="datepicker_new_indicador" style="font-size:10px;"></div>
		</div>
		
		</div>
	</div>
      <div class="modal-footer">
	  	<div class="col-md-6 text-left col-md-offset-8">
	     <button class="btn btn-primary" type="submit" value="Crear" name="crear_param"><i class="glyphicon glyphicon-check"></i> Crear</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
		 </form>
		</div>
		</div>
		
		</div>
	
		</div>
		</div>
	<script>	
	$(function () {
        $('#active').hide();
	    $('#nat').change(function (){
	        console.log($(this).val());
	        if ($(this).val() == '$'){
                $('#col1').removeClass('col-md-6').addClass('col-md-5');
                $('#col2').removeClass('col-md-6').addClass('col-md-5');
                $('#active').show('last');
            } else {
                $('#col1').removeClass('col-md-5').addClass('col-md-6');
                $('#col2').removeClass('col-md-5').addClass('col-md-6');
                $('#active').hide('last');
            }
        });

    $('#parametro').bootstrapValidator({
        message: 'This value is not valid',
        fields: {
              nombre: {
                message: 'El nombre del parametro no es válido',
                validators: {
                    notEmpty: {
                        message: 'El nombre del parametro no puede estar vacío'
                    },
				   }
			    },
     
        }
		
    });
	
});
</script>	
<?php 
include 'set.php';
 session_start();
   $error = "";  
   date_default_timezone_set('America/Mexico_City');
   $fecha = date('Y-m-d');
   $hora = date('H:i:s');

if(isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == 1){ 
 $id_usuario = $_SESSION['id_usuario'];
 $nombre = $_SESSION['nombre_usuario'];
 $tipo_usuario = $_SESSION['tipo_usuario'] ;
 $empresa_usuario =$_SESSION['empresa_usuario']; 
  $id_empresa = $_SESSION['id_empresa'];
}

/*Crea control*/
if(isset($_POST["crear_compl"]))
{  
    $tipo = $_POST["tipo"];
    $nombre = $_POST["nombre"];	
	
	$query1 = mysqli_query($conn,"select * from complementos where nombre_complemento='$nombre' and id_empresa='$id_empresa'");
	
	if(mysqli_num_rows($query1) != 0)
	{
	header('location:brm.php?carp=compl&msj=el control ya existe');
     exit(); }	
	
	$query2 = "Insert into complementos (id_usuario,id_cliente,id_empresa,nombre_complemento,naturaleza,fecha,hora)
	values('$id_usuario','','$id_empresa','$nombre','$tipo','$fecha','$hora')";

	mysqli_query($conn,$query2) or die (mysqli_error());
    
   header('location:brm.php?carp=compl&msj=control creado');
}

/*Actualiza referencia de control*/
if(isset($_POST["up_compl"]))
{    

	  $nat = $_POST["tipo"];
     $nombre = $_POST["nombre"];
	 $referencia = $_POST["referencia"];
	 $valor = $_POST["valor"];
	 $pcliente = $_POST["cliente"];
	 $id_param = $_POST["id_param"];
	 $fecha_captura = date('Y-m-d', strtotime($_POST["date_addref"]));

 
	$query1 = mysqli_query($conn,"select * from referencias_complementos where id_cliente='$pcliente' and id_complemento='$id_param' and referencia='$referencia'");

	
	
	if(mysqli_num_rows($query1) != 0)
	{ header('location:brm.php?cl='.$pcliente.'&carp=compl&file='.$id_param.'&msj=referencia existente');
		exit(); }
	
	$query2 = "Insert into referencias_complementos (id_usuario,id_complemento,id_cliente,referencia,valor,naturaleza,orden,fecha,hora)
	values('$id_usuario','$id_param','$pcliente','$referencia','$valor','$nat','999','$fecha_captura','$hora')";

	mysqli_query($conn,$query2) or die (mysqli_error());
    
   header('location:brm.php?cl='.$pcliente.'&carp=compl&file='.$id_param.'&msj=control actualizado');
}

if(isset($_POST["up_ref"]))
{    
     $pcliente = $_POST["cliente"];
	 $referencia = $_POST["referencia"];
	 $valor = $_POST["valor"];
	 $id_param = $_POST["id_parametro"];
	 $id_com = $_POST["id_com"];
	 $id_ref = $_POST["id_referencia"];
     $nat = $_POST["tipo"];
	 $fecha_captura = date('Y-m-d', strtotime($_POST["date_upref"]));
	 
	
	$query2 = "update referencias_complementos set referencia='$referencia',valor='$valor',fecha='$fecha_captura',hora='$hora' where id='$id_ref'";

    $query3 = "update complementos set fecha='$fecha',hora='$hora', id_usuario='$id_usuario' where id_complemento='$id_com'";

	mysqli_query($conn,$query2) or die (mysqli_error());
    mysqli_query($conn,$query3) or die (mysqli_error());
    
   header('location:brm.php?cl='.$pcliente.'&carp=compl&file='.$id_param.'&msj=referencia actualizada');
}

if(isset($_POST["del_ref"]))
{    
     $pcliente = $_POST["cliente"];
	 $id_param = $_POST["id_parametro"];
	 $id_ref = $_POST["id_referencia"];
     	
	
	$query2 = "delete from referencias_complementos where id='$id_ref'";

	mysqli_query($conn,$query2) or die (mysqli_error());
    
   header('location:brm.php?cl='.$pcliente.'&carp=compl&file='.$id_param.'&msj=referencia eliminada');
}

if(isset ($_GET['ind'])){
foreach ($_GET['ind'] as $position => $item)
{
    $sql = "UPDATE referencias SET orden = $position WHERE id = $item"; 
	mysqli_query($conn,$sql) or die (mysqli_error());	
}

echo "Orden de referencias actualizado";
 
}




if(isset($_POST["up_param"]))
	
{    $tipo = $_POST["tipo"];
     $nombre = $_POST["nombre"];
	 $referencia = $_POST["referencia"];
	 $valor = $_POST["valor"];
     
	
	$query1 = mysqli_query($conn,"select * from parametros where nombre_parametro='$nombre'");
	
	if(mysqli_num_rows($query1) != 0)
	{echo 'este parametro ya existe'; exit(); }	
	
	$query2 = "Insert into parametros (id_usuario,nombre_parametro,referencia, naturaleza,valor)
	values('$cliente','$id_usuario','$nombre','$referencia','$tipo','$valor')";

	mysqli_query($conn,$query2) or die (mysqli_error());
    
   header('location:brm.php');
}


/*Actualiza controles*/
if(isset($_POST["up_compl2"]))
	
{   
	$nat = $_POST["naturaleza"];		

	if ($nat == "dd/mm/aa")
	{$nat="d/m/a";}
	
     $nombre = $_POST["nombre"];
	 $nombrep = $_POST["nombrep"];
	 $id_param = $_POST["id_param"];	 
	 
	$query2 = "update complementos set id_usuario='$id_usuario', nombre_complemento='$nombre',naturaleza='$nat' where nombre_complemento='$nombrep'";

	mysqli_query($conn,$query2) or die (mysqli_error());
    
   header('location:brm.php?carp=compl&file='.$id_param.'&msj=control actualizado');
}


if(isset($_POST["del_compl"]))
{    
     $id_del = $_POST["id_param"];
     	
	$query1 = "delete from complementos where id_complemento='$id_del'";

	mysqli_query($conn,$query1) or die (mysqli_error());
    
   header('location:brm.php?carp=compl&msj=control eliminado');
}

?>
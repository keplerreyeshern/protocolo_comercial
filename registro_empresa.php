<?php 

 include 'set.php';
   session_start();
   error_reporting(E_ALL);
	ini_set('display_errors', 1);
   
if(isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == 1){ 
 $id_usuario = $_SESSION['id_usuario'];
$nombre = $_SESSION['nombre_usuario'];
 $tipo_usuario = $_SESSION['tipo_usuario'] ;
}



//codigo para verificar registro de empresa
if(isset($_POST["registrar"])) {

	
$final_save_dir = 'logos/';
move_uploaded_file($_FILES["logo"]['tmp_name'], $final_save_dir . $_FILES["logo"]['name']);
$imagen = $final_save_dir . $_FILES['logo']['name'];

//inserta en la bd la empresa
if(isset($_POST["empresa"])) {$nombre_empresa = $_POST["empresa"];}
if(isset($_POST["razon"])) {$razon_empresa = $_POST["razon"];}      	
	
$query = "INSERT INTO empresa(nombre_empresa,razon,logo)VALUES('$nombre_empresa','$razon_empresa','$imagen')";

mysqli_query($conn,$query) or die (mysqli_error());

   header('location:admin.php');
   exit();
}


require 'header.php';

?>
<body class="body-login">
 
<div class="wrapper container">   
     
<form enctype="multipart/form-data"  method="post" action="registro_empresa.php" id="form-registro-empresa" class="col-md-4 col-md-offset-4">
<div class="row">
	<div class="col-md-12">
		<h2>Registro de Empresa</h2>
		<hr></hr>
	</div>
</div>

<div class="row">

<div class="col-md-12">

		<div class="form-group">
		<label class=" control-label">Nombre</label>
		
		<input type="text" class="form-control" name="empresa" />
		
		</div>
		<div class="form-group">
		<label class="control-label">Razón Social</label>
		
		<input type="text" class="form-control" name="razon"/>
		
		</div>
		<div class="form-group">
		<label class="control-label" id="captchaOperation"></label>
		
		<input type="text" class="form-control" name="captcha" />
		
		</div>
		<div class="form-group">		
		<div class="input-group">
						<span class="input-group-btn">
							<span class="btn btn-default btn-file">
							<span class="glyphicon glyphicon-folder-open"></span>
							<b>&nbsp;Logo</b>                   
								<input type="file" name="logo" id="logo"/>
							</span>
						</span>
						<input type="text" class="form-control" readonly>
					</div>		
		</div>
		
	</div>	

     
</div>


	
<div class="row">
<hr></hr>
	<div class="form-group">
	<div class="col-md-12">
		<button type="submit" class="btn btn-primary" value="Registrar" name="registrar"/><i class="glyphicon glyphicon-check"></i> Registrar</button>
		<a href="admin.php" class="btn btn-info"> <i class="glyphicon glyphicon-menu-left"></i> Volver</a>
	</div>
	</div>
</div>	

</form>
</div>

<script type="text/javascript">

$(function () {
    // Generate a simple captcha
    function randomNumber(min, max) {
        return Math.floor(Math.random() * (max - min + 1) + min);
    };
    $('#captchaOperation').html([randomNumber(1, 100), '+', randomNumber(1, 200),'='].join(' '));

    $('#form-registro-empresa').bootstrapValidator({
        message: 'El valor introducido no es válido',
        fields: {
			    empresa: {
                message: 'El nombre de empresa no es válido',
                validators: {
                    notEmpty: {
                        message: 'El nombre de empresa no puede estar vacío'
                    },
                    stringLength: {
                        min: 6,
                        max: 30,
                        message: 'El nombre de empresa debe tener mínimo 6 caracteres'
                    },
                   /* regexp: {
                        regexp: /^[a-zA-Z0-9_\.]+$/,
                        message: 'El nombre de empresa sólo puede contener , números, puntos o subrayados'
                    },
                      */
                }
            },
			
        razon: {
                message: 'La razón social no es válida',
                validators: {
                    notEmpty: {
                        message: 'La razón social de la empresa no puede estar vacía'
                    },
                    
                }
            },
			logo: {
                validators: {
                    notEmpty: {
                        message: 'El campo logo no puede estar vacío'
                    },
                    
                }
            },
			
            captcha: {
                validators: {
                    callback: {
                        message: 'Respuesta Incorrecta',
                        callback: function(value, validator) {
                            var items = $('#captchaOperation').html().split(' '), sum = parseInt(items[0]) + parseInt(items[2]);
                            return value == sum;
                        }
                    }
                }
            }
        }
    });
});

//introducir logo formulario
$(document).on('change', '.btn-file :file', function() {
  var input = $(this),
      numFiles = input.get(0).files ? input.get(0).files.length : 1,
      label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
  input.trigger('fileselect', [numFiles, label]);
});

$(document).ready( function() {
   
   $('.btn-file :file').on('fileselect', function(event, numFiles, label) {
        
        var input = $(this).parents('.input-group').find(':text'),
            log = numFiles > 1 ? numFiles + ' files selected' : label;
        
        if( input.length ) {
            input.val(log);
        } else {
            if( log ) alert(log);
        }
        
    });
		
});

</script>

</body>


<?php
/** Error reporting */
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);

/** Include PHPExcel */
require_once dirname(__FILE__) . '/Classes/PHPExcel.php';
include dirname(__FILE__) .'/Classes/PHPExcel/IOFactory.php';
include 'set.php';

/** Datos de sesion **/
session_start();

/** Header UTF-8 **/


if(isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == 1){ 
 $id_usuario = $_SESSION['id_usuario'];
 $nombre = $_SESSION['nombre_usuario'];
 $tipo_usuario = $_SESSION['tipo_usuario'] ;
 $empresa_usuario =$_SESSION['empresa_usuario']; 
$id_empresa = $_SESSION['id_empresa'];
 }
 

if(isset($_POST['importar'])){ //importacion de clientes
	
	$path = $_FILES['excel']['tmp_name'];
   $objPHPExcel = PHPExcel_IOFactory::load($path);
   
foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
    $worksheetTitle     = $worksheet->getTitle();
    $highestRow         = $worksheet->getHighestRow(); // e.g. 10
    $highestColumn      = $worksheet->getHighestColumn(); // e.g 'F'
    $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
    $nrColumns = ord($highestColumn) - 64;
   
}

for ($row = 2; $row <= $highestRow; ++ $row) {
    $val=array();
for ($col = 0; $col < $highestColumnIndex; ++ $col) {
   $cell = $worksheet->getCellByColumnAndRow($col, $row);
   $val[] =utf8_decode($cell->getValue());
}


	if($row == 2){		//comparaciones de columnas 
		
		if ( strcasecmp ($val[0] , 'Nombre') == 0 && strcasecmp ($val[1] , 'Activo') == 0 && strcasecmp ($val[2] , 'Grupo de Clientes') == 0  && strcasecmp ($val[5] , utf8_decode('Claves Telefónicas Asignadas')) == 0
									&& strcasecmp ($val[6] , utf8_decode('Razón Social')) == 0 && strcasecmp ($val[7] , utf8_decode('Calle')) == 0 && strcasecmp ($val[8] , utf8_decode('Número externo')) == 0
									&& strcasecmp ($val[9] , utf8_decode('Número interno')) == 0 && strcasecmp ($val[10] , utf8_decode('Colonia')) == 0  
									&& strcasecmp ($val[11] , utf8_decode('Municipio / Delegación')) == 0 && strcasecmp ($val[12] , utf8_decode('Estado')) == 0 
									&& strcasecmp ($val[13] , utf8_decode('Localidad')) == 0 && strcasecmp ($val[14] , utf8_decode('Código Postal')) == 0 
									&& strcasecmp ($val[15] , utf8_decode('Número RFC')) == 0 && strcasecmp ($val[16] , utf8_decode('Oficina o N° Membresía')) == 0 
									&& strcasecmp ($val[17] , utf8_decode('Teléfono')) == 0 && strcasecmp ($val[18] , utf8_decode('E-Mail')) == 0
									&& strcasecmp ($val[19] , utf8_decode('Persona Contacto')) == 0) 
		{
			$go = 1; // Todas la columnas normales estan correctas
			
			if(isset ($val[21]) && !empty($val[21]) && strcasecmp ($val[21] , 'Facebook') != 0) 
			{$go = 0; } // comprueba columna de facebook
		    	
			if(isset($val[22]) && !empty($val[22]) && strcasecmp ($val[22] , 'Twitter') != 0)
			{$go = 0;} // comprueba columna de twitter
		    
			if(isset($val[23]) && !empty($val[23]) && strcasecmp ($val[23] , utf8_decode('Tratamiento')) != 0)
			{$go = 0; }// comprueba columna de tratamiento
			
			if(isset($val[24]) && !empty($val[24]) && strcasecmp ($val[24] , utf8_decode('Atributos')) != 0) 
			{ $go = 0; } // comprueba columna de atributos
			
			if(isset($val[25]) && !empty($val[25]) && strcasecmp ($val[25] , utf8_decode('Contestación')) != 0)
			{ $go = 0;} // comprueba columna de contestacion
			
		} else {
			$go = 0; //validacion de las columnas normales incorrectas
		}
	}

if($row > 2 && $go ==1){
//Verifica si hay clientes duplicados 
$cliente_dupl = mysqli_query($conn,"select nombre from cliente where nombre='$val[0]' and id_empresa='$id_empresa'"); 


if(mysqli_num_rows($cliente_dupl)>0) 
{ 
$msj=' en el archivo existian clientes duplicados';
}

else { //inserta en la base de datos los datos de clientes

if($val[0] != "")	{ 

$query = "Insert into cliente (id_usuario,id_empresa,nombre, grupo, razon, rfc, oficina, estatus,clave_telefono)
 values ('$id_usuario','$id_empresa','$val[0]','$val[2]','$val[6]','$val[15]','$val[16]','$val[1]','$val[5]')";

   mysqli_query($conn,$query) or die (mysqli_error($conn));
   
   $idc=mysqli_insert_id($conn);
   
		if(isset($val[21])){$fb = $val[21];} else {$fb = "";}
		if(isset($val[22])){$tw = $val[22];} else {$tw = "";}
		if(isset($val[23])){$des = $val[23];} else {$des = "";}
		if(isset($val[24])){$sl = $val[24];} else {$sl = "";}
		if(isset($val[25])){$con = $val[25];} else {$con = "";} 
		 
$query1 = "insert into general_cliente (id_cliente,id_usuario,id_empresa,calle,interno,externo, colonia, municipio, estado, localidad, postal, contactos,telefono,
email,facebook,twitter) values ('$idc','$id_usuario','$id_empresa','$val[7]','$val[9]','$val[8]','$val[10]','$val[11]','$val[12]','$val[13]',
'$val[14]','$val[19]','$val[17]','$val[18]','$fb','$tw')";

 mysqli_query($conn,$query1) or die (mysqli_error($conn));

 
 $query2 = "insert into protocolo_cliente (id_cliente,id_usuario,id_empresa,descripcion,slogan,contestacion) 
 values ('$idc','$id_usuario','$id_empresa','$des','$sl','$con')";

 mysqli_query($conn,$query2) or die (mysqli_error($conn));
 
	$msj = 'clientes importados';
					}
			}

	}
	
		if($row > 2 && $go ==0){ // Algun error en los nombres de columnas
	
			$msj = 'los nombres de columnas estan incorrectos, verifique el archivo';
		}	
		
		
			
		} //fin del bucle de lectura de columnas
		
		header('location:brm.php?msj=importacion de clientes - '.$msj.'');
	}
	
if(isset($_POST['importar_record'])){ //importacion de recordatorios
	
	$path = $_FILES['excel']['tmp_name'];
   $objPHPExcel = PHPExcel_IOFactory::load($path);
foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
    $worksheetTitle     = $worksheet->getTitle();
    $highestRow         = $worksheet->getHighestRow(); // e.g. 10
    $highestColumn      = $worksheet->getHighestColumn(); // e.g 'F'
    $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
    $nrColumns = ord($highestColumn) - 64;
  
}

for ($row = 3; $row <= $highestRow; ++ $row) {
    $val=array();
for ($col = 0; $col < $highestColumnIndex; ++ $col) {
   $cell = $worksheet->getCellByColumnAndRow($col, $row);
   $val[] =utf8_decode($cell->getValue());
}

$query1 = mysqli_query($conn,"select id_cliente,razon from cliente where nombre = '$val[1]'");
    while($qcr = mysqli_fetch_array($query1)){
	$r_id = $qcr['id_cliente'];
	$razon = $qcr['razon'];
	}
	
	 if(empty($r_id)){
    header('location:brm.php?msj=Recordatorios no importados - el cliente ' .$val[1]. ' no se encuentra en la base de datos');
	 }
	 
    $msj = "Estimado Cliente " .$razon."\n \n"; 
	$msj.= "por este medio le informamos que nuestros registros muestran la Liquidaci&oacute;n n&uacute;mero ".$val[0].","; 
    $msj.=  "correspondiente al ".substr($val[5],0,-8).",con un importe de $".number_format($val[7], 2, '.', ',').", la cual aparece ".$val[3].",";
	$msj.=	"por lo que le suplicamos aclare esta situaci&oacute;n.\n";
	$msj .= "Quedamos a sus &oacute;rdenes.\n\n";
	$msj .= "ATENTAMENTE\n";
	$msj .= $empresa_usuario;
			
    		
	 $query2 = "Insert into recordatorios (id_cliente, id_usuario, id_empresa,id_recordatorio,cliente,asunto,fecha,hora,contenido,status)
	 values('$r_id','$id_usuario','$id_empresa','$val[0]','$val[1]','Recordatorio de pago',now(),now(),'$msj','no')";
	  
	 if(mysqli_query($conn,$query2)){
	
			header('location:brm.php?msj=recordatorios importados');
					}
		else {
			header('location:brm.php?msj=existen recordatorios duplicados en el archivo');
					}
	}

}	
?>


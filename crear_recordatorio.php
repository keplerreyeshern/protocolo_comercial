<?php 
include 'set.php';
 session_start();
   $error = "";  
   date_default_timezone_set('America/Mexico_City');
  $fecha = date('Y-m-d');
   $hora = date('H:i:s');
   
   
if(isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == 1){ 
		 $id_usuario = $_SESSION['id_usuario'];
		 $nombre = $_SESSION['nombre_usuario'];
		 $asunto_usuario = $_SESSION['asunto_usuario'] ;
		 $empresa_usuario =$_SESSION['empresa_usuario']; 
		 $id_empresa = $_SESSION['id_empresa'];
}

if(isset($_POST["guardar_recor"])){
	  
		$r_id = $_POST["cliente"];
		$asunto = $_POST["asunto"];
		$mensaje = $_POST["mensaje"];
        $estado = $_POST["estado"];
		
		$edate=strtotime($_POST['expiracion']); 
        $expiracion=date("Y-m-d",$edate);
		
		$query1 = mysqli_query($conn,"select nombre from cliente where id_cliente = '$r_id'");
        $rquery1 = mysqli_fetch_assoc($query1);
		$cliente = $rquery1["nombre"];	
		$hora_exp = $_POST["us_time"];

		$query2 = "Insert into recordatorios (id_cliente, id_usuario,id_empresa,cliente,asunto,fecha,hora,contenido,status,hora_exp)
		values('$r_id','$id_usuario','$id_empresa','$cliente','$asunto','$expiracion','$hora','$mensaje','no','$hora_exp')";

	   mysqli_query($conn,$query2) or die (mysqli_error());
    
   header('location:brm.php?cl='.$r_id.'&carp=recor&msj=recordatorio creado');	
}


if(isset($_POST["importar_record"]))
{    $asunto = $_POST["asunto"];
    
	$file = $_FILES['excel']['tmp_name'];
	$handle = fopen($file, "r");
	$c=0;
	fgetcsv($handle);
	while(($filesop = fgetcsv($handle, 1000, ",")) !== false)
	{
		$id = $filesop[0];
		$cliente = $filesop[1];
		//$asunto = $filesop[2];
        $asunto = "Recordatorio de pago";
		$estado = $filesop[3];
        $fecha_desde = $filesop[4];
        $fecha_hasta = $filesop[5];
        $fecha_saldo = $filesop[6];
        $saldo_li = $filesop[7];
        $saldo_mo = $filesop[8];
        $saldo_to = $filesop[9];
	
    $query1 = mysqli_query($conn,"select id_cliente,razon from cliente where nombre = '$cliente'");
    while($qcr = mysqli_fetch_array($query1)){
	$r_id = $qcr['id_cliente'];
	$razon = $qcr['razon'];
	}
	
	 if(empty($r_id)){
    header('location:brm.php?msj=Recordatorios no importados - el cliente ' .$cliente. ' no se encuentra en la base de datos');
	 }
	 
    $msj = "Estimado Cliente " .$razon."\n \n"; 
	$msj.= "por este medio le informamos que nuestros registros muestran la Liquidación número ".$id.","; 
    $msj.=  "correspondiente al ".substr($fecha_hasta,0,-8).",con un importe de $".number_format($saldo_li, 2, '.', ',').", la cual aparece ".$estado.",";
	$msj.=	"por lo que le suplicamos aclare esta situación.\n";
	$msj .= "Quedamos a sus órdenes.\n\n";
	$msj .= "ATENTAMENTE\n";
	$msj .= $empresa_usuario;
			
    		
	 $query2 = "Insert into recordatorios (id_cliente, id_usuario, id_empresa,id_recordatorio,cliente,asunto,fecha,hora,contenido,status,hora_exp)
	 values('$r_id','$id_usuario','$id_empresa','$id','$cliente','$asunto','$fecha','$hora','$msj','no','$hora')";
      
	  //echo $query2;
	  
	 if(mysqli_query($conn,$query2)){
	
			header('location:brm.php?msj=recordatorios importados');
					}
		else {
			header('location:brm.php?msj=existen recordatorios duplicados en el archivo');
					}
	}

		
}


if(isset($_POST["recor_up"]))
{   
	 $id_recor = $_POST["id"];
	 $asunto = $_POST["asunto"];
     $contenido = $_POST["contenido"];
	 $edate=strtotime($_POST['expiracion']); 
     $expiracion=date("Y-m-d",$edate);
	 $hora_exp = $_POST['us_time'];
	
	 if(isset($_POST["cliente"])){
	 	$cliente = $_POST["cliente"];
		$query = "update recordatorios set id_cliente = '$cliente', id_usuario = '$id_usuario',asunto ='$asunto', contenido='$contenido',
		fecha = '$expiracion', hora = '$hora',status='no', hora_exp = '$hora_exp' where id_recordatorio='$id_recor'";


		mysqli_query($conn,$query) or die (mysqli_error());
 header('location:brm.php?carp=ale&msj=recordatorio actualizado');
}

 if(isset($_POST["cl"])){
	 	$cl = $_POST["cl"];
     $query = "update recordatorios set id_cliente = '$cl', id_usuario = '$id_usuario', asunto ='$asunto', contenido='$contenido',
	fecha = '$expiracion', hora = '$hora',status='no', hora_exp = '$hora_exp' where id_recordatorio='$id_recor'";

	mysqli_query($conn,$query) or die (mysqli_error());
	header( 'location:brm.php?cl='.$cl.'&carp=recor&msj=recordatorio actualizado');
 }

}


if(isset($_POST["recor_del"]))
{    
     $id_recor = $_POST["id"];
	$query1 = "delete from recordatorios where id_recordatorio='$id_recor'";
	mysqli_query($conn,$query1) or die (mysqli_error());
    
     
	 if(isset($_POST["cl"])){
		 $cl = $_POST["cl"];
		 header( 'location:brm.php?cl='.$cl.'&carp=recor&msj=recordatorio eliminado');
	 }
else {	 
  header('location:brm.php?carp=ale&msj=recordatorio eliminado');
}

}

if(isset($_POST["tratado_recor"]))
{    
	$id_recor = $_POST["id_recordatorio"];
	 
	$query = "update recordatorios set status='si' where id_recordatorio='$id_recor'";

	mysqli_query($conn,$query) or die (mysqli_error());
	
	if(isset($_POST['cliente']) && $_POST['cliente'] != ""){
	
	$cl = $_POST['cliente'];	
	
	header('location:brm.php?cl='.$cl.'&carp=recor&file='.$id_recor.'&msj=recordatorio marcado como tratado');	
	
	}
	
	else {
		header('location:brm.php?carp=ale&msj=recordatorio marcado como tratado');
	}
	
}

?>
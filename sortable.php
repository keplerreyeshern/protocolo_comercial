<script>
$(".table-cola > tbody").sortable({
    items: 'tr:has(td)',
	connectWith: 'tbody',
	 axis: 'y',
	 update: function( event, ui ) {
        var serial = $(".table-cola > tbody").sortable('serialize');
        $(".row-footer").load("accion_protocolo.php?"+serial); 
		
      }
});

$(".table-param > tbody").sortable({
    items: 'tr:has(td)',
	connectWith: 'tbody',
	 axis: 'y',
	 update: function( event, ui ) {
        var serial = $(".table-param > tbody").sortable('serialize');
        $(".row-footer").load("accion_parametro.php?"+serial); 
		location.reload();
      }
});

$(".table-compl > tbody").sortable({
    items: 'tr:has(td)',
	connectWith: 'tbody',
	 axis: 'y',
	 update: function( event, ui ) {
        var serial = $(".table-compl > tbody").sortable('serialize');
        $(".row-footer").load("accion_complemento.php?"+serial); 
		location.reload();
      }
});


  $(".table-list-cli").tablesorter({
      debug: false,

      dateFormat: "uk", // set the default date format


      textExtraction: function (node) {
          var $node = $(node)
          var text = $node.text();
          if ($node.hasClass('nat_moneda')) {
              text = text.replace(/[$,]/g, '');
          }
          ;
          if ($node.hasClass('nat_porcentaje')) {
              text = text.replace(/[%,]/g, '');
          }
          ;
          if ($node.hasClass('nat_hora')) {
              text = text.replace(':', '.');
          }
          ;
          if ($node.hasClass('nat_numero')) {
              text = text.replace(',', '');
          }
          ;
          return text;
      }
  });
</script>
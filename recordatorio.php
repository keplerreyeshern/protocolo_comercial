<?php 
if($tipo_usuario == 1 ){ 
	if($id_empresa != 1)
	{$qem = "where id_empresa='".$id_empresa."'";}
	else {$qem = "";}	
	
$query = mysqli_query($conn,"select * from cliente ".$qem." order by nombre");
}
if($tipo_usuario == 2 | $tipo_usuario == 3  ){ 
$query = mysqli_query($conn,"select * from cliente where id_empresa='$id_empresa' order by nombre");
}

if(isset ($_GET['cl']))
$cl = $_GET['cl'];
?>
<!--- MODAL DE RECORDATORIOS-->		
<!-- Modal -->
<div id="modal_record" class="modal fade" role="dialog">
  <div class="modal-dialog modal-record">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Nuevo Recordatorio</h4>
      </div>
	  
      <div class="modal-body"> 
	 
	<form class="cliente_excel" name="contact" enctype="multipart/form-data" id="recordatorio" method="post" action="crear_recordatorio.php">
 <div class="row">
	    	<div class="col-md-6">		
		<div class="form-group">
		 <select class="form-control" name="cliente">
		 <!--<option value="" disabled selected>Cliente Destinatario</option>-->
		<?php 
		if (isset($cl)){
		while ($r_cl = mysqli_fetch_array($query)){
			
			if($r_cl["id_cliente"] == $cl){
		      	echo '<option value="'.$r_cl["id_cliente"].'" selected>'.$r_cl["nombre"].'</option>';	
			} 
            else{			
			echo '<option value="'.$r_cl["id_cliente"].'">'.$r_cl["nombre"].'</option>';
			}
		}
		}
		else {
			echo '<option value="" selected disabled> Seleccione Cliente </option>';
			while ($r_cl = mysqli_fetch_array($query)){
			
           echo '<option value="'.$r_cl["id_cliente"].'">'.$r_cl["nombre"].'</option>';			
		}
		}
		?>
			</select>
			</div>
		
			<div class="form-group">
			  <input type="text" name="asunto" class="form-control" placeholder="Asunto">
			  </div>
           <div class="form-group">	
		<textarea name="mensaje" class="form-control" placeholder="Mensaje" rows="5"></textarea></br>
		</div>

       
		</div>
		
		<div class="col-md-6">		
			<div class="form-group">
			<div id="datepickern" style="font-size:10.5px;"></div>
			  </div>	
		</div>
		
		<div class="col-md-6">		
			<div class="form-group">
				<input type="time" name="us_time" placeholder="Hora" class="campo_hora">
			</div>	
		</div>
		
			</div>

        </div>
      <div class="modal-footer">
	  	  		<div class="col-md-5">
				 <button class="btn btn-primary" type="submit" name="guardar_recor"><i class="glyphicon glyphicon-check"></i> Guardar</button>
				 </form>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
	            </div>
		
		<?php if($tipo_usuario == 3 || $tipo_usuario == 1) {?>
		<form class="cliente_excel" name="contact" enctype="multipart/form-data" id="cliente" method="post" action="importar.php">

		 <div class="col-md-5">
		
		<div class="input-group">
		<span class="input-group-btn">
                   <span class="btn btn-default btn-file">
					<span class="glyphicon glyphicon-folder-open"></span>
                    <b>&nbsp;Excel</b>                   
						<input type="file" name="excel" id="excel" />
                    </span>
                </span>
                <input type="text" class="form-control" readonly>
				</div>
            </div>
			
			<div class="col-md-2">
			<button type="submit" class="btn btn-success" name="importar_record" id="submit"><i class="glyphicon glyphicon-import"></i> Importar</button>
			</form>
			</div>
		<?php } ?>
	</div>
</div>
</div>
</div>

	
<script>
$(function () {

    $('#recordatorio').bootstrapValidator({
        message: 'This value is not valid',
        fields: {
              asunto: {
                message: 'El asunto del recordatorio no es válido',
                validators: {
                    notEmpty: {
                        message: 'El asunto del recordatorio no puede estar vacío'
                    },
				   }
			    },
			
            mensaje: {
                message: 'El contenido del recordatorio no es válido',
                validators: {
                    notEmpty: {
                        message: 'El contenido del recordatorio no puede estar vacío'
                    }
                    
                }
            },
			
			    expiracion: {
                message: 'El contenido del recordatorio no es válido',
                validators: {
                    notEmpty: {
                        message: 'El contenido de la fecha no puede estar vacío'
                    }
                    
                }
            },
			
           
     
        }
		
    });
	
			$( "#datepickern" ).datepicker({minDate: '0' });
			
				var n = $( "#datepickern" ).val();
				if(n != null){
				$('#recordatorio').append('<input type="hidden" name="expiracion" id="date" value="'+n+'"/>'); 
				}	
			$( "#datepickern" ).change(function() {
			var n = $( "#datepickern" ).val();
			$('#date').val(n);
			
		});

		//Valida campo de hora
		$(".campo_hora").timepicker();
		
});
</script>
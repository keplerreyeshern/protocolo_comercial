<?php 

 include 'set.php';
 error_reporting(E_ALL);
 ini_set('display_errors', 1);
 
 //Establece la duracion de la sesion en segundos, definida por el admin
	$qseg =mysqli_query($conn,"SELECT duracion_sesion FROM configuraciones WHERE id_conf= '1'");
	$rqseg = mysqli_fetch_assoc($qseg);
	$value_s = $rqseg['duracion_sesion']; 
	$expiry = $value_s*60;
	$expiry_max= $value_s + 100000;
	ini_set('session.cookie_lifetime', $expiry);
    ini_set('session.gc_maxlifetime', $expiry_max);
	session_start();
	$error = "";

	$sid = session_id();

if(isset($_SESSION['loggedin'])){ 


if($_SESSION['loggedin'] == 1 && $_SESSION['tipo_usuario'] == 1 ){
	header('location:admin.php');
}

else {
	header('location:brm.php');
} 

}
   
if(isset($_POST['login']) || isset($_COOKIE['admin_login']))
{

	
	if(isset($_POST['login'])){
		
	$nombre = $_POST['username'];
	$password = $_POST['password'];
	$empresa = $_POST['empresa'];
	
	}
	else if(isset($_COOKIE['admin_login'])){
		
		$cookie = json_decode($_COOKIE["admin_login"]);
		
		$data = $cookie->data;
		
		$value1 = $cookie->data->valor1;
		$value2 = $cookie->data->valor2;
		$value3 = $cookie->data->valor3;
		
		$nombre = $value1; 
		$empresa = $value2;
		$password = $value3;
		
	}
	

//ubica el id de la empresa
$query = mysqli_query($conn,"select id_empresa,nombre_empresa from empresa where nombre_empresa = '$empresa'");
while($row = mysqli_fetch_array($query))
 {
    $id_empresa = $row["id_empresa"]; 
	$empresa_usuario = $row["nombre_empresa"];
	
 }	

if(isset($id_empresa)){	
$query1 = mysqli_query($conn,"select * from usuario where nombre = '$nombre' and password = '$password' and id_empresa='$id_empresa'");
$conteo = mysqli_num_rows($query1);

while($row1 = mysqli_fetch_array($query1))
 {
    $tipo_usuario = $row1["tipo"];
	$id_usuario = $row1["id_usuario"];
	$id_empresa = $row1["id_empresa"]; 
	$status = $row1["status"];
	  
	
 }
}



else {
	$error = '<div class="alert alert-danger">Nombre de empresa incorrecto</div>';
}

 if (isset ($conteo)&& $conteo == 0){

	$error = '<div class="alert alert-danger">Datos de usuario incorrectos</div>';
}


//Verifica la sesion
$conteos = 0;
if(isset($id_usuario) && $id_usuario  != 1){
$querys1 = mysqli_query($conn,"select * from sesiones where usuario = '$id_usuario'");
$conteos = mysqli_num_rows($querys1);

}
 
if(isset($status) && $status == "activo"){

if ($conteo == 1 && $conteos == 0 && $id_empresa == 1){ //Verifica la sesion de usuario administrador
	$_SESSION['loggedin'] = 1; 
	$_SESSION['id_usuario'] = $id_usuario;
    $_SESSION['nombre_usuario'] = $nombre;
    $_SESSION['tipo_usuario'] = $tipo_usuario;
	$_SESSION['empresa_usuario'] = $empresa_usuario;
	$_SESSION['id_empresa'] = $id_empresa;
	
	
	$querys2 = "Insert into sesiones (usuario,id_sesion)values('$id_usuario','$sid')";

	$result_sesion = mysqli_query($conn,$querys2);
	
	$data = (object) array( "valor1" => $nombre, "valor2" => $empresa_usuario, "valor3" => $password);
    $cookieData = (object) array( "data" => $data, "expiry" => time()+$expiry );
	setcookie ("admin_login",json_encode($cookieData),time()+$expiry); //Crea la cookie de sesion
	
		header("location:admin.php");	
	
}
else if ($conteo == 1 && $conteos == 0){ //Verifica sesion de usuarios no administradores
	$_SESSION['loggedin'] = 1; 
	$_SESSION['id_usuario'] = $id_usuario;
    $_SESSION['nombre_usuario'] = $nombre;
    $_SESSION['tipo_usuario'] = $tipo_usuario;
	$_SESSION['empresa_usuario'] = $empresa_usuario;
    $_SESSION['id_empresa'] = $id_empresa;
	
	$querys2 = "Insert into sesiones (usuario,id_sesion)values('$id_usuario','$sid')";

	$result_sesion = mysqli_query($conn,$querys2);
	
	if($result_sesion){
		
		$data = (object) array( "valor1" => $nombre, "valor2" => $empresa_usuario, "valor3" => $password);
		$cookieData = (object) array( "data" => $data, "expiry" => time()+$expiry );
		setcookie ("admin_login",json_encode($cookieData),time()+$expiry); //Crea la cookie de sesion
		
		header("location:brm.php");	
	}
	
	else {
		$error = '<div class="alert alert-danger">Sesion iniciada en otro dispositivo
		<p><a href="logout.php?id_user='.$id_usuario.'" >Cerrar Sesion</a></p></div>';
		}
	
	}

}

if(isset($status) && $status=="bloqueado"){
	$error = '<div class="alert alert-danger">Su usuario ha sido bloqueado. Contacte al Administrador</div>';

	}
if(isset($status) && $status=="cancelado"){
	$error = '<div class="alert alert-danger">Su usuario ha sido dado de baja. Contacte al Administrador</div>';

	}


if($conteos == 1){
	$error = '<div class="alert alert-danger">Sesion iniciada en otro dispositivo
	<p><a href="logout.php?id_user='.$id_usuario.'" >Cerrar Sesion</a></p></div>';
	}	

}

require 'header.php';
?>
    <body class="body-login">
 
        <div class="wrapper">  

<form id="defaultForm" method="post" class="form-signin col-md-4 col-md-offset-4 form-horizontal" action="index.php">
 <h2 class="form-signin-heading">Login<hr></hr></h2>
 <?php if(isset($error)&& $error != ""){echo $error;}?>
 <div class="form-group">
<label class="col-lg-3 control-label">Empresa</label>
<div class="col-lg-9">
<input type="text" class="form-control" name="empresa" />
</div>
</div>
<div class="form-group">
<label class="col-lg-3 control-label">Usuario</label>
<div class="col-lg-9">
<input type="text" class="form-control" name="username" />
</div>
</div>
<div class="form-group">
<label class="col-lg-3 control-label">Password</label>
<div class="col-lg-9">
<input type="password" class="form-control" name="password" />
</div>
</div>

<div class="form-group">
<label class="col-lg-3 control-label" id="captchaOperation"></label>
<div class="col-lg-6">
<input type="text" class="form-control" name="captcha" />
</div>
</div>

<div class="form-group">
<div class="col-lg-9 col-lg-offset-3">
<button type="submit" class="btn btn-primary" name="login"><i class="glyphicon glyphicon-log-in"></i> Iniciar</button>
<!--<a href="registro_usuario.php" class="btn btn-info"> <i class="glyphicon glyphicon-user"></i> Registro de Usuario</a>-->
</div>
</div>
</form>
</div>

<script type="text/javascript">

$(function () {
	    // Generate a simple captcha
    function randomNumber(min, max) {
        return Math.floor(Math.random() * (max - min + 1) + min);
    };
    $('#captchaOperation').html([randomNumber(1, 100), '+', randomNumber(1, 200),'='].join(' '));

    $('#defaultForm').bootstrapValidator({
        message: 'This value is not valid',
        fields: {
              empresa: {
                message: 'El nombre de empresa no es válido',
                validators: {
                    notEmpty: {
                        message: 'El nombre de empresa no puede estar vacío'
                    },
                   /* stringLength: {
                        min: 6,
                        max: 30,
                        message: 'El nombre de empresa debe tener mínimo 6 caracteres'
                    },
                    regexp: {
                        regexp: /^[a-zA-Z0-9_\.]+$/,
                        message: 'El nombre de empresa sólo puede contener , números, puntos o subrayados'
                    },*/
                    different: {
                        field: 'password',
                        message: 'El nombre de empresa y la contraseña no pueden ser iguales'
                    }
                }
            },
			
            username: {
                message: 'El nombre de usuario no es válido',
                validators: {
                    notEmpty: {
                        message: 'El nombre de usuario no puede estar vacío'
                    },
                    stringLength: {
                        min: 6,
                        max: 30,
                        message: 'El nombre de usuario debe tener mínimo 6 caracteres'
                    },
                    regexp: {
                        regexp: /^[a-zA-Z0-9_\.]+$/,
                        message: 'El nombre de usuario sólo puede contener , números, puntos o subrayados'
                    },
                    different: {
                        field: 'password',
                        message: 'El nombre de usuario y la contraseña no pueden ser iguales'
                    }
                }
            },
            password: {
                validators: {
                    notEmpty: {
                        message: 'La contraseña no puede estar vacía'
                    },
                   
                    different: {
                        field: 'username',
                        message: 'La contraseña y el nombre de usuario no pueden ser iguales'
                    }
                }
            },
			      captcha: {
                validators: {
                    callback: {
                        message: 'Respuesta Incorrecta',
                        callback: function(value, validator) {
                            var items = $('#captchaOperation').html().split(' '), sum = parseInt(items[0]) + parseInt(items[2]);
                            return value == sum;
                        }
                    }
                }
            }
            
     
        }
    });
});
</script>
</body>
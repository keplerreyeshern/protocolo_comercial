<?php

	include 'set.php';
	error_reporting(E_ALL);
	ini_set('display_errors', 1);
	date_default_timezone_set('America/Mexico_City');

	session_start();


	$error = "";
	$date = date('d/m/y H:i:s');
	$fechap = date('d/m/y');

if(isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == 1){
 $id_usuario = $_SESSION['id_usuario'];
 $nombre = $_SESSION['nombre_usuario'];
 $tipo_usuario = $_SESSION['tipo_usuario'];
 $empresa_usuario =$_SESSION['empresa_usuario'];
 $id_empresa = $_SESSION['id_empresa'];

}

else {header('location:index.php');}

include ('busqueda.php');

require 'header.php';
?>
<body onload="relojillo()">
<div class="container-fluid frame">
  <div class="row">
       <div class="col-md-5 col-md-offset-3 data-general">
       <h4>BRM - <?php echo $empresa_usuario.' - '.$nombre.' '.$fechap ;?> <span id="horeja"></span></h4>
	   </div>	<?php /*echo $_SESSION['LAST_ACTIVITY'];
	    echo '<br>life1'.ini_set('session.cookie_lifetime',$value_s);
		echo '<br>life2'.ini_set('session.gc_maxlifetime', $value_s);*/?>
	  <div class="col-md-2 text-right">
	   <?php if($tipo_usuario ==1){
		  echo'<a href="admin.php" class="btn btn-xs btn-primary header-btn"><i class="glyphicon glyphicon-cog"></i>
			  Admin</a>';
	   }
	   else {
		  echo'<button type="button" data-toggle="modal" data-target="#modal_up_user" class="btn btn-xs btn-primary header-btn"><i class="glyphicon glyphicon-user"></i>
			  Perfil</button>';
	   }?>
	</div>
	<div class="col-md-1 col-md-offset-1">
		 <a href="logout.php?id_user=<?php echo $id_usuario;?>" class="btn btn-xs btn-primary logout header-btn"><i class="glyphicon glyphicon-log-out"></i>
			  Cerrar Sesión</a>
			  </div>
              </div>

		<div class="row barra">


					<div class="col-md-3 text-center barra-right"></br>
		<form role="search">
        <div class="input-group">
		 <label for="search_main" class="search-text">Búsqueda General en Clientes...
      <input type="search" size="35" results=5 autosave=a_unique_value class="form-control" name="s" id="search_main">
    </label>

	<span class="input-group-btn">
							<a href="brm.php" type="reset" class="btn btn-primary">
								<span class="glyphicon glyphicon-refresh">
								</span>
							</a>

	</div><!-- /input-group -->
           </form>
		   </br>
					</div>

					<div class="col-md-5 text-center">
					<div class="tareas">
					<?php if(isset($cliente)){
					 while($d_cli = mysqli_fetch_array($sql6)) { ?>
					<button class="btn btn-primary btn-xs icono" aria-label="Left Align">
                    <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                    </button> <?php echo $d_cli['nombre'];?></br>

					<button type="button" class="btn btn-xs btn-primary icono" aria-label="Left Align">
				  <i class="glyphicon glyphicon-list-alt"></i>
                     </button> <?php echo $d_cli['razon'];?>

					<?php } }?>

	                </div>
					</div>

					<div class="col-md-4 text-center barra-left"></br>
					<!-- Trigger the modal with a button -->
					<button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#myModal">
					<i class="glyphicon glyphicon-plus"></i></br> Cliente</button>
					<button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal_msj">
					<i class="glyphicon glyphicon-plus"></i></br> Mensaje</button>
					<button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal_record">
					<i class="glyphicon glyphicon-plus"></i></br> Recordatorio</button>
					<?php if(isset($cliente) || $tipo_usuario==2){}
					else { ?>
					<button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal_param">
					<i class="glyphicon glyphicon-plus"></i></br>Indicador</button>
					<button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal_compl">
					<i class="glyphicon glyphicon-plus"></i></br>Control</button>
					<?php }?>
                     </br></br>
	                </div>
                      </div>
		</br>

<div class="container-fluid container-columns">

<div class="row">

	<!-- PRIMERA COLUMNA -->
    <div class="col-xs-2 c-cliente">
	<h4>Herramientas</h4>

     <?php 	if(isset($cliente)){ ?>

	 <h4 <?php if (isset($carpeta) && ($carpeta == "param")){echo 'class="resaltador"';}	?>><a class="carpeta" href="brm.php?cl=<?php echo $cliente;?>&carp=param"> <i class="glyphicon glyphicon-signal"></i> Indicadores</a></h4>
	 <h4 <?php if (isset($carpeta) && ($carpeta == "compl")){echo 'class="resaltador"';}	?>><a class="carpeta" href="brm.php?cl=<?php echo $cliente;?>&carp=compl"> <i class="glyphicon glyphicon-asterisk"></i> Controles</a></h4>


	 <?php } 	 else { ?>
	 <h4 <?php if (isset($carpeta) && ($carpeta == "param")){echo 'class="resaltador"';}	?>><a class="carpeta" href="brm.php?carp=param"> <i class="glyphicon glyphicon-signal"></i> Indicadores</a></h4>
     <h4 <?php if (isset($carpeta) && ($carpeta == "compl")){echo 'class="resaltador"';}	?>><a class="carpeta" href="brm.php?carp=compl"> <i class="glyphicon glyphicon-asterisk"></i> Controles</a></h4>
	 <h4 <?php if (isset($carpeta) && ($carpeta == "ale")){echo 'class="resaltador"';}	?>><a class="carpeta" href="brm.php?carp=ale"> <i class="glyphicon glyphicon-exclamation-sign"></i> Alertas</a></h4>
	<?php  }

	 if (isset($cliente)){ ?>
     </br>
      <h4>Carpetas de cliente</h4>

         <h4 <?php if (isset($carpeta) && ($carpeta == "cli")){echo 'class="resaltador"';}	?>><a class="carpeta" href="brm.php?cl=<?php echo $cliente;?>&carp=cli&file=datos"><i class="glyphicon glyphicon-user"></i> Cliente</a></h4>
		 <h4 <?php if (isset($carpeta) && ($carpeta == "prot")){echo 'class="resaltador"';}	?>><a class="carpeta" href="brm.php?cl=<?php echo $cliente;?>&carp=prot&file=trata"> <i class="glyphicon glyphicon-th-list"></i> Protocolo</a></h4>
		 <h4 <?php if (isset($carpeta) && ($carpeta == "msj")){echo 'class="resaltador"';}	?>><a class="carpeta" href="brm.php?cl=<?php echo $cliente;?>&carp=msj"> <i class="glyphicon glyphicon-envelope"></i> Mensajes</a></h4>
		 <h4 <?php if (isset($carpeta) && ($carpeta == "recor")){echo 'class="resaltador"';}	?>><a class="carpeta" href="brm.php?cl=<?php echo $cliente;?>&carp=recor"> <i class="glyphicon glyphicon-calendar"></i> Recordatorios</a></h4>



  <?php } ?>

			</div>

    <!--FIN DE PRIMERA COLUMNA -->

	<!-- SEGUNDA COLUMNA-->
					  <div class="col-xs-2 c-carp"></br>
				 <form role="search" method="get" action="brm.php">
        <div class="input-group">
		<?php if (isset($carpeta) && ($carpeta != "cli" & $carpeta != "prot" & $carpeta != "param" & $carpeta != "compl")	){?>
   <label for="search_carpeta" class="search-text-carpeta"> Buscar en carpeta...
   <input type="search" size="38" results=5 autosave=a_unique_value class="form-control" name="s2" id="search_carpeta" /></label>
	<input type="hidden" value="<?php echo $carpeta;?>" name="carp">
	<?php 	if (isset($cliente)){?>
	<input type="hidden" value="<?php echo $cliente;?>" name="cl">
		<?php } }  else { ?>
		 <label for="search_carpeta" class="search-text-carpeta"> Buscar en carpeta...
		 <input type="search" disabled size="38" results=5 autosave=a_unique_value class="form-control" name="s2" id="search_carpeta"/></label>
		<?php }?>
    </div>
           </form></br>
		   <table class="table">
		<?php
		if (isset($carpeta)){

		//Lista de indicadores
		if($carpeta == "param"){
         if (mysqli_num_rows($sqlparam)!= ""){
        while($param=mysqli_fetch_array($sqlparam)){

		if(isset($cliente)){?>

	    <tr id="param_<?php echo $param['id_parametro'];?>"><td <?php if (isset($_GET['file']) && ($_GET['file'] == $param['id_parametro'])){echo 'class="resaltadors"';}	?>><a href="brm.php?cl=<?php echo $cliente;?>&carp=param&file=<?php echo $param['id_parametro'];?>#param_<?php echo $param['id_parametro'];?>" class="carpeta"><h5><?php echo $param['nombre_parametro'];?></h5></a></td></tr>
		<?php } else { ?>
		<tr id="param_<?php echo $param['id_parametro'];?>"><td <?php if (isset($_GET['file']) && ($_GET['file'] == $param['id_parametro'])){echo 'class="resaltadors"';}	?>><a href="brm.php?carp=param&file=<?php echo $param['id_parametro'];?>#param_<?php echo $param['id_parametro'];?>" class="carpeta"><h5><?php echo $param['nombre_parametro'];?></h5></a></td></tr>
		<?php }

		}

		}
        else if (mysqli_num_rows($sqlparam)== "") {echo '<br><p class="text-center">Esta carpeta no tiene ningún contenido</p>'; }
		}

		//Lista de controles
		if($carpeta == "compl"){
         if (mysqli_num_rows($sqlcompl)!= ""){
        while($compl=mysqli_fetch_array($sqlcompl)){

		if(isset($cliente)){?>

	    <tr id="com_<?php echo $compl["id_complemento"]?>"><td <?php if (isset($_GET['file']) && ($_GET['file'] == $compl['id_complemento'])){echo 'class="resaltadors"';}	?>><a href="brm.php?cl=<?php echo $cliente;?>&carp=compl&file=<?php echo $compl['id_complemento'];?>#com_<?php echo $compl["id_complemento"];?>" class="carpeta"><h5><?php echo $compl['nombre_complemento'];?></h5></a></td></tr>
		<?php } else { ?>
		<tr id="com_<?php echo $compl["id_complemento"]?>"><td <?php if (isset($_GET['file']) && ($_GET['file'] == $compl['id_complemento'])){echo 'class="resaltadors"';}	?>><a href="brm.php?carp=compl&file=<?php echo $compl['id_complemento'];?>#com_<?php echo $compl["id_complemento"];?>" class="carpeta"><h5><?php echo $compl['nombre_complemento'];?></h5></a></td></tr>
		<?php }

		}

		}
        else if (mysqli_num_rows($sqlcompl)== "") {echo '<br><p class="text-center">Esta carpeta no tiene ningún contenido</p>'; }
		}

		//lista de alertas
		if($carpeta == "ale"){
		 if (mysqli_num_rows($sqlale1)!= ""){
        while($alerta1=mysqli_fetch_array($sqlale1)){

		$c_alerta =$alerta1['id_cliente'];
		$sqlale1n = mysqli_query($conn,"select nombre from cliente where id_cliente='$c_alerta'");
		$rqlale1n = mysqli_fetch_assoc($sqlale1n);
		$cliente_alerta = $rqlale1n['nombre'];


		if(isset($cliente)){

		?>
	    <tr class="bs-callout" id="alerta_<?php echo $alerta1['id_mensaje'];?>">
		<td <?php if (isset($file) && ($file == $alerta1['id_mensaje']))
		{echo 'class="resaltadors"';}	?>>
		<a href="brm.php?cl=<?php echo $cliente;?>&carp=ale&file=<?php echo $alerta1['id_mensaje'];?>#alerta_<?php echo $alerta1['id_mensaje'];?>" class="carpeta">
		<?php if ($alerta1['mensaje'] == "mensaje"){ ?><span>Mensaje</span><?php }?>
		<?php if ($alerta1['mensaje'] == "recordatorio"){ ?><span>Recordatorio</span><?php }?>
		 </span><?php echo ' &nbsp;'.date_format(date_create($alerta1['fecha']),'d/m/Y').' ';?></span>
		 <?php if($alerta1['hora_exp'] != '00:00:00' & $alerta1['hora_exp'] != 'si' & $alerta1['hora_exp'] != 'Si' & $alerta1['hora_exp'] != 'no' & $alerta1['hora_exp'] != 'No' ){ ?>
		 </span><?php echo '  &nbsp;&nbsp;'.date('H:i', strtotime($alerta1['hora_exp']));?></span><br>
			 <?php } else { echo '<br>';} ?>
		<span><?php echo $cliente_alerta; ?></span><br>
		 <span><?php echo $alerta1['asunto']; ?> </span><br>


		</a></td></tr>

		<?php  }

		 else { ?>
		<tr class="bs-callout" id="alerta_<?php echo $alerta1['id_mensaje'];?>">
		<td <?php if (isset($file) && ($file == $alerta1['id_mensaje']))
		{echo 'class="resaltadors"';}	?>>
		<a href="brm.php?carp=ale&file=<?php echo $alerta1['id_mensaje'];?>#alerta_<?php echo $alerta1['id_mensaje'];?>" class="carpeta">
		<?php if ($alerta1['mensaje'] == "mensaje"){ ?><span>Mensaje</span> <?php }?>
		<?php if ($alerta1['mensaje'] == "recordatorio"){ ?><span>Recordatorio</span> <?php }?>
		 </span><?php echo ' &nbsp;'.date_format(date_create($alerta1['fecha']),'d/m/Y').' ';?></span>
		 <?php if($alerta1['hora_exp'] != '00:00:00' & $alerta1['hora_exp'] != 'si' & $alerta1['hora_exp'] != 'Si' & $alerta1['hora_exp'] != 'no' & $alerta1['hora_exp'] != 'No' ){ ?>
		 </span><?php echo '  &nbsp;&nbsp;'.date('H:i', strtotime($alerta1['hora_exp']));?></span><br>
		 <?php } else { echo '<br>';}?>
		 <span><?php echo $cliente_alerta; ?></span><br>
		 <span><?php echo $alerta1['asunto']; ?> </span><br>


		</a></td></tr>

		 <?php }

		  } }

		}



        if($carpeta == "cli"){
		  if (mysqli_num_rows($sql7)!= ""){
        while($cli7=mysqli_fetch_array($sql7)){ ?>
	<tr><td <?php if (isset($file) && $file == "datos"){echo 'class="resaltadors"';}	?>><a class="carpeta" href="brm.php?cl=<?php echo $cliente;?>&carp=cli&file=datos"><h5>Datos Generales</h5></a></td></tr>
	<tr><td <?php if (isset($file) && $file == "local"){echo 'class="resaltadors"';}	?>><a class="carpeta" href="brm.php?cl=<?php echo $cliente;?>&carp=cli&file=local"><h5>Localización</h5></a></td></tr>
	<!--<tr><td><a class="carpeta" disabled href="brm.php?cl=<?php echo $cliente;?>&carp=cli&file=general"><h5>Características de Cliente</h5></a></td></tr>-->
    <tr><td <?php if (isset($file) && $file == "docs"){echo 'class="resaltadors"';}	?>><a class="carpeta" href="brm.php?cl=<?php echo $cliente;?>&carp=cli&file=docs"><h5>Documentos</h5></a></td></tr>

	<?php }
		  }
		 else {echo '<br><p class="text-center">Esta carpeta no tiene ningún contenido</p>'; }
		}

		if($carpeta == "msj"){
			if (mysqli_num_rows($sql9)!= ""){
			while($msj9=mysqli_fetch_array($sql9)){
			if($msj9['status']=='no'){$clase='bs-callout';}
			if($msj9['status']=='si'){$clase='bs';}?>
			<tr class="<?php echo $clase;?>"><td <?php if (isset($file) && ($file == $msj9['id_mensaje'])){echo 'class="resaltadors"';}	?>><a href="brm.php?cl=<?php echo $cliente;?>&carp=msj&file=<?php echo $msj9['id_mensaje'];?>" class="carpeta"><h5><?php echo $msj9['asunto'];?></h5></a></td></tr>
		<?php } }
		 else {echo '<br><p class="text-center">Esta carpeta no tiene ningún contenido</p>'; }
		}

			if( $carpeta == "recor"){
          if (mysqli_num_rows($sql10)!= ""){
        while($recor10=mysqli_fetch_array($sql10)){
        if($recor10['status']=='no'){$clase='bs-callout';}
		if($recor10['status']=='si'){$clase='bs';}

		if($recor10['asunto']== "Por paquete"){
		?>
	    <tr class="<?php echo $clase;?>"><td <?php if (isset($file) && ($file == $recor10['id_recordatorio'])){echo 'class="resaltadors"';}	?>><a href="brm.php?cl=<?php echo $cliente;?>&carp=recor&file=<?php echo $recor10['id_recordatorio'];?>" class="carpeta"><h5><?php echo 'Recordatorio '.$recor10['id_recordatorio'].'<br>'.date_format(date_create($recor10['fecha']),'d/m/Y'); if($recor10['hora_exp'] != "00:00:00") { echo ' &nbsp;&nbsp;'.date('H:i', strtotime($recor10['hora_exp']));}?></h5></a></td></tr>
		<?php }
         if($recor10['asunto'] != "Por paquete"){
		?>
	    <tr class="<?php echo $clase;?>"><td <?php if (isset($file) && ($file == $recor10['id_recordatorio'])){echo 'class="resaltadors"';}	?>><a href="brm.php?cl=<?php echo $cliente;?>&carp=recor&file=<?php echo $recor10['id_recordatorio'];?>" class="carpeta"><h5><?php echo $recor10['asunto'].'<br>'.date_format(date_create($recor10['fecha']),'d/m/Y'); if($recor10['hora_exp'] != "00:00:00") { echo ' &nbsp;&nbsp;'.date('H:i', strtotime($recor10['hora_exp']));}?></h5></a></td></tr>
		<?php }

		} }

			 else {echo '<br><p class="text-center">Esta carpeta no tiene ningún contenido</p>'; }
			}


			if($carpeta == "prot"){
        if (mysqli_num_rows($sql12)!= ""){
        while($prot12=mysqli_fetch_array($sql12)){ ?>
	    <tr><td <?php if (isset($file) && $file == "trata" ){echo 'class="resaltadors"';}	?>><a href="brm.php?cl=<?php echo $cliente;?>&carp=prot&file=trata" class="carpeta"><h5>Tratamiento</h5></a></td></tr>
		<tr><td <?php if (isset($file) && $file == "slogan"){echo 'class="resaltadors"';}	?>><a href="brm.php?cl=<?php echo $cliente;?>&carp=prot&file=slogan" class="carpeta"><h5>Atributos</h5></a></td></tr>
		<tr><td <?php if (isset($file) && $file == "contes"){echo 'class="resaltadors"';}	?>><a href="brm.php?cl=<?php echo $cliente;?>&carp=prot&file=contes" class="carpeta"><h5>Contestación Telefónica</h5></a></td></tr>
		<tr><td <?php if (isset($file) && $file == "cola"){echo 'class="resaltadors"';}	?>><a href="brm.php?cl=<?php echo $cliente;?>&carp=prot&file=cola" class="carpeta"><h5>Colaboradores</h5></a></td></tr>
		<tr><td <?php if (isset($file) && $file == "pic"){echo 'class="resaltadors"';}	?>><a href="brm.php?cl=<?php echo $cliente;?>&carp=prot&file=pic" class="carpeta"><h5>Mas información</h5></a></td></tr>
		<?php }
		}
        else  { ?>

		<tr><td <?php if (isset($file) && $file == "trata" ){echo 'class="resaltadors"';}	?>><a href="brm.php?cl=<?php echo $cliente;?>&carp=prot&file=trata" class="carpeta"><h5>Tratamiento Cliente</h5></a></td></tr>
		<tr><td <?php if (isset($file) && $file == "slogan"){echo 'class="resaltadors"';}	?>><a href="brm.php?cl=<?php echo $cliente;?>&carp=prot&file=slogan" class="carpeta"><h5>Atributos</h5></a></td></tr>
		<tr><td <?php if (isset($file) && $file == "contes"){echo 'class="resaltadors"';}	?>><a href="brm.php?cl=<?php echo $cliente;?>&carp=prot&file=contes" class="carpeta"><h5>Contestación Telefónica</h5></a></td></tr>
		<tr><td <?php if (isset($file) && $file == "cola"){echo 'class="resaltadors"';}	?>><a href="brm.php?cl=<?php echo $cliente;?>&carp=prot&file=cola" class="carpeta"><h5>Colaboradores</h5></a></td></tr>
		<tr><td <?php if (isset($file) && $file == "pic"){echo 'class="resaltadors"';}	?>><a href="brm.php?cl=<?php echo $cliente;?>&carp=prot&file=pic" class="carpeta"><h5>Mas información</h5></a></td></tr>
	<?php	}

		}

		}
		?>



       </table>
				</br>
					  </div>

					  <!-- TERCERA COLUMNA-->
					  <div class="col-xs-8 c-hall">

				     <?php
					 //HALLAZGOS PRIMERA BÚSQUEDA
					 if(isset($search)){
				      echo '<table class="table table-hover table-search">';

					  //opciones para exportar clientes

					  echo '<tr><td><form action="exportar.php" method="post">
					  <select name="exporta_cliente">
						<option value="0">TODOS</option>';
							$sqlsg2 = mysqli_query($conn,"select * FROM grupos WHERE id_empresa='$id_empresa' AND nombre_grupo!='' ORDER BY id ASC");
								while($sqg2 = mysqli_fetch_array($sqlsg2)){
									echo "<option value=".$sqg2['id'].">".$sqg2['nombre_grupo']."</option>";
							}
					  echo '</select>
					  <input type="hidden" name="empresa_clientes" value="'.$empresa_usuario.'"/>
					  <input type="hidden" name="id_empresa_clientes" value="'.$id_empresa.'"/>
					  <input type="hidden" name="modificado_por" value="'.$nombre.'"/>

					  <button type="submit" class="btn btn-success btn-xs" name="exportacli">
					  <i class="glyphicon glyphicon-export"></i>
						Exportar clientes
					  </button>
					  </form>
					  </td></tr>';

					 if (mysqli_num_rows($sql1)!= ""){
						 echo '<tr><td>	<button type="button" class="btn btn-xs btn-primary icono" aria-label="Left Align">
						<i class="glyphicon glyphicon-user"></i>
                     </button> <b>Clientes</b></td></tr>' ;

					 while($r_cli = mysqli_fetch_array($sql1)) {

                        echo '<tr><td><a class="hallazgo" href="brm.php?cl='.$r_cli['id_cliente'].'&carp=cli&file=datos">'.$r_cli['nombre'].' - ';
						if($r_cli['estatus']== "si" or $r_cli['estatus']== "Si"){
							echo '<u>Activo</u></a></td></tr>';
						}

						if($r_cli['estatus']== "no" or $r_cli['estatus']== "No"){
							echo '<u>Inactivo</u></a></td></tr>';
						}else{
								$busqueda_estatus = $r_cli['estatus'];
								$rquerybus_estatus = mysqli_query($conn, "SELECT * FROM estatus WHERE id = '$busqueda_estatus'");
								while($bus_estatus = mysqli_fetch_array($rquerybus_estatus)){
									echo '<u>'.$bus_estatus['nombre_estatus'].'</u></a></td></tr>';
								}
						}
					 }
					 }
						 if (mysqli_num_rows($sql1a)!= ""){
						 echo '<tr><td>	<button type="button" class="btn btn-xs btn-primary icono" aria-label="Left Align">
				  <i class="glyphicon glyphicon-user"></i>
                     </button> <b>Razon Social</b></td></tr>' ;

					  while($r_cli2 = mysqli_fetch_array($sql1a)) {

						echo '<tr><td><a class="hallazgo" href="brm.php?cl='.$r_cli2['id_cliente'].'&carp=cli&file=datos">'.$r_cli2['razon'].' - <u>'.$r_cli2['nombre'].'</u></a></td></tr>';
						}

						 }
                     if (mysqli_num_rows($sql1b)!= ""){
						 echo '<tr><td>	<button type="button" class="btn btn-xs btn-primary icono" aria-label="Left Align">
				  <i class="glyphicon glyphicon-user"></i>
                     </button> <b>Número RFC</b></td></tr>' ;

					  while($r_cli3 = mysqli_fetch_array($sql1b)) {

						echo '<tr><td><a class="hallazgo" href="brm.php?cl='.$r_cli3['id_cliente'].'&carp=cli&file=datos">'.$r_cli3['rfc'].' - <u>'.$r_cli3['nombre'].'</u></a></td></tr>';
						}

						 }


						 if (mysqli_num_rows($sqlr)!= ""){
						 echo '<tr><td>	<button type="button" class="btn btn-xs btn-primary icono" aria-label="Left Align">
				  <i class="glyphicon glyphicon-user"></i>
                     </button> <b>Oficina</b></td></tr>' ;

					  while($r_clir = mysqli_fetch_array($sqlr)) {

						echo '<tr><td><a class="hallazgo" href="brm.php?cl='.$r_clir['id_cliente'].'&carp=cli&file=datos">'.$r_clir['oficina'].' - <u>'.$r_clir['nombre'].'</u></a></td></tr>';
						}

						 }


                         if (mysqli_num_rows($sql1c)!= ""){
						 echo '<tr><td>	<button type="button" class="btn btn-xs btn-primary icono" aria-label="Left Align">
				  <i class="glyphicon glyphicon-user"></i>
                     </button> <b>Clave Telefono</b></td></tr>' ;

					  while($r_cli4 = mysqli_fetch_array($sql1c)) {

						echo '<tr><td><a class="hallazgo" href="brm.php?cl='.$r_cli4['id_cliente'].'&carp=cli&file=datos">'.$r_cli4['clave_telefono'].' - <u>'.$r_cli4['nombre'].'</u></a></td></tr>';
						}

						 }

					 if (mysqli_num_rows($sql1d)!= ""){
							 echo '<tr><td>	<button type="button" class="btn btn-xs btn-primary icono" aria-label="Left Align">
				  <i class="glyphicon glyphicon-user"></i>
						</button> <b>Personas Contacto</b></td></tr>' ;
					    while($r_cli5 = mysqli_fetch_array($sql1d)) {
						$id_cli5 = $r_cli5['id_cliente'];
						$sql1d1 = mysqli_query($conn,"select nombre from cliente where id_cliente='$id_cli5'");
						$rsqld1 = mysqli_fetch_assoc($sql1d1);
						$cli5 = $rsqld1['nombre'];
						echo '<tr><td><a class="hallazgo" href="brm.php?cl='.$r_cli5['id_cliente'].'&carp=cli&file=local">'.$r_cli5['contactos'].' - <u>'.$cli5.'</a></td></tr>';

						}
					 }

					  if (mysqli_num_rows($sql1e)!= ""){
							 echo '<tr><td>	<button type="button" class="btn btn-xs btn-primary icono" aria-label="Left Align">
				  <i class="glyphicon glyphicon-user"></i>
						</button> <b>Calle</b></td></tr>' ;
					    while($r_cli6 = mysqli_fetch_array($sql1e)) {
						$id_cli6 = $r_cli6['id_cliente'];
						$sql1e1 = mysqli_query($conn,"select nombre from cliente where id_cliente='$id_cli6'");
						$rsql1e1 = mysqli_fetch_assoc($sql1e1);
						$cli6 = $rsql1e1['nombre'];
						echo '<tr><td><a class="hallazgo" href="brm.php?cl='.$r_cli6['id_cliente'].'&carp=cli&file=local">'.$r_cli6['calle'].' - <u>'.$cli6.'</a></td></tr>';

						}
					 }

					 if (mysqli_num_rows($sql1f)!= ""){
							 echo '<tr><td>	<button type="button" class="btn btn-xs btn-primary icono" aria-label="Left Align">
				  <i class="glyphicon glyphicon-user"></i>
						</button> <b>Número Interno</b></td></tr>' ;
					    while($r_cli7 = mysqli_fetch_array($sql1f)) {
						$id_cli7 = $r_cli7['id_cliente'];
						$sql1f1 = mysqli_query($conn,"select nombre from cliente where id_cliente='$id_cli7'");
						$rsql1f1 = mysqli_fetch_assoc($sql1f1);
						$cli7 = $rsql1f1['nombre'];
						echo '<tr><td><a class="hallazgo" href="brm.php?cl='.$r_cli7['id_cliente'].'&carp=cli&file=local">'.$r_cli7['interno'].' - <u>'.$cli7.'</a></td></tr>';

						}
					 }

					 if (mysqli_num_rows($sql1g)!= ""){
							 echo '<tr><td>	<button type="button" class="btn btn-xs btn-primary icono" aria-label="Left Align">
				  <i class="glyphicon glyphicon-user"></i>
						</button> <b>Número Externo</b></td></tr>' ;
					    while($r_cli8 = mysqli_fetch_array($sql1g)) {
						$id_cli8 = $r_cli8['id_cliente'];
						$sql1g1 = mysqli_query($conn,"select nombre from cliente where id_cliente='$id_cli8'");
						$rsql1g1 = mysqli_fetch_assoc($sql1g1);
						$cli8 = $rsql1g1['nombre'];
						echo '<tr><td><a class="hallazgo" href="brm.php?cl='.$r_cli8['id_cliente'].'&carp=cli&file=local">'.$r_cli8['externo'].' - <u>'.$cli8.'</a></td></tr>';

						}
					 }

					 if (mysqli_num_rows($sql1h)!= ""){
							 echo '<tr><td>	<button type="button" class="btn btn-xs btn-primary icono" aria-label="Left Align">
				  <i class="glyphicon glyphicon-user"></i>
						</button> <b>Colonia</b></td></tr>' ;
					    while($r_cli9 = mysqli_fetch_array($sql1h)) {
						$id_cli9 = $r_cli9['id_cliente'];
						$sql1h1 = mysqli_query($conn,"select nombre from cliente where id_cliente='$id_cli9'");
						$rsql1h1 = mysqli_fetch_assoc($sql1h1);
						$cli9 = $rsql1h1['nombre'];
						echo '<tr><td><a class="hallazgo" href="brm.php?cl='.$r_cli9['id_cliente'].'&carp=cli&file=local">'.$r_cli9['colonia'].' - <u>'.$cli9.'</a></td></tr>';

						}
					 }
					 if (mysqli_num_rows($sql1i)!= ""){
							 echo '<tr><td>	<button type="button" class="btn btn-xs btn-primary icono" aria-label="Left Align">
				  <i class="glyphicon glyphicon-user"></i>
						</button> <b>Municipio</b></td></tr>' ;
					    while($r_cli10 = mysqli_fetch_array($sql1i)) {
						$id_cli10 = $r_cli10['id_cliente'];
						$sql1i1 = mysqli_query($conn,"select nombre from cliente where id_cliente='$id_cli10'");
						$rsql1i1 = mysqli_fetch_assoc($sql1i1);
						$cli10 = $rsql1i1['nombre'];
						echo '<tr><td><a class="hallazgo" href="brm.php?cl='.$r_cli10['id_cliente'].'&carp=cli&file=local">'.$r_cli10['municipio'].' - <u>'.$cli10.'</a></td></tr>';

						}
					 }

					  if (mysqli_num_rows($sql1j)!= ""){
							 echo '<tr><td>	<button type="button" class="btn btn-xs btn-primary icono" aria-label="Left Align">
				  <i class="glyphicon glyphicon-user"></i>
						</button> <b>Estado</b></td></tr>' ;
					    while($r_cli11 = mysqli_fetch_array($sql1j)) {
						$id_cli11 = $r_cli11['id_cliente'];
						$sql1j1 = mysqli_query($conn,"select nombre from cliente where id_cliente='$id_cli11'");
						$rsql1j1 = mysqli_fetch_assoc($sql1j1);
						$cli11 = $rsql1j1['nombre'];
						echo '<tr><td><a class="hallazgo" href="brm.php?cl='.$r_cli11['id_cliente'].'&carp=cli&file=local">'.$r_cli11['estado'].' - <u>'.$cli11.'</a></td></tr>';

						}
					 }

					 if (mysqli_num_rows($sql1k)!= ""){
							 echo '<tr><td>	<button type="button" class="btn btn-xs btn-primary icono" aria-label="Left Align">
				  <i class="glyphicon glyphicon-user"></i>
						</button> <b>Localidad</b></td></tr>' ;
					    while($r_cli12 = mysqli_fetch_array($sql1k)) {
						$id_cli12 = $r_cli12['id_cliente'];
						$sql1k1 = mysqli_query($conn,"select nombre from cliente where id_cliente='$id_cli12'");
						$rsql1k1 = mysqli_fetch_assoc($sql1k1);
						$cli12 = $rsql1k1['nombre'];
						echo '<tr><td><a class="hallazgo" href="brm.php?cl='.$r_cli12['id_cliente'].'&carp=cli&file=local">'.$r_cli12['localidad'].' - <u>'.$cli12.'</a></td></tr>';

						}
					 }

					  if (mysqli_num_rows($sql1l)!= ""){
							 echo '<tr><td>	<button type="button" class="btn btn-xs btn-primary icono" aria-label="Left Align">
				  <i class="glyphicon glyphicon-user"></i>
						</button> <b>Postal</b></td></tr>' ;
					    while($r_cli13 = mysqli_fetch_array($sql1l)) {
						$id_cli13 = $r_cli13['id_cliente'];
						$sql1l1 = mysqli_query($conn,"select nombre from cliente where id_cliente='$id_cli13'");
						$rsql1l1 = mysqli_fetch_assoc($sql1l1);
						$cli13 = $rsql1l1['nombre'];
						echo '<tr><td><a class="hallazgo" href="brm.php?cl='.$r_cli13['id_cliente'].'&carp=cli&file=local">'.$r_cli13['postal'].' - <u>'.$cli13.'</a></td></tr>';

						}
					 }

					if (mysqli_num_rows($sql1m)!= ""){
							 echo '<tr><td>	<button type="button" class="btn btn-xs btn-primary icono" aria-label="Left Align">
				  <i class="glyphicon glyphicon-user"></i>
						</button> <b>Teléfono</b></td></tr>' ;
					    while($r_cli14 = mysqli_fetch_array($sql1m)) {
						$id_cli14 = $r_cli14['id_cliente'];
						$sql1m1 = mysqli_query($conn,"select nombre from cliente where id_cliente='$id_cli14'");
						$rsql1m1 = mysqli_fetch_assoc($sql1m1);
						$cli14 = $rsql1m1['nombre'];
						echo '<tr><td><a class="hallazgo" href="brm.php?cl='.$r_cli14['id_cliente'].'&carp=cli&file=datos">'.$r_cli14['telefono'].' - <u>'.$cli14.'</a></td></tr>';

						}
					 }

                     if (mysqli_num_rows($sql1n)!= ""){
							 echo '<tr><td>	<button type="button" class="btn btn-xs btn-primary icono" aria-label="Left Align">
				  <i class="glyphicon glyphicon-user"></i>
						</button> <b>Email</b></td></tr>' ;
					    while($r_cli15 = mysqli_fetch_array($sql1n)) {
						$id_cli15 = $r_cli15['id_cliente'];
						$sql1n1 = mysqli_query($conn,"select nombre from cliente where id_cliente='$id_cli15'");
						$rsql1n1 = mysqli_fetch_assoc($sql1n1);
						$cli15 = $rsql1n1['nombre'];
						echo '<tr><td><a class="hallazgo" href="brm.php?cl='.$r_cli15['id_cliente'].'&carp=cli&file=local">'.$r_cli15['email'].' - <u>'.$cli15.'</a></td></tr>';

						}
					 }

					 if (mysqli_num_rows($sql2)!= ""){
						 echo '<tr><td>	<button type="button" class="btn btn-xs btn-primary icono" aria-label="Left Align">
				  <i class="glyphicon glyphicon-th-list"></i>
                     </button> <b>Tratamiento al cliente</b></td></tr>' ;

					 while($r_pro = mysqli_fetch_array($sql2)) {
						$id_cliente_pro =  $r_pro['id_cliente'];
						$r_pro1 = mysqli_query($conn,"select nombre from cliente where id_cliente = '$id_cliente_pro'");
                        $rr_pro1 = mysqli_fetch_assoc($r_pro1);
						$n_pro = $rr_pro1['nombre'];
                        echo '<tr><td><a class="hallazgo" href="brm.php?cl='.$r_pro['id_cliente'].'&carp=prot&file=trata">'.$r_pro['descripcion'].' - <u>'.$n_pro.'</a></td></tr>';

					 }}
					if (mysqli_num_rows($sql2a)!= ""){
						 echo '<tr><td>	<button type="button" class="btn btn-xs btn-primary icono" aria-label="Left Align">
				  <i class="glyphicon glyphicon-th-list"></i>
                     </button> <b>Colaboradores</b></td></tr>' ;

					 while($r_col = mysqli_fetch_array($sql2a)) {
						$id_cliente_col =  $r_col['id_cliente'];
						$r_col1 = mysqli_query($conn,"select nombre from cliente where id_cliente = '$id_cliente_col'");
                        $rr_col1 = mysqli_fetch_assoc($r_col1);
						$n_col = $rr_col1['nombre'];
						$col2 = $r_col['nombre'];

                        	 echo '<tr><td><a class="hallazgo" href="brm.php?cl='.$r_col['id_cliente'].'&carp=prot&file=cola">'.$col2. ' - <u>'.$n_col.'</a></td></tr>';

					 }}

					  if (mysqli_num_rows($sql2b)!= ""){
						 echo '<tr><td>	<button type="button" class="btn btn-xs btn-primary icono" aria-label="Left Align">
				  <i class="glyphicon glyphicon-th-list"></i>
                     </button> <b>Atributos</b></td></tr>' ;

					 while($r_atr = mysqli_fetch_array($sql2b)) {
						$id_cliente_atr =  $r_atr['id_cliente'];
						$r_atr1 = mysqli_query($conn, "select nombre from cliente where id_cliente = '$id_cliente_atr'");
                        $rr_atr1 = mysqli_fetch_assoc($r_atr1);
						$n_atr = $rr_atr1['nombre'];
                        echo '<tr><td><a class="hallazgo" href="brm.php?cl='.$r_atr['id_cliente'].'&carp=prot&file=slogan">'.$r_atr['slogan'].' - <u>'.$n_atr.'</a></td></tr>';

					 }}

					  if (mysqli_num_rows($sql2c)!= ""){
						 echo '<tr><td>	<button type="button" class="btn btn-xs btn-primary icono" aria-label="Left Align">
				  <i class="glyphicon glyphicon-th-list"></i>
                     </button> <b>Contestación Telefónica</b></td></tr>' ;

					 while($r_contes = mysqli_fetch_array($sql2c)) {
						$id_cliente_contes =  $r_contes['id_cliente'];
						$r_contes1 = mysqli_query($conn,"select nombre from cliente where id_cliente = '$id_cliente_contes'");
                        $rr_contes1 = mysqli_fetch_assoc($r_contes1);
						$n_contes = $rr_contes1['nombre'];
                        echo '<tr><td><a class="hallazgo" href="brm.php?cl='.$r_contes['id_cliente'].'&carp=prot&file=contes">'.$r_contes['contestacion'].' - <u>'.$n_contes.'</a></td></tr>';

					 }}

					 if (mysqli_num_rows($sql3) != ""){
						 echo '<tr><td>	<button type="button" class="btn btn-xs btn-primary icono" aria-label="Left Align">
				  <i class="glyphicon glyphicon-envelope"></i>
                     </button> <b>Mensajes</b></td></tr>' ;

					 while($r_msj = mysqli_fetch_array($sql3)) {

						$id_cliente_msj =  $r_msj['id_cliente'];
						$r_msj1 = mysqli_query($conn,"select nombre from cliente where id_cliente = '$id_cliente_msj'");
                        $rr_msj1 = mysqli_fetch_assoc($r_msj1);
						$n_msj = $rr_msj1['nombre'];
                        echo '<tr><td><a class="hallazgo" href="brm.php?cl='.$r_msj['id_cliente'].'&carp=msj&file='.$r_msj['id_mensaje'].'">'.$r_msj['asunto'].' - <u>'.$n_msj.'</a></td></tr>';

					 }}

					 if (mysqli_num_rows($sql4) != ""){
						 echo '<tr><td>	<button type="button" class="btn btn-xs btn-primary icono" aria-label="Left Align">
				  <i class="glyphicon glyphicon-calendar"></i>
                     </button> <b>Recordatorios</b></td></tr>' ;

					 while($r_rec = mysqli_fetch_array($sql4)) {
						 $id_cliente_rec =  $r_rec['id_cliente'];
						$r_rec1 = mysqli_query($conn,"select nombre from cliente where id_cliente = '$id_cliente_rec'");
                        $rr_rec1 = mysqli_fetch_assoc($r_rec1);
						$n_rec = $rr_rec1['nombre'];
                        echo '<tr><td><a class="hallazgo" href="brm.php?cl='.$r_rec['id_cliente'].'&carp=recor&file='.$r_rec['id_recordatorio'].'">Recordatorio '.$r_rec['id_recordatorio'].' - <u>'.$n_rec.'</a></td></tr>';

					 }}
					  echo "</table>";
					 }
					// else {echo '</br><h4 class="text-center">La búsqueda no produjo ningun resultado</h4>';}



            //HALLAZGOS SEGUNDA BÚSQUEDA

				else if (isset($search2)){
				  echo '<table class="table table-hover table-search2">';
				  if(isset($carpeta) && $carpeta == "msj"){

					 if (mysqli_num_rows($sql26)!= ""){
						 echo '<tr><td>	<button type="button" class="btn btn-xs btn-primary icono" aria-label="Left Align">
				  <i class="glyphicon glyphicon-envelope"></i>
                     </button> <b>Mensajes</b></td></tr>' ;
					 }
					 while($cli26 = mysqli_fetch_array($sql26)) {

                        echo '<tr><td><a class="hallazgo" href="brm.php?cl='.$cli26['id_cliente'].'&carp='.$carpeta.'&file='.$cli26['id_mensaje'].'">'.$cli26['asunto'].'</a></td></tr>';


					 }
				}


				if(isset($carpeta) && $carpeta == "recor"){

					 if (mysqli_num_rows($sql27)!= ""){
						 echo '<tr><td>	<button type="button" class="btn btn-xs btn-primary icono" aria-label="Left Align">
				  <i class="glyphicon glyphicon-calendar"></i>
                     </button> <b>Mensajes</b></td></tr>' ;
					 }
					 while($cli27 = mysqli_fetch_array($sql27)) {

                        echo '<tr><td><a class="hallazgo" href="brm.php?cl='.$cli27['id_cliente'].'&carp='.$carpeta.'&file='.$cli27['id_recordatorio'].'">Recordatorio '.$cli27['id_recordatorio'].'</a></td></tr>';


					 }
				}



				if(isset($carpeta) && $carpeta == "ale"){

					 if (mysqli_num_rows($sql28)!= ""){
						 echo '<tr><td>	<button type="button" class="btn btn-xs btn-primary icono" aria-label="Left Align">
				  <i class="glyphicon glyphicon-exclamation-sign"></i>
                     </button> <b>Mensajes</b></td></tr>' ;
					 }
					 while($cli28 = mysqli_fetch_array($sql28)) {

						if(isset($cliente)){
	     echo '<tr><td><a class="hallazgo" href="brm.php?cl='.$cliente.'&carp='.$carpeta.'&file='.$cli28['id_mensaje'].'">'.$cli28['asunto'].'</a></td></tr>';
	     } else {
		 echo '<tr><td><a class="hallazgo" href="brm.php?carp='.$carpeta.'&file='.$cli28['id_mensaje'].'">'.$cli28['asunto'].'</a></td></tr>';
		} }

		if (mysqli_num_rows($sql29)!= ""){
						 echo '<tr><td>	<button type="button" class="btn btn-xs btn-primary icono" aria-label="Left Align">
				  <i class="glyphicon glyphicon-exclamation-sign"></i>
                     </button> <b>Recordatorios</b></td></tr>' ;
					 }
					 while($cli29 = mysqli_fetch_array($sql29)) {

						if(isset($cliente)){
	     echo '<tr><td><a class="hallazgo" href="brm.php?cl='.$cliente.'&carp='.$carpeta.'&file='.$cli29['id_recordatorio'].'">Recordatorio '.$cli29['id_recordatorio'].'</a></td></tr>';
	     } else {
		 echo '<tr><td><a class="hallazgo" href="brm.php?carp='.$carpeta.'&file='.$cli29['id_recordatorio'].'">Recordatorio '.$cli29['id_recordatorio'].'</a></td></tr>';

		}
		}
		}
            echo "</table>";
				}

			//INFORMACIÓN SELECCIONADA

			/*CARPETA DATOS GENERALES*/
			else if (isset($file)&& $carpeta == "cli" && $file == "datos"){
			     echo '<table class="table table-hover table-datos">';
				while($cli14=mysqli_fetch_array($sql14)){

                         echo '<tr><td><b>Grupo</b></td><td colspan="3"><b>Razón Social</b></td></tr>';;

						$dgrupo = $cli14['grupo'];


						if(is_numeric($dgrupo)){

							$rquerydgrupo = mysqli_query($conn, "SELECT * FROM grupos WHERE id = '$dgrupo'");
								while($cligrupo = mysqli_fetch_array($rquerydgrupo)){
									echo '<td>'.$cligrupo['nombre_grupo'].'</td>';
								}
						}else{
							echo '<tr><td>'.$dgrupo.'</td>';
						}

						 echo '<td colspan="3">'.$cli14['razon'].'</td></tr>';
						 echo '<tr><td><b>Status</b></td><td><b>Número RFC</b></td><td><b>Oficina</b></td></tr>';
						 echo '<tr>';

						 $dstatus = $cli14['estatus'];

						 if($dstatus == "Si" or $dstatus  == "si"){
							echo '<td>Activo</td>';}
						 if($dstatus == "No" or $dstatus == "no"){
							echo '<td>Inactivo</td>';}
						else{

							$rquerydstatus = mysqli_query($conn, "SELECT * FROM estatus WHERE id = '$dstatus'");
								while($clistatus = mysqli_fetch_array($rquerydstatus)){
									echo '<td>'.$clistatus['nombre_estatus'].'</td>';
								}
						}

						 echo '<td>'.$cli14['rfc'].'</td><td>'.$cli14['oficina'].'</td></tr>';
						 echo '<tr><td colspan="3"><b>Claves Teléfonicas</b></td></tr>';
						 echo '<td <td colspan="3">'.$cli14['clave_telefono'].'</td></tr>';
						 echo '<tr><td colspan="3" class="celda-right"><button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal_cli_up1"><i class="glyphicon glyphicon-pencil"></i> Editar</button>';
						 if($tipo_usuario != 2 ){
						 echo '&nbsp;<button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal_cli_del"> <i class="glyphicon glyphicon-remove"></i> Eliminar Cliente</button></td></tr>';
						 }
						 echo '<tr><td colspan="3"><a target="_blank" href="'.$cli14['logo'].'"><img id="logo-img" src="'.$cli14['logo'].'"/></a></td></tr>';
						 echo '<tr><td colspan="3"><form action="accion_cliente.php" enctype="multipart/form-data" method="post" class="inline-forms">
						<div class="input-group">
								<span class="input-group-btn"  data-toggle="tooltip" title="Seleccione logo/foto">
									<span class="btn btn-default btn-file">
									<span class="glyphicon glyphicon-picture"></span>
									<b>&nbsp;Logo/Foto</b>
										<input type="file" name="logo" id="logo" onchange="readURL(this);"/>
									</span>
								</span>

								<input type="text" class="form-control read_logo" readonly>
								<button type="submit" name="guarda_logo" id="guarda_logo" class="form-control guarda_logo btn-primary btn btn-xs" data-toggle="tooltip" title="Guardar"><i class="glyphicon glyphicon-floppy-disk"></i></button>
								<button type="submit" name="elimina_logo" class="form-control elimina_logo btn-info btn btn-xs" data-toggle="tooltip" title="Borrar"><i class="glyphicon glyphicon-remove"></i></button>
							</div>
						 <input type="hidden" name="id_cli" value="'.$cli14['id_cliente'].'">
						 </form></td></tr>';


			}
			 echo '</table>';
			}
					  else if (isset($file)&& $carpeta == "cli" && $file == "local"){

					  echo '<table class="table table-hover table-local">';
					  while($cli15=mysqli_fetch_array($sql15)){

						echo '<tr><td><b>Calle</b></td><td><b>Número Externo</b></td><td colspan="2"><b>Número Interno</b></td></tr>';
					    echo '<tr><td>'.$cli15['calle'].'</td>';
						echo '<td>'.$cli15['externo'].'</td><td colspan="2">'.$cli15['interno'].'</td></tr>';
					    echo '<tr><td><b>Colonia</b></td><td><b>Municipio</b></td><td><b>Localidad</b></td><td><b>Estado</b></td></tr>';
					    echo '<tr><td>'.$cli15['colonia'].'</td><td>'.$cli15['municipio'].'</td><td>'.$cli15['localidad'].'</td><td colspan="2">'.$cli15['estado'].'</td></tr>';
					    echo '<tr><td><b>Código Postal</b></td><td><b>Persona Contacto</b></td><td colspan="2"><b>Teléfono</b></td></tr>';
						echo '<tr><td>'.$cli15['postal'].'</td><td>'.$cli15['contactos'].'</td><td colspan="2">'.$cli15['telefono'].'</td></tr>';
						echo '<tr><td><b>Email</b></td><td><b>Facebook</b></td><td colspan="2"><b>Twitter</b></td></tr>';
					    echo '<tr><td>'.$cli15['email'].'</td><td>'.$cli15['facebook'].'</td><td colspan="2">'.$cli15['twitter'].'</td></tr>';
						echo '<tr><td colspan="4" class="celda-right"><button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal_cli_up1"><i class="glyphicon glyphicon-pencil"></i> Editar</button>&nbsp;';
						if($tipo_usuario != 2 ){
						echo '<button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal_cli_del"> <i class="glyphicon glyphicon-remove"></i> Eliminar Cliente</button></td></tr>';
							}
						echo '<tr><td colspan="4"><a target="_blank" href="'.$cli15['foto_contacto'].'"><img id="foto-img" src="'.$cli15['foto_contacto'].'"/></a></td></tr>';
						echo '<tr><td colspan="4"><form action="accion_cliente.php" enctype="multipart/form-data" method="post" class="inline-forms">
						<div class="input-group">
								<span class="input-group-btn"  data-toggle="tooltip" title="Seleccione foto">
									<span class="btn btn-default btn-file">
									<span class="glyphicon glyphicon-picture"></span>
									<b>&nbsp;Foto</b>
										<input type="file" name="foto_contacto" id="foto_contacto" onchange="readfURL(this);"/>
									</span>
								</span>

								<input type="text" class="form-control read_foto" readonly>
								<button type="submit" name="guarda_foto" id="guarda_foto" class="form-control guarda_logo btn-primary btn btn-xs" data-toggle="tooltip" title="Guardar"><i class="glyphicon glyphicon-floppy-disk"></i></button>
								<button type="submit" name="elimina_foto" class="form-control elimina_logo btn-info btn btn-xs" data-toggle="tooltip" title="Borrar"><i class="glyphicon glyphicon-remove"></i></button>
							</div>
						 <input type="hidden" name="id_cli" value="'.$cli15['id_cliente'].'">
						 </form></td></tr>';

					}


						echo '</table>';
					 }

					 else if (isset($file)&& $carpeta == "cli" && $file == "docs"){
					 	echo '<table class="table table-hover table-docs">';
						echo '<tr><td><b>Documentos</b></td></tr>';
					  $cd = 0;
					  while($cli20=mysqli_fetch_array($sql20)){

                    $doc = explode (",",$cli20['documentos']);
				    $maxd = sizeof($doc);
					$d =0 ;
		            while($d < $maxd){
					$cd= $cd+1;
				    echo '<tr>';
		            echo '<td><a target=_blank href="'.$doc[$d].'">'.substr($doc[$d],5).'</a>&nbsp;';
					echo '<td><a class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal_doc_up"> <i class="glyphicon glyphicon-folder-open"></i> Actualizar</a></td>';
?>
<div id="modal_doc_up" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Actualizar Documentos</h4>
      </div>
	 <div class="modal-body">

	 <div class="row">
	<div  class="col-md-8">
	   <form class="cliente" name="contact" enctype="multipart/form-data" id="cliente" method="post" action="accion_cliente.php">
	<div class="input-group">
                <span class="input-group-btn">
                    <span class="btn btn-default btn-file">
					<span class="glyphicon glyphicon-folder-open"></span>
                    <b>&nbsp;Doc.</b>
						<input type="file" name="doc[]" id="doc" multiple="true"/>
                    </span>
                </span>
                <input type="text" class="form-control" readonly>
            </div>
			</div>
        <input type="hidden" value="<?php echo $_GET['cl'];?>" name="cl">
			<input type="hidden" value="<?php if(isset($cli20['id'])){echo $cli20['id']; }?>"  name="id_doc">
		 <button class="btn btn-primary" type="submit" value="Guardar" name="up_doc"><i class="glyphicon glyphicon-check"></i> Guardar</button>
          </form>

			</div>
			</div>

			 <div class="modal-footer">
					<div class="col-md-6 text-left col-md-offset-10">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>

					</div>
				  </div>
			</div>
			</div>
			</div>
			<?php
					echo '<form method="post" action="accion_cliente.php">';
					echo '<input type="hidden" value="'.$cli20['id'].'" name="id_doc">';
					echo '<input type="hidden" value="'.$_GET['cl'].'" name="cl">';
	                echo '<td><button type="submit" class="btn btn-primary btn-xs" name="doc_del"> <i class="glyphicon glyphicon-remove"></i> Eliminar</button></td>';
					echo '</form>';
                    echo '<td></td></tr>';
					$d++;

					 }

					  }
					if ($cd < 10){
					echo '<tr><td><button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal_docs_up"> <i class="glyphicon glyphicon-folder-open"></i> Agregar Documento</button></td></tr>';
					}
					echo '</table>';
					 }

					 //CARPETA DETALLES MENSAJES
					 else if (isset($file)&& $carpeta=="msj"){
					 	echo '<table class="table table-hover table-msj">';
						while($cli16=mysqli_fetch_array($sql16)){

				    echo '<tr><td><b>Asunto</b></td><td><b>Estatus</b></td></tr>';
					echo '<tr><td>'.$cli16['asunto'].'</td>';
					if($cli16['status']=="si"){echo '<td>Enviado</td></tr>';}
					else if ($cli16['status']=="no"){echo '<td>No enviado</td></tr>';$tratado = 1;}
					echo '<tr><td colspan="2"><b>Contenido</b></td></tr>';
					echo '<tr><td colspan="3">'.$cli16['contenido'].'</td></tr>';
					$id_user = $cli16['id_usuario'];
					$sql16a = mysqli_query($conn,"select nombre_completo from usuario where id_usuario='$id_user'");
					$rsql16a = mysqli_fetch_assoc($sql16a);
					$g_user = $rsql16a['nombre_completo'];
					echo '<tr><td><b>Generado Por</b></td><td>'.$g_user.'</td><td><b>Fecha</b></td><td><b>Hora</b></td><td>'.date_format(date_create($cli16['fecha']),'d/m/Y').'</td><td>'.$cli16['hora'].'</td></tr>';
					echo '</table>';

					echo '<div class="col-md-6">
					<form action="crear_mensaje.php" method="post">';

					if($tipo_usuario != 2 || ($tipo_usuario == 2 && ($id_user == $id_usuario))){
						echo '<button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal_msj_up"><i class="glyphicon glyphicon-pencil"></i> Editar</button>&nbsp;';
						echo '<button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal_msj_del"> <i class="glyphicon glyphicon-remove"></i> Eliminar</button>&nbsp;';
						echo '<button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal_enviar"> <i class="glyphicon glyphicon-ok"></i> Enviar</button>&nbsp;';
						if(isset($tratado)){ //imprime el boton de tratado solo si el mensaje no ha sido enviado
							echo '<button type="submit" name="tratado_msj" class="btn btn-primary btn-xs" data-toggle="modal"> <i class="glyphicon glyphicon-flag"></i> Marcar como tratado</button>';
							}

					}

					echo '<input type="hidden" name="id_mensaje" value="'.$cli16['id_mensaje'].'">';
					echo '<input type="hidden" name="cliente" value="'.$_GET['cl'].'">';
					echo '</form></div>';

					}


					 }//Fin detalles carpeta mensajes

				//CARPETA RECORDATORIOS
				else if (isset($file)&& $carpeta=="recor"){
				echo '<table class="table table-hover table-recor">';
				while($cli17=mysqli_fetch_array($sql17)){
					if($cli17['asunto']== "Recordatorio de pago"){
				    echo '<tr><td><b>Liquidación</b></td><td><b>Asunto</b></td><td><b>Estatus</b></td></tr>';
					echo '<tr><td>'.$cli17['id_recordatorio'].'</td><td>'.$cli17['asunto'].'</td>';}
					if($cli17['asunto']!= "Recordatorio de pago"){
				    echo '<tr><td><b>Asunto</b></td><td><b>Estatus</b></td></tr>';
					echo '<tr><td>'.$cli17['asunto'].'</td>';}

					if($cli17['status']=="si"){echo '<td>Enviado</td></tr>';}
					else if ($cli17['status']=="no"){echo '<td>No enviado</td></tr>';$tratado = 1;}

					echo '<tr><td colspan="3"><b>Contenido</b></td></tr>';

					if($cli17['asunto']== "Recordatorio de pago"){
					echo '<tr><td colspan="3"><textarea disabled class="recor_text">'.$cli17['contenido'].'</textarea></td></tr>';
					}
					if($cli17['asunto']!= "Recordatorio de pago"){
					echo '<tr><td colspan="3">'.$cli17['contenido'].'</td></tr>';
					}

					$id_user = $cli17['id_usuario'];
					$sql17a = mysqli_query($conn,"select nombre_completo from usuario where id_usuario='$id_user'");
					$rsql17a = mysqli_fetch_assoc($sql17a);
					$g_user = $rsql17a['nombre_completo'];
					echo '<tr><td><b>Generado Por</b></td><td>'.$g_user.'</td><td><b>Fecha</b></td><td><b>Hora</b></td><td>'.date_format(date_create($cli17['fecha']),'d/m/Y').'</td><td>'.$cli17['hora'].'</td></tr>';
					echo '</table>';

					echo '<div class="col-md-6">
					<form action="crear_recordatorio.php" method="post">
					<button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal_recor_up"><i class="glyphicon glyphicon-pencil"></i> Editar</button>&nbsp;';

					if($tipo_usuario != 2 ){
					echo '<button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal_recor_del"> <i class="glyphicon glyphicon-remove"></i> Eliminar</button>&nbsp;';
				    }

					echo '<button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal_enviar"> <i class="glyphicon glyphicon-ok"></i> Enviar</button>&nbsp;';
					echo '<input type="hidden" name="id_recordatorio" value="'.$cli17['id_recordatorio'].'">';
					echo '<input type="hidden" name="cliente" value="'.$_GET['cl'].'">';
					if(isset($tratado)){
					echo '<button type="submit" name="tratado_recor" class="btn btn-primary btn-xs" data-toggle="modal"> <i class="glyphicon glyphicon-flag"></i> Marcar como tratado</button></form></div>';
					} }

					 } //Fin detalles carpeta recordatorios


					/*Detalles carpeta Indicadores*/

					else if (isset($carpeta)&& isset($file)&& $carpeta=="param"){
					    $natu='';
					    $username='';
                   	echo '<table class="table table-hover table-compl table-list-cp" id="table_ind">';
				    while($cli_param=mysqli_fetch_array($sql18)){


						$nom_param = $cli_param['nombre_parametro'];
						$id_user = $cli_param['id_usuario'];
						$sql18a = mysqli_query($conn,"select nombre from usuario where id_usuario='$id_user'");
						$rsql18a = mysqli_fetch_assoc($sql18a);
						$g_user = $rsql18a['nombre'];
                        $username = $rsql18a['nombre'];
						$hora = $cli_param['hora'];
						$fecha = $cli_param['fecha'];
						$nat = $cli_param['naturaleza'];
                        $active = $cli_param['active'];
						$id_param = $cli_param['id_parametro'];

						$natu = $nat;


					}

					// indicadores para cliente
					if(isset($cliente)){
                        $sql18d = mysqli_query($conn,"select nombre from cliente where id_cliente='$cliente'");
                        $rsql18d = mysqli_fetch_assoc($sql18d);
                        $pcliente = $rsql18d['nombre'];
                        $sql18dr = mysqli_query($conn,"select razon from cliente where id_cliente='$cliente'");
                        $rsql18dr = mysqli_fetch_assoc($sql18dr);
                        $rcliente = $rsql18dr['razon'];
					echo '<thead><tr><th><b>Indicador </b> '.$nom_param.'</th>
                            <th><b>Naturaleza </b> '.$nat.'</th></tr>';

//					echo '<tr></tr>';
                    echo '<tr><th class="tvalor" id="btnreferencia"><b>Referencia</b></th><th class="tvalor" id="btnvalor"><b>Valor</b></th>';
                    if($natu == '$') {
                        if($active) {
                            echo '<th><b>Subtotal</b></th>';
                        }
                    }
					echo '<th class="tvalor" id="btnfecha"><b>Fecha</b></th><th>Accion</th></tr></thead>';
					$nombre_cli = $pcliente;
					$razon_cli = $rcliente;

					$tbl_excel1 = array();
					$tbl_excel2 = array();
					$tbl_excel3 = array();
                    $tbl_excel4 = array();

                        $tabless = [];
                        while ($com =  mysqli_fetch_array($sql18b)){
                            array_push($tabless, $com);
                        }
                        echo '<tbody id="orden" class="hidden">';
                        $sub = 0;
                        foreach($tabless as $cli_param){

                            $nombre_ref = 'modal_param_'.$cli_param['referencia'];
                            $tbl_excel1[] = $cli_param['referencia'];
                            echo '<tr id="ind_'.$cli_param['id'].'"><td>'.$cli_param['referencia'].'</td>';

                            //aplicacion de mascara
                            if($cli_param['naturaleza'] == "hh:mm"){
                                $valor = $cli_param['valor'];
                                $tbl_excel2[]= $cli_param['valor'];
                                echo '<td>'.$cli_param['valor'].'</td>';
                            }
                            if($cli_param['naturaleza'] == "$"){
                                //setlocale(LC_MONETARY, 'en_US');
                                $nv  = $cli_param['valor'];
                                $valor = $nv;
                                $tbl_excel2[]=$nv;
                                echo '<td>$ '.number_format((float)$nv, 2, '.', ',').'</td>';
                                $sub = $sub + $cli_param['valor'];
                                if($active) {
                                    $tbl_excel4[]=$sub;
                                    echo '<td>$ '.number_format((float)$sub, 2, '.', ',').'</td>';
                                }

                            }
                            if($cli_param['naturaleza'] == "nn"){
                                $tbl_excel2[]=number_format((float)$cli_param['valor'], 2, '.', ',');
                                $valor = number_format((float)$cli_param['valor'], 2, '.', ',');
                                echo '<td>'.number_format((float)$cli_param['valor'], 2, '.', ',').'</td>';
                            }
                            if($cli_param['naturaleza'] == "%"){
                                $tbl_excel2[]=number_format((float)$cli_param['valor'], 2, '.', ',').'%';
                                $valor = number_format((float)$cli_param['valor'], 2, '.', ',');
                                echo '<td>'.number_format((float)$cli_param['valor'], 2, '.', ',').'%</td>';
                            }


                            $tbl_excel3[]=date_format(date_create($cli_param['fecha']),'d/m/Y');
                            ?>

                            <td><?php echo date_format(date_create($cli_param['fecha']),'d/m/Y');?></td>

                            <td>
                                <button class="btn btn-primary btn-xs setupdate" id="<?php echo $cli_param['id'];?>" data-toggle="modal" data-target="#<?php echo $cli_param['id']; ?>">
                                    <i class="glyphicon glyphicon-pencil"></i>
                                    Editar
                                </button>
                                <button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#del_<?php echo $cli_param['id']; ?>">
                                    <i class="glyphicon glyphicon-remove"></i>
                                    Eliminar
                                </button>
                            </td>
                            </tr>

                            <!-- MODAL ACTUALIZAR REFERENCIA-->
                            <div id="<?php echo $cli_param['id']; ?>" class="modal fade" role="dialog">
                                <div class="modal-dialog">
                                    Modal content
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Actualizar Referencia de Indicador</h4> </div>
                                        <div class="modal-body">
                                            <form class="cliente" name="contact" enctype="multipart/form-data" id="indicador_upref_<?php echo $cli_param['id']?>" method="post" action="crear_parametro.php">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <input type="text" name="referencia" class="form-control" placeholder="Referencia" value="<?php echo $cli_param['referencia'] ?>">
                                                        </div>
                                                        <div class="form-group">
                                                            <?php if($cli_param['naturaleza']=='hh:mm'){$class_valor="valord"; $event="";}
                                                            if($cli_param['naturaleza'] == '$' || $cli_param['naturaleza'] == 'nn' || $cli_param['naturaleza'] == '%' ) {
                                                                $event = 'return isNumberKey(this,event)';
                                                                $class_valor = "valor_num"; } ?>
                                                            <input type="text" name="valor" class="form-control <?php echo $class_valor;?>"
                                                                   onkeypress="<?php echo $event;?>"
                                                                   placeholder="Valor en <?php echo $cli_param['naturaleza'];?>" value="<?php echo $valor; ?>">
                                                        </div>
                                                    </div>

                                                    <?php $fechaind_upref = date('m/d/Y',strtotime($cli_param['fecha'])); ?>

                                                    <div class="col-md-6">
                                                        <label>Fecha de referencia</label>
                                                        <div class="date_upref_indicador" style="font-size:10px;"></div>
                                                    </div>

                                                </div>
                                        </div>
                                        <div class="modal-footer">
                                            <div class="col-md-6 text-left col-md-offset-8">
                                                <input type="hidden" name="fecha" id="fechaind_upref_<?php echo $cli_param['id'];?>" value="<?php echo $fechaind_upref;?>"/>
                                                <input type="hidden" name="id_referencia" value="<?php echo $cli_param['id']?>"/>
                                                <input type="hidden" name="id_parametro" value="<?php echo $cli_param['id_parametro']?>"/>
                                                <input type="hidden" name="tipo" value="<?php echo $cli_param['naturaleza']?>"/>
                                                <input type="hidden" name="cliente" value="<?php echo $cliente ?>"/>
                                                <button class="btn btn-primary" type="submit" value="Crear" name="up_ref"><i class="glyphicon glyphicon-check"></i> Guardar</button>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- MODAL ELIMINAR REFERENCIA-->
                            <div id="del_<?php echo $cli_param['id']; ?>" class="modal fade" role="dialog">
                                <div class="modal-dialog">
                                    Modal content
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Eliminar Referencia</h4> </div>
                                        <div class="modal-body">
                                            <form class="cliente" name="contact" enctype="multipart/form-data" id="parametro" method="post" action="crear_parametro.php">
                                                <div class="row">
                                                    <div class="col-md-12">

                                                        <p> ¿ Esta seguro que desea eliminar la referencia <?php echo $cli_param['referencia'] ?>? </p>
                                                    </div>
                                                </div>
                                        </div>
                                        <div class="modal-footer">
                                            <div class="col-md-6 text-left col-md-offset-8">
                                                <input type="hidden" name="id_referencia" value="<?php echo $cli_param['id']?>"/>
                                                <input type="hidden" name="id_parametro" value="<?php echo $cli_param['id_parametro']?>"/>
                                                <input type="hidden" name="cliente" value="<?php echo $cliente ?>"/>
                                                <button class="btn btn-primary" type="submit" value="Crear" name="del_ref"><i class="glyphicon glyphicon-remove"></i> Eliminar</button>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <?php

                            $ref_idusuario = $cli_param['id_usuario'];
                            $sqlref_usuario = mysqli_query($conn,"select nombre from usuario where id_usuario='$ref_idusuario'");
                            $nameref = mysqli_fetch_assoc($sqlref_usuario);
                            $fecharef= $cli_param['fecha'];
                            $horaref= $cli_param['hora'];

                        }
                        echo '</tbody>';
                        echo '<tbody id="fecha_desc" class="hidden">';
                        // orden descendente fecha
                        usort($tabless, function ($a, $b) {
                            return strcmp($a["fecha"], $b["fecha"]);
                        });
                        $sub = 0;
                        foreach($tabless as $cli_param){

                            $nombre_ref = 'modal_param_'.$cli_param['referencia'];
                            echo '<tr id="ind_'.$cli_param['id'].'"><td>'.$cli_param['referencia'].'</td>';

                            //aplicacion de mascara
                            if($cli_param['naturaleza'] == "hh:mm"){
                                $valor = $cli_param['valor'];
                                echo '<td>'.$cli_param['valor'].'</td>';
                            }
                            if($cli_param['naturaleza'] == "$"){
                                //setlocale(LC_MONETARY, 'en_US');
                                $nv  = $cli_param['valor'];
                                $valor = $nv;
                                echo '<td>$ '.number_format((float)$nv, 2, '.', ',').'</td>';
                                $sub = $sub + $cli_param['valor'];
                                if($active) {
                                    echo '<td>$ '.number_format((float)$sub, 2, '.', ',').'</td>';
                                }

                            }
                            if($cli_param['naturaleza'] == "nn"){
                                $valor = number_format((float)$cli_param['valor'], 2, '.', ',');
                                echo '<td>'.number_format((float)$cli_param['valor'], 2, '.', ',').'</td>';
                            }
                            if($cli_param['naturaleza'] == "%"){
                                $valor = number_format((float)$cli_param['valor'], 2, '.', ',');
                                echo '<td>'.number_format((float)$cli_param['valor'], 2, '.', ',').'%</td>';
                            }


                            $tbl_excel3[]=date_format(date_create($cli_param['fecha']),'d/m/Y');
                            ?>

                            <td><?php echo date_format(date_create($cli_param['fecha']),'d/m/Y');?></td>

                            <td>
                                <button class="btn btn-primary btn-xs setupdate" id="<?php echo $cli_param['id'];?>" data-toggle="modal" data-target="#<?php echo $cli_param['id']; ?>">
                                    <i class="glyphicon glyphicon-pencil"></i>
                                    Editar
                                </button>
                                <button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#del_<?php echo $cli_param['id']; ?>">
                                    <i class="glyphicon glyphicon-remove"></i>
                                    Eliminar
                                </button>
                            </td>
                            </tr>

                            <!-- MODAL ACTUALIZAR REFERENCIA-->
                            <div id="<?php echo $cli_param['id']; ?>" class="modal fade" role="dialog">
                                <div class="modal-dialog">
                                    Modal content
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Actualizar Referencia de Indicador</h4> </div>
                                        <div class="modal-body">
                                            <form class="cliente" name="contact" enctype="multipart/form-data" id="indicador_upref_<?php echo $cli_param['id']?>" method="post" action="crear_parametro.php">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <input type="text" name="referencia" class="form-control" placeholder="Referencia" value="<?php echo $cli_param['referencia'] ?>">
                                                        </div>
                                                        <div class="form-group">
                                                            <?php if($cli_param['naturaleza']=='hh:mm'){$class_valor="valord"; $event="";}
                                                            if($cli_param['naturaleza'] == '$' || $cli_param['naturaleza'] == 'nn' || $cli_param['naturaleza'] == '%' ) {
                                                                $event = 'return isNumberKey(this,event)';
                                                                $class_valor = "valor_num"; } ?>
                                                            <input type="text" name="valor" class="form-control <?php echo $class_valor;?>"
                                                                   onkeypress="<?php echo $event;?>"
                                                                   placeholder="Valor en <?php echo $cli_param['naturaleza'];?>" value="<?php echo $valor; ?>">
                                                        </div>
                                                    </div>

                                                    <?php $fechaind_upref = date('m/d/Y',strtotime($cli_param['fecha'])); ?>

                                                    <div class="col-md-6">
                                                        <label>Fecha de referencia</label>
                                                        <div class="date_upref_indicador" style="font-size:10px;"></div>
                                                    </div>

                                                </div>
                                        </div>
                                        <div class="modal-footer">
                                            <div class="col-md-6 text-left col-md-offset-8">
                                                <input type="hidden" name="fecha" id="fechaind_upref_<?php echo $cli_param['id'];?>" value="<?php echo $fechaind_upref;?>"/>
                                                <input type="hidden" name="id_referencia" value="<?php echo $cli_param['id']?>"/>
                                                <input type="hidden" name="id_parametro" value="<?php echo $cli_param['id_parametro']?>"/>
                                                <input type="hidden" name="tipo" value="<?php echo $cli_param['naturaleza']?>"/>
                                                <input type="hidden" name="cliente" value="<?php echo $cliente ?>"/>
                                                <button class="btn btn-primary" type="submit" value="Crear" name="up_ref"><i class="glyphicon glyphicon-check"></i> Guardar</button>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- MODAL ELIMINAR REFERENCIA-->
                            <div id="del_<?php echo $cli_param['id']; ?>" class="modal fade" role="dialog">
                                <div class="modal-dialog">
                                    Modal content
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Eliminar Referencia</h4> </div>
                                        <div class="modal-body">
                                            <form class="cliente" name="contact" enctype="multipart/form-data" id="parametro" method="post" action="crear_parametro.php">
                                                <div class="row">
                                                    <div class="col-md-12">

                                                        <p> ¿ Esta seguro que desea eliminar la referencia <?php echo $cli_param['referencia'] ?>? </p>
                                                    </div>
                                                </div>
                                        </div>
                                        <div class="modal-footer">
                                            <div class="col-md-6 text-left col-md-offset-8">
                                                <input type="hidden" name="id_referencia" value="<?php echo $cli_param['id']?>"/>
                                                <input type="hidden" name="id_parametro" value="<?php echo $cli_param['id_parametro']?>"/>
                                                <input type="hidden" name="cliente" value="<?php echo $cliente ?>"/>
                                                <button class="btn btn-primary" type="submit" value="Crear" name="del_ref"><i class="glyphicon glyphicon-remove"></i> Eliminar</button>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <?php

                            $ref_idusuario = $cli_param['id_usuario'];
                            $sqlref_usuario = mysqli_query($conn,"select nombre from usuario where id_usuario='$ref_idusuario'");
                            $nameref = mysqli_fetch_assoc($sqlref_usuario);
                            $fecharef= $cli_param['fecha'];
                            $horaref= $cli_param['hora'];

                        }
                        echo '</tbody>';
                        echo '<tbody id="fecha_asc" class="hidden">';
                        // orden ascendente fecha
                        usort($tabless, function ($a, $b) {
                            return strcmp($b["fecha"], $a["fecha"]);
                        });
                        $sub = 0;
                        foreach($tabless as $cli_param){

                            $nombre_ref = 'modal_param_'.$cli_param['referencia'];
                            echo '<tr id="ind_'.$cli_param['id'].'"><td>'.$cli_param['referencia'].'</td>';

                            //aplicacion de mascara
                            if($cli_param['naturaleza'] == "hh:mm"){
                                $valor = $cli_param['valor'];
                                echo '<td>'.$cli_param['valor'].'</td>';
                            }
                            if($cli_param['naturaleza'] == "$"){
                                //setlocale(LC_MONETARY, 'en_US');
                                $nv  = $cli_param['valor'];
                                $valor = $nv;
                                echo '<td>$ '.number_format((float)$nv, 2, '.', ',').'</td>';
                                $sub = $sub + $cli_param['valor'];
                                if($active) {
                                    echo '<td>$ '.number_format((float)$sub, 2, '.', ',').'</td>';
                                }

                            }
                            if($cli_param['naturaleza'] == "nn"){
                                $valor = number_format((float)$cli_param['valor'], 2, '.', ',');
                                echo '<td>'.number_format((float)$cli_param['valor'], 2, '.', ',').'</td>';
                            }
                            if($cli_param['naturaleza'] == "%"){
                                $valor = number_format((float)$cli_param['valor'], 2, '.', ',');
                                echo '<td>'.number_format((float)$cli_param['valor'], 2, '.', ',').'%</td>';
                            }
                            ?>

                            <td><?php echo date_format(date_create($cli_param['fecha']),'d/m/Y');?></td>

                            <td>
                                <button class="btn btn-primary btn-xs setupdate" id="<?php echo $cli_param['id'];?>" data-toggle="modal" data-target="#<?php echo $cli_param['id']; ?>">
                                    <i class="glyphicon glyphicon-pencil"></i>
                                    Editar
                                </button>
                                <button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#del_<?php echo $cli_param['id']; ?>">
                                    <i class="glyphicon glyphicon-remove"></i>
                                    Eliminar
                                </button>
                            </td>
                            </tr>

                            <!-- MODAL ACTUALIZAR REFERENCIA-->
                            <div id="<?php echo $cli_param['id']; ?>" class="modal fade" role="dialog">
                                <div class="modal-dialog">
                                    Modal content
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Actualizar Referencia de Indicador</h4> </div>
                                        <div class="modal-body">
                                            <form class="cliente" name="contact" enctype="multipart/form-data" id="indicador_upref_<?php echo $cli_param['id']?>" method="post" action="crear_parametro.php">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <input type="text" name="referencia" class="form-control" placeholder="Referencia" value="<?php echo $cli_param['referencia'] ?>">
                                                        </div>
                                                        <div class="form-group">
                                                            <?php if($cli_param['naturaleza']=='hh:mm'){$class_valor="valord"; $event="";}
                                                            if($cli_param['naturaleza'] == '$' || $cli_param['naturaleza'] == 'nn' || $cli_param['naturaleza'] == '%' ) {
                                                                $event = 'return isNumberKey(this,event)';
                                                                $class_valor = "valor_num"; } ?>
                                                            <input type="text" name="valor" class="form-control <?php echo $class_valor;?>"
                                                                   onkeypress="<?php echo $event;?>"
                                                                   placeholder="Valor en <?php echo $cli_param['naturaleza'];?>" value="<?php echo $valor; ?>">
                                                        </div>
                                                    </div>

                                                    <?php $fechaind_upref = date('m/d/Y',strtotime($cli_param['fecha'])); ?>

                                                    <div class="col-md-6">
                                                        <label>Fecha de referencia</label>
                                                        <div class="date_upref_indicador" style="font-size:10px;"></div>
                                                    </div>

                                                </div>
                                        </div>
                                        <div class="modal-footer">
                                            <div class="col-md-6 text-left col-md-offset-8">
                                                <input type="hidden" name="fecha" id="fechaind_upref_<?php echo $cli_param['id'];?>" value="<?php echo $fechaind_upref;?>"/>
                                                <input type="hidden" name="id_referencia" value="<?php echo $cli_param['id']?>"/>
                                                <input type="hidden" name="id_parametro" value="<?php echo $cli_param['id_parametro']?>"/>
                                                <input type="hidden" name="tipo" value="<?php echo $cli_param['naturaleza']?>"/>
                                                <input type="hidden" name="cliente" value="<?php echo $cliente ?>"/>
                                                <button class="btn btn-primary" type="submit" value="Crear" name="up_ref"><i class="glyphicon glyphicon-check"></i> Guardar</button>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- MODAL ELIMINAR REFERENCIA-->
                            <div id="del_<?php echo $cli_param['id']; ?>" class="modal fade" role="dialog">
                                <div class="modal-dialog">
                                    Modal content
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Eliminar Referencia</h4> </div>
                                        <div class="modal-body">
                                            <form class="cliente" name="contact" enctype="multipart/form-data" id="parametro" method="post" action="crear_parametro.php">
                                                <div class="row">
                                                    <div class="col-md-12">

                                                        <p> ¿ Esta seguro que desea eliminar la referencia <?php echo $cli_param['referencia'] ?>? </p>
                                                    </div>
                                                </div>
                                        </div>
                                        <div class="modal-footer">
                                            <div class="col-md-6 text-left col-md-offset-8">
                                                <input type="hidden" name="id_referencia" value="<?php echo $cli_param['id']?>"/>
                                                <input type="hidden" name="id_parametro" value="<?php echo $cli_param['id_parametro']?>"/>
                                                <input type="hidden" name="cliente" value="<?php echo $cliente ?>"/>
                                                <button class="btn btn-primary" type="submit" value="Crear" name="del_ref"><i class="glyphicon glyphicon-remove"></i> Eliminar</button>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <?php

                            $ref_idusuario = $cli_param['id_usuario'];
                            $sqlref_usuario = mysqli_query($conn,"select nombre from usuario where id_usuario='$ref_idusuario'");
                            $nameref = mysqli_fetch_assoc($sqlref_usuario);
                            $fecharef= $cli_param['fecha'];
                            $horaref= $cli_param['hora'];

                        }
                        echo '</tbody>';
                        echo '<tbody id="valor_desc" class="hidden">';
                        // orden descendente valor
                        usort($tabless, function ($a, $b) {
                            return $a['valor'] - $b['valor'];
                        });
                        $sub = 0;
                        foreach($tabless as $cli_param){

                            $nombre_ref = 'modal_param_'.$cli_param['referencia'];
                            echo '<tr id="ind_'.$cli_param['id'].'"><td>'.$cli_param['referencia'].'</td>';

                            //aplicacion de mascara
                            if($cli_param['naturaleza'] == "hh:mm"){
                                $valor = $cli_param['valor'];
                                echo '<td>'.$cli_param['valor'].'</td>';
                            }
                            if($cli_param['naturaleza'] == "$"){
                                //setlocale(LC_MONETARY, 'en_US');
                                $nv  = $cli_param['valor'];
                                $valor = $nv;
                                echo '<td>$ '.number_format((float)$nv, 2, '.', ',').'</td>';
                                $sub = $sub + $cli_param['valor'];
                                if($active) {
                                    echo '<td>$ '.number_format((float)$sub, 2, '.', ',').'</td>';
                                }

                            }
                            if($cli_param['naturaleza'] == "nn"){
                                $valor = number_format((float)$cli_param['valor'], 2, '.', ',');
                                echo '<td>'.number_format((float)$cli_param['valor'], 2, '.', ',').'</td>';
                            }
                            if($cli_param['naturaleza'] == "%"){
                                $valor = number_format((float)$cli_param['valor'], 2, '.', ',');
                                echo '<td>'.number_format((float)$cli_param['valor'], 2, '.', ',').'%</td>';
                            }
                            ?>

                            <td><?php echo date_format(date_create($cli_param['fecha']),'d/m/Y');?></td>

                            <td>
                                <button class="btn btn-primary btn-xs setupdate" id="<?php echo $cli_param['id'];?>" data-toggle="modal" data-target="#<?php echo $cli_param['id']; ?>">
                                    <i class="glyphicon glyphicon-pencil"></i>
                                    Editar
                                </button>
                                <button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#del_<?php echo $cli_param['id']; ?>">
                                    <i class="glyphicon glyphicon-remove"></i>
                                    Eliminar
                                </button>
                            </td>
                            </tr>

                            <!-- MODAL ACTUALIZAR REFERENCIA-->
                            <div id="<?php echo $cli_param['id']; ?>" class="modal fade" role="dialog">
                                <div class="modal-dialog">
                                    Modal content
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Actualizar Referencia de Indicador</h4> </div>
                                        <div class="modal-body">
                                            <form class="cliente" name="contact" enctype="multipart/form-data" id="indicador_upref_<?php echo $cli_param['id']?>" method="post" action="crear_parametro.php">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <input type="text" name="referencia" class="form-control" placeholder="Referencia" value="<?php echo $cli_param['referencia'] ?>">
                                                        </div>
                                                        <div class="form-group">
                                                            <?php if($cli_param['naturaleza']=='hh:mm'){$class_valor="valord"; $event="";}
                                                            if($cli_param['naturaleza'] == '$' || $cli_param['naturaleza'] == 'nn' || $cli_param['naturaleza'] == '%' ) {
                                                                $event = 'return isNumberKey(this,event)';
                                                                $class_valor = "valor_num"; } ?>
                                                            <input type="text" name="valor" class="form-control <?php echo $class_valor;?>"
                                                                   onkeypress="<?php echo $event;?>"
                                                                   placeholder="Valor en <?php echo $cli_param['naturaleza'];?>" value="<?php echo $valor; ?>">
                                                        </div>
                                                    </div>

                                                    <?php $fechaind_upref = date('m/d/Y',strtotime($cli_param['fecha'])); ?>

                                                    <div class="col-md-6">
                                                        <label>Fecha de referencia</label>
                                                        <div class="date_upref_indicador" style="font-size:10px;"></div>
                                                    </div>

                                                </div>
                                        </div>
                                        <div class="modal-footer">
                                            <div class="col-md-6 text-left col-md-offset-8">
                                                <input type="hidden" name="fecha" id="fechaind_upref_<?php echo $cli_param['id'];?>" value="<?php echo $fechaind_upref;?>"/>
                                                <input type="hidden" name="id_referencia" value="<?php echo $cli_param['id']?>"/>
                                                <input type="hidden" name="id_parametro" value="<?php echo $cli_param['id_parametro']?>"/>
                                                <input type="hidden" name="tipo" value="<?php echo $cli_param['naturaleza']?>"/>
                                                <input type="hidden" name="cliente" value="<?php echo $cliente ?>"/>
                                                <button class="btn btn-primary" type="submit" value="Crear" name="up_ref"><i class="glyphicon glyphicon-check"></i> Guardar</button>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- MODAL ELIMINAR REFERENCIA-->
                            <div id="del_<?php echo $cli_param['id']; ?>" class="modal fade" role="dialog">
                                <div class="modal-dialog">
                                    Modal content
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Eliminar Referencia</h4> </div>
                                        <div class="modal-body">
                                            <form class="cliente" name="contact" enctype="multipart/form-data" id="parametro" method="post" action="crear_parametro.php">
                                                <div class="row">
                                                    <div class="col-md-12">

                                                        <p> ¿ Esta seguro que desea eliminar la referencia <?php echo $cli_param['referencia'] ?>? </p>
                                                    </div>
                                                </div>
                                        </div>
                                        <div class="modal-footer">
                                            <div class="col-md-6 text-left col-md-offset-8">
                                                <input type="hidden" name="id_referencia" value="<?php echo $cli_param['id']?>"/>
                                                <input type="hidden" name="id_parametro" value="<?php echo $cli_param['id_parametro']?>"/>
                                                <input type="hidden" name="cliente" value="<?php echo $cliente ?>"/>
                                                <button class="btn btn-primary" type="submit" value="Crear" name="del_ref"><i class="glyphicon glyphicon-remove"></i> Eliminar</button>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <?php

                            $ref_idusuario = $cli_param['id_usuario'];
                            $sqlref_usuario = mysqli_query($conn,"select nombre from usuario where id_usuario='$ref_idusuario'");
                            $nameref = mysqli_fetch_assoc($sqlref_usuario);
                            $fecharef= $cli_param['fecha'];
                            $horaref= $cli_param['hora'];

                        }
                        echo '</tbody>';
                        echo '<tbody id="valor_asc" class="hidden">';
                        // orden ascendente valor
                        usort($tabless, function ($a, $b) {
                            return $b['valor'] - $a['valor'];
                        });
                        $sub = 0;
                        foreach($tabless as $cli_param){

                            $nombre_ref = 'modal_param_'.$cli_param['referencia'];
                            echo '<tr id="ind_'.$cli_param['id'].'"><td>'.$cli_param['referencia'].'</td>';

                            //aplicacion de mascara
                            if($cli_param['naturaleza'] == "hh:mm"){
                                $valor = $cli_param['valor'];
                                echo '<td>'.$cli_param['valor'].'</td>';
                            }
                            if($cli_param['naturaleza'] == "$"){
                                //setlocale(LC_MONETARY, 'en_US');
                                $nv  = $cli_param['valor'];
                                $valor = $nv;
                                echo '<td>$ '.number_format((float)$nv, 2, '.', ',').'</td>';
                                $sub = $sub + $cli_param['valor'];
                                if($active) {
                                    echo '<td>$ '.number_format((float)$sub, 2, '.', ',').'</td>';
                                }

                            }
                            if($cli_param['naturaleza'] == "nn"){
                                $valor = number_format((float)$cli_param['valor'], 2, '.', ',');
                                echo '<td>'.number_format((float)$cli_param['valor'], 2, '.', ',').'</td>';
                            }
                            if($cli_param['naturaleza'] == "%"){
                                $valor = number_format((float)$cli_param['valor'], 2, '.', ',');
                                echo '<td>'.number_format((float)$cli_param['valor'], 2, '.', ',').'%</td>';
                            }
                            ?>

                            <td><?php echo date_format(date_create($cli_param['fecha']),'d/m/Y');?></td>

                            <td>
                                <button class="btn btn-primary btn-xs setupdate" id="<?php echo $cli_param['id'];?>" data-toggle="modal" data-target="#<?php echo $cli_param['id']; ?>">
                                    <i class="glyphicon glyphicon-pencil"></i>
                                    Editar
                                </button>
                                <button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#del_<?php echo $cli_param['id']; ?>">
                                    <i class="glyphicon glyphicon-remove"></i>
                                    Eliminar
                                </button>
                            </td>
                            </tr>

                            <!-- MODAL ACTUALIZAR REFERENCIA-->
                            <div id="<?php echo $cli_param['id']; ?>" class="modal fade" role="dialog">
                                <div class="modal-dialog">
                                    Modal content
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Actualizar Referencia de Indicador</h4> </div>
                                        <div class="modal-body">
                                            <form class="cliente" name="contact" enctype="multipart/form-data" id="indicador_upref_<?php echo $cli_param['id']?>" method="post" action="crear_parametro.php">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <input type="text" name="referencia" class="form-control" placeholder="Referencia" value="<?php echo $cli_param['referencia'] ?>">
                                                        </div>
                                                        <div class="form-group">
                                                            <?php if($cli_param['naturaleza']=='hh:mm'){$class_valor="valord"; $event="";}
                                                            if($cli_param['naturaleza'] == '$' || $cli_param['naturaleza'] == 'nn' || $cli_param['naturaleza'] == '%' ) {
                                                                $event = 'return isNumberKey(this,event)';
                                                                $class_valor = "valor_num"; } ?>
                                                            <input type="text" name="valor" class="form-control <?php echo $class_valor;?>"
                                                                   onkeypress="<?php echo $event;?>"
                                                                   placeholder="Valor en <?php echo $cli_param['naturaleza'];?>" value="<?php echo $valor; ?>">
                                                        </div>
                                                    </div>

                                                    <?php $fechaind_upref = date('m/d/Y',strtotime($cli_param['fecha'])); ?>

                                                    <div class="col-md-6">
                                                        <label>Fecha de referencia</label>
                                                        <div class="date_upref_indicador" style="font-size:10px;"></div>
                                                    </div>

                                                </div>
                                        </div>
                                        <div class="modal-footer">
                                            <div class="col-md-6 text-left col-md-offset-8">
                                                <input type="hidden" name="fecha" id="fechaind_upref_<?php echo $cli_param['id'];?>" value="<?php echo $fechaind_upref;?>"/>
                                                <input type="hidden" name="id_referencia" value="<?php echo $cli_param['id']?>"/>
                                                <input type="hidden" name="id_parametro" value="<?php echo $cli_param['id_parametro']?>"/>
                                                <input type="hidden" name="tipo" value="<?php echo $cli_param['naturaleza']?>"/>
                                                <input type="hidden" name="cliente" value="<?php echo $cliente ?>"/>
                                                <button class="btn btn-primary" type="submit" value="Crear" name="up_ref"><i class="glyphicon glyphicon-check"></i> Guardar</button>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- MODAL ELIMINAR REFERENCIA-->
                            <div id="del_<?php echo $cli_param['id']; ?>" class="modal fade" role="dialog">
                                <div class="modal-dialog">
                                    Modal content
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Eliminar Referencia</h4> </div>
                                        <div class="modal-body">
                                            <form class="cliente" name="contact" enctype="multipart/form-data" id="parametro" method="post" action="crear_parametro.php">
                                                <div class="row">
                                                    <div class="col-md-12">

                                                        <p> ¿ Esta seguro que desea eliminar la referencia <?php echo $cli_param['referencia'] ?>? </p>
                                                    </div>
                                                </div>
                                        </div>
                                        <div class="modal-footer">
                                            <div class="col-md-6 text-left col-md-offset-8">
                                                <input type="hidden" name="id_referencia" value="<?php echo $cli_param['id']?>"/>
                                                <input type="hidden" name="id_parametro" value="<?php echo $cli_param['id_parametro']?>"/>
                                                <input type="hidden" name="cliente" value="<?php echo $cliente ?>"/>
                                                <button class="btn btn-primary" type="submit" value="Crear" name="del_ref"><i class="glyphicon glyphicon-remove"></i> Eliminar</button>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <?php

                            $ref_idusuario = $cli_param['id_usuario'];
                            $sqlref_usuario = mysqli_query($conn,"select nombre from usuario where id_usuario='$ref_idusuario'");
                            $nameref = mysqli_fetch_assoc($sqlref_usuario);
                            $fecharef= $cli_param['fecha'];
                            $horaref= $cli_param['hora'];

                        }
                        echo '</tbody>';
                        echo '<tbody id="referencia_desc" class="hidden">';
                        // orden descendente referencia
                        foreach ($tabless as $clave => $fila) {
                            $referencias[$clave] = $fila['referencia'];
                            $fechas[$clave] = $fila['fecha'];
                        }
                        $referencias  = array_column($tabless, 'referencia');
                        $fechas = array_column($tabless, 'fecha');
                        array_multisort($referencias, SORT_DESC, $fechas, SORT_DESC, $tabless);
                        $sub = 0;
                        foreach($tabless as $cli_param){

                            $nombre_ref = 'modal_param_'.$cli_param['referencia'];
                            echo '<tr id="ind_'.$cli_param['id'].'"><td>'.$cli_param['referencia'].'</td>';

                            //aplicacion de mascara
                            if($cli_param['naturaleza'] == "hh:mm"){
                                $valor = $cli_param['valor'];
                                echo '<td>'.$cli_param['valor'].'</td>';
                            }
                            if($cli_param['naturaleza'] == "$"){
                                //setlocale(LC_MONETARY, 'en_US');
                                $nv  = $cli_param['valor'];
                                $valor = $nv;
                                echo '<td>$ '.number_format((float)$nv, 2, '.', ',').'</td>';
                                $sub = $sub + $cli_param['valor'];
                                if($active) {
                                    echo '<td>$ '.number_format((float)$sub, 2, '.', ',').'</td>';
                                }

                            }
                            if($cli_param['naturaleza'] == "nn"){
                                $valor = number_format((float)$cli_param['valor'], 2, '.', ',');
                                echo '<td>'.number_format((float)$cli_param['valor'], 2, '.', ',').'</td>';
                            }
                            if($cli_param['naturaleza'] == "%"){
                                $valor = number_format((float)$cli_param['valor'], 2, '.', ',');
                                echo '<td>'.number_format((float)$cli_param['valor'], 2, '.', ',').'%</td>';
                            }
                            ?>

                            <td><?php echo date_format(date_create($cli_param['fecha']),'d/m/Y');?></td>

                            <td>
                                <button class="btn btn-primary btn-xs setupdate" id="<?php echo $cli_param['id'];?>" data-toggle="modal" data-target="#<?php echo $cli_param['id']; ?>">
                                    <i class="glyphicon glyphicon-pencil"></i>
                                    Editar
                                </button>
                                <button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#del_<?php echo $cli_param['id']; ?>">
                                    <i class="glyphicon glyphicon-remove"></i>
                                    Eliminar
                                </button>
                            </td>
                            </tr>

                            <!-- MODAL ACTUALIZAR REFERENCIA-->
                            <div id="<?php echo $cli_param['id']; ?>" class="modal fade" role="dialog">
                                <div class="modal-dialog">
                                    Modal content
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Actualizar Referencia de Indicador</h4> </div>
                                        <div class="modal-body">
                                            <form class="cliente" name="contact" enctype="multipart/form-data" id="indicador_upref_<?php echo $cli_param['id']?>" method="post" action="crear_parametro.php">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <input type="text" name="referencia" class="form-control" placeholder="Referencia" value="<?php echo $cli_param['referencia'] ?>">
                                                        </div>
                                                        <div class="form-group">
                                                            <?php if($cli_param['naturaleza']=='hh:mm'){$class_valor="valord"; $event="";}
                                                            if($cli_param['naturaleza'] == '$' || $cli_param['naturaleza'] == 'nn' || $cli_param['naturaleza'] == '%' ) {
                                                                $event = 'return isNumberKey(this,event)';
                                                                $class_valor = "valor_num"; } ?>
                                                            <input type="text" name="valor" class="form-control <?php echo $class_valor;?>"
                                                                   onkeypress="<?php echo $event;?>"
                                                                   placeholder="Valor en <?php echo $cli_param['naturaleza'];?>" value="<?php echo $valor; ?>">
                                                        </div>
                                                    </div>

                                                    <?php $fechaind_upref = date('m/d/Y',strtotime($cli_param['fecha'])); ?>

                                                    <div class="col-md-6">
                                                        <label>Fecha de referencia</label>
                                                        <div class="date_upref_indicador" style="font-size:10px;"></div>
                                                    </div>

                                                </div>
                                        </div>
                                        <div class="modal-footer">
                                            <div class="col-md-6 text-left col-md-offset-8">
                                                <input type="hidden" name="fecha" id="fechaind_upref_<?php echo $cli_param['id'];?>" value="<?php echo $fechaind_upref;?>"/>
                                                <input type="hidden" name="id_referencia" value="<?php echo $cli_param['id']?>"/>
                                                <input type="hidden" name="id_parametro" value="<?php echo $cli_param['id_parametro']?>"/>
                                                <input type="hidden" name="tipo" value="<?php echo $cli_param['naturaleza']?>"/>
                                                <input type="hidden" name="cliente" value="<?php echo $cliente ?>"/>
                                                <button class="btn btn-primary" type="submit" value="Crear" name="up_ref"><i class="glyphicon glyphicon-check"></i> Guardar</button>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- MODAL ELIMINAR REFERENCIA-->
                            <div id="del_<?php echo $cli_param['id']; ?>" class="modal fade" role="dialog">
                                <div class="modal-dialog">
                                    Modal content
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Eliminar Referencia</h4> </div>
                                        <div class="modal-body">
                                            <form class="cliente" name="contact" enctype="multipart/form-data" id="parametro" method="post" action="crear_parametro.php">
                                                <div class="row">
                                                    <div class="col-md-12">

                                                        <p> ¿ Esta seguro que desea eliminar la referencia <?php echo $cli_param['referencia'] ?>? </p>
                                                    </div>
                                                </div>
                                        </div>
                                        <div class="modal-footer">
                                            <div class="col-md-6 text-left col-md-offset-8">
                                                <input type="hidden" name="id_referencia" value="<?php echo $cli_param['id']?>"/>
                                                <input type="hidden" name="id_parametro" value="<?php echo $cli_param['id_parametro']?>"/>
                                                <input type="hidden" name="cliente" value="<?php echo $cliente ?>"/>
                                                <button class="btn btn-primary" type="submit" value="Crear" name="del_ref"><i class="glyphicon glyphicon-remove"></i> Eliminar</button>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <?php

                            $ref_idusuario = $cli_param['id_usuario'];
                            $sqlref_usuario = mysqli_query($conn,"select nombre from usuario where id_usuario='$ref_idusuario'");
                            $nameref = mysqli_fetch_assoc($sqlref_usuario);
                            $fecharef= $cli_param['fecha'];
                            $horaref= $cli_param['hora'];

                        }
                        echo '</tbody>';
                        echo '<tbody id="referencia_asc" class="hidden">';
                        // orden acendente referencia
                        foreach ($tabless as $clave => $fila) {
                            $referencias[$clave] = $fila['referencia'];
                            $fechas[$clave] = $fila['fecha'];
                        }
                        $referencias  = array_column($tabless, 'referencia');
                        $fechas = array_column($tabless, 'fecha');
                        array_multisort($referencias, SORT_ASC, $fechas, SORT_ASC, $tabless);
                        $sub = 0;
                        foreach($tabless as $cli_param){

                            $nombre_ref = 'modal_param_'.$cli_param['referencia'];
                            echo '<tr id="ind_'.$cli_param['id'].'"><td>'.$cli_param['referencia'].'</td>';

                            //aplicacion de mascara
                            if($cli_param['naturaleza'] == "hh:mm"){
                                $valor = $cli_param['valor'];
                                echo '<td>'.$cli_param['valor'].'</td>';
                            }
                            if($cli_param['naturaleza'] == "$"){
                                //setlocale(LC_MONETARY, 'en_US');
                                $nv  = $cli_param['valor'];
                                $valor = $nv;
                                echo '<td>$ '.number_format((float)$nv, 2, '.', ',').'</td>';
                                $sub = $sub + $cli_param['valor'];
                                if($active) {
                                    echo '<td>$ '.number_format((float)$sub, 2, '.', ',').'</td>';
                                }

                            }
                            if($cli_param['naturaleza'] == "nn"){
                                $valor = number_format((float)$cli_param['valor'], 2, '.', ',');
                                echo '<td>'.number_format((float)$cli_param['valor'], 2, '.', ',').'</td>';
                            }
                            if($cli_param['naturaleza'] == "%"){
                                $valor = number_format((float)$cli_param['valor'], 2, '.', ',');
                                echo '<td>'.number_format((float)$cli_param['valor'], 2, '.', ',').'%</td>';
                            }
                            ?>

                            <td><?php echo date_format(date_create($cli_param['fecha']),'d/m/Y');?></td>

                            <td>
                                <button class="btn btn-primary btn-xs setupdate" id="<?php echo $cli_param['id'];?>" data-toggle="modal" data-target="#<?php echo $cli_param['id']; ?>">
                                    <i class="glyphicon glyphicon-pencil"></i>
                                    Editar
                                </button>
                                <button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#del_<?php echo $cli_param['id']; ?>">
                                    <i class="glyphicon glyphicon-remove"></i>
                                    Eliminar
                                </button>
                            </td>
                            </tr>

                            <!-- MODAL ACTUALIZAR REFERENCIA-->
                            <div id="<?php echo $cli_param['id']; ?>" class="modal fade" role="dialog">
                                <div class="modal-dialog">
                                    Modal content
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Actualizar Referencia de Indicador</h4> </div>
                                        <div class="modal-body">
                                            <form class="cliente" name="contact" enctype="multipart/form-data" id="indicador_upref_<?php echo $cli_param['id']?>" method="post" action="crear_parametro.php">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <input type="text" name="referencia" class="form-control" placeholder="Referencia" value="<?php echo $cli_param['referencia'] ?>">
                                                        </div>
                                                        <div class="form-group">
                                                            <?php if($cli_param['naturaleza']=='hh:mm'){$class_valor="valord"; $event="";}
                                                            if($cli_param['naturaleza'] == '$' || $cli_param['naturaleza'] == 'nn' || $cli_param['naturaleza'] == '%' ) {
                                                                $event = 'return isNumberKey(this,event)';
                                                                $class_valor = "valor_num"; } ?>
                                                            <input type="text" name="valor" class="form-control <?php echo $class_valor;?>"
                                                                   onkeypress="<?php echo $event;?>"
                                                                   placeholder="Valor en <?php echo $cli_param['naturaleza'];?>" value="<?php echo $valor; ?>">
                                                        </div>
                                                    </div>

                                                    <?php $fechaind_upref = date('m/d/Y',strtotime($cli_param['fecha'])); ?>

                                                    <div class="col-md-6">
                                                        <label>Fecha de referencia</label>
                                                        <div class="date_upref_indicador" style="font-size:10px;"></div>
                                                    </div>

                                                </div>
                                        </div>
                                        <div class="modal-footer">
                                            <div class="col-md-6 text-left col-md-offset-8">
                                                <input type="hidden" name="fecha" id="fechaind_upref_<?php echo $cli_param['id'];?>" value="<?php echo $fechaind_upref;?>"/>
                                                <input type="hidden" name="id_referencia" value="<?php echo $cli_param['id']?>"/>
                                                <input type="hidden" name="id_parametro" value="<?php echo $cli_param['id_parametro']?>"/>
                                                <input type="hidden" name="tipo" value="<?php echo $cli_param['naturaleza']?>"/>
                                                <input type="hidden" name="cliente" value="<?php echo $cliente ?>"/>
                                                <button class="btn btn-primary" type="submit" value="Crear" name="up_ref"><i class="glyphicon glyphicon-check"></i> Guardar</button>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- MODAL ELIMINAR REFERENCIA-->
                            <div id="del_<?php echo $cli_param['id']; ?>" class="modal fade" role="dialog">
                                <div class="modal-dialog">
                                    Modal content
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Eliminar Referencia</h4> </div>
                                        <div class="modal-body">
                                            <form class="cliente" name="contact" enctype="multipart/form-data" id="parametro" method="post" action="crear_parametro.php">
                                                <div class="row">
                                                    <div class="col-md-12">

                                                        <p> ¿ Esta seguro que desea eliminar la referencia <?php echo $cli_param['referencia'] ?>? </p>
                                                    </div>
                                                </div>
                                        </div>
                                        <div class="modal-footer">
                                            <div class="col-md-6 text-left col-md-offset-8">
                                                <input type="hidden" name="id_referencia" value="<?php echo $cli_param['id']?>"/>
                                                <input type="hidden" name="id_parametro" value="<?php echo $cli_param['id_parametro']?>"/>
                                                <input type="hidden" name="cliente" value="<?php echo $cliente ?>"/>
                                                <button class="btn btn-primary" type="submit" value="Crear" name="del_ref"><i class="glyphicon glyphicon-remove"></i> Eliminar</button>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <?php

                            $ref_idusuario = $cli_param['id_usuario'];
                            $sqlref_usuario = mysqli_query($conn,"select nombre from usuario where id_usuario='$ref_idusuario'");
                            $nameref = mysqli_fetch_assoc($sqlref_usuario);
                            $fecharef= $cli_param['fecha'];
                            $horaref= $cli_param['hora'];

                        }
                        echo '</tbody>';



					if(isset($nameref)){
						echo '<tr cellspacing="6"><td><b>Modificado Por</b></td><td><b>Fecha</b></td><td><b>Hora</b></td></tr>';
						echo '<tr><td>'.$username.'</td>';
					}
					if(isset($fecharef)){
						echo '<td>'.date_format(date_create($fecha),'d/m/Y').'</td>';
					}
					if(isset($horaref)){
						echo '<td>'.$hora.'</td>';
					}
					else{
						echo '<tr><td> </td></tr>';
					}


					echo '<tr><td colspan="2"><button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal_param_up"><i class="glyphicon glyphicon-pencil"></i> Agregar Referencia</button>&nbsp;';
					echo '<form action="exportar.php" method="post">';
					echo '<input type="hidden" name="cliente" value="'.$nombre_cli.'">';
					echo '<input type="hidden" name="razon" value="'.$razon_cli.'">';
					echo '<input type="hidden" name="empresa" value="'.$empresa_usuario.'">';
					echo '<input type="hidden" name="nombre" value="'.$nom_param.'">';
					echo '<input type="hidden" name="usuario" value="'.$id_usuario.'">';
					echo '<input type="hidden" name="nat" value="'.$nat.'">';
					echo '<input type="hidden" name="active" value="'.$active.'">';
					$tbl_excel_ser1 = base64_encode(serialize($tbl_excel1));
					$tbl_excel_ser2 = base64_encode(serialize($tbl_excel2));
					$tbl_excel_ser3 = base64_encode(serialize($tbl_excel3));
					$tbl_excel_ser4 = base64_encode(serialize($tbl_excel4));
					echo '<input type="hidden" name="excel1" value="'.$tbl_excel_ser1.'">';
					echo '<input type="hidden" name="excel2" value="'.$tbl_excel_ser2.'">';
					echo '<input type="hidden" name="excel3" value="'.$tbl_excel_ser3.'">';
					echo '<input type="hidden" name="excel4" value="'.$tbl_excel_ser4.'">';
					echo '<input type="hidden" name="active" value="'.$active.'">';
					echo '<td><button type="submit" name="exportar" class="btn btn-success btn-xs"> <i class="glyphicon glyphicon-export"></i> Exportar</button></td></tr></form>';
					echo '</tfoot></table>';
					}

					//fin de indicadores para cliente
					else {

					$a_excel1 = array();
					$a_excel2 = array();
					$a_excel3 = array();
					$a_excel4 = array();
					$a_excel5 = array();
					$a_excel6 = array();
					$a_excel7 = array();

					// PANEL DE CONTROL INDICADORES

					echo '<table class="table table-hover table-responsive">
                        <tbody>
                        <tr>
                            <td><b>Indicador </b>'.$nom_param.'</td>
                            <td><b>Naturaleza </b> '.$nat.'</td>
                            <td>';
                            if($tipo_usuario != 2 ){
                                echo '<button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal_param_up2"><i class="glyphicon glyphicon-pencil"></i> Editar</button>&nbsp;';
                                $sql18d = mysqli_query($conn,"select count(*) from referencias where id_parametro='$id_param'");
                                $rsql18d = mysqli_fetch_assoc($sql18d);
                                $c_param = $rsql18d['count(*)'];
                                if ($c_param == 0){
                                    echo '<button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal_param_del"><i class="glyphicon glyphicon-remove"></i> Eliminar</button></td></tr>';
                                }
                            }
                        echo '</td>';
                    echo '</tr>';
					echo '<tr>
                            <td><b>Generado Por </b> '.$g_user.'</td>
                            <td><b>Fecha </b> '.date_format(date_create($fecha),'d/m/Y').'</td>
                            <td><b>Hora </b> '.$hora.'</td>
                            </tr>
                            </tbody>
                            </table>
                            <table class="table table-hover">';
//					echo '<tr></tr>';

				if($tipo_usuario != 2 ){

//					echo '<tr><td colspan="3"><button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal_param_up2"><i class="glyphicon glyphicon-pencil"></i> Editar</button>&nbsp;';
//					$sql18d = mysqli_query($conn,"select count(*) from referencias where id_parametro='$id_param'");
//					$rsql18d = mysqli_fetch_assoc($sql18d);
//					$c_param = $rsql18d['count(*)'];
//					if ($c_param == 0){
//					echo '<button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal_param_del"><i class="glyphicon glyphicon-remove"></i> Eliminar</button></td></tr>';
//					}
				}

					echo '<tr><td></td></tr>';
					echo '<tr><td><select id="list_client1" class="form-control">';
					echo '<option value="">Todos</option>';

					$flgrupo = mysqli_query($conn, "SELECT * FROM grupos WHERE id_empresa = '$id_empresa'");

					while($rflgrupo = mysqli_fetch_array($flgrupo)){

					if(!empty($rflgrupo['nombre_grupo'])){
						if($option_grupo == $rflgrupo['id']){
							echo '<option selected value="'.$rflgrupo['id'].'">'.$rflgrupo['nombre_grupo'].'</option>';
						}else{
							echo '<option value="'.$rflgrupo['id'].'">'.$rflgrupo['nombre_grupo'].'</option>';
						}
					  }

					}


					echo '</select></td>';


					echo '<td><select id="list_client2" class="form-control"><option selected value="">Todos</option>';

					$flestatus = mysqli_query($conn, "SELECT * FROM estatus WHERE id_empresa = '$id_empresa' AND id_grupo='$option_grupo'");

					while($rflestatus = mysqli_fetch_array($flestatus)){

					if(!empty($rflestatus['nombre_estatus'])){
						if($option_estatus == $rflestatus['id']){
							echo '<option selected value="'.$rflestatus['id'].'">'.$rflestatus['nombre_estatus'].'</option>';
						}else{
							echo '<option value="'.$rflestatus['id'].'">'.$rflestatus['nombre_estatus'].'</option>';
						}
					  }

					}


					echo '</select></td>';
					echo '<td><input type="text" class="form-control" name="ifdesde" id="ifdesde" value="'.$fecha_desde.'" placeholder="Fecha desde"></td>';
					echo '<td><input type="text" class="form-control" name="ifhasta" id="ifhasta" value="'.$fecha_hasta.'" placeholder="Fecha hasta"></td>';
					echo '</tr>';
					echo '<input type="hidden" id="param_number" value="'.$file.'"/></td></tr></table>';
					echo '<table class="table table-hover table-list-cli">
					<thead><tr><th class="tvalor">Cliente</th><th class="tvalor">Status</th><th class="tvalor">Valor + Bajo</th><th class="tvalor">Valor + Alto</th><th class="tprom">Promedio</th><th class="tvalor">Suma</th></tr></thead><tbody>';

					function imprime_cliente_indicador($cli_list_param,$file,$conn,$id_empresa,$option_grupo,$option_estatus,$option_fecha,$hpadre){


						$id_c = $cli_list_param['id_cliente'];

						 $a_excel1[]= $cli_list_param['nombre'];

						 echo '<tr>';

						 if($hpadre !=0){

							echo '<td class="text-center">';

						 }else{
							echo '<td>';
						 }

						 echo '<a class="btn btn-xs btn-info" href="brm.php?cl='.$id_c.'&carp=param&file='.$file.'">'.$cli_list_param['nombre'].'</a></td>';


						 if($cli_list_param['estatus'] == "si" || $cli_list_param['estatus'] == "Si"){
						 $a_excel2[]= 'Activo';
						 echo '<td>Activo</td>';}

						 else if($cli_list_param['estatus'] == "no" || $cli_list_param['estatus'] == "No"){
                         $a_excel2[]= 'Inactivo';
						 echo '<td>Inactivo</td>';}

						 else {

								$iestatus="";

								$list_estatus = $cli_list_param['estatus'];
								$getcstatus = mysqli_query($conn, "SELECT * FROM estatus WHERE id = '$list_estatus' AND id_empresa='$id_empresa'");
								while($rgetcstatus = mysqli_fetch_array($getcstatus)){
									$iestatus =$rgetcstatus['nombre_estatus'];
									$a_excel2[]= $iestatus;
								}
								echo '<td>'.$iestatus.'</td>';
						 }

						 $a_excel3 []= $cli_list_param['razon'];
						/* echo '<td>'.$cli_list_param['razon'].'</td>';*/


							//consulta el minimo y maximo valor de referecia
							$valor = array();
							$sql30a = mysqli_query($conn,"select valor, naturaleza from referencias t1 where id_cliente='$id_c' and id_parametro='$file' $option_fecha");
							while($refer =mysqli_fetch_array($sql30a)){

							 $nat_ref = $refer['naturaleza'];

							 //procesa el arreglo de acuerdo a la naturaleza

							 if($nat_ref == "hh:mm"){
							 $valor_t = str_replace(':','.',$refer['valor']);
							 $valor [] = (float)$valor_t;
									}

							 else {
								 $valor_t = $refer['valor'];
								 $valor [] = (float)$valor_t;
								}
							}

							 //imprime los valores de minimo y maximo segun la naturaleza
							 if($nat_ref == "hh:mm"){
							 $min_val = number_format(min($valor),2);
							 $max_val = number_format(max($valor),2);
							 $a_excel4 [] = str_replace('.',':',$min_val);
							 echo '<td class="nat_hora">'.str_replace('.',':',$min_val).'</td>';
							 $a_excel5 [] = str_replace('.',':',$max_val);
							 echo '<td class="nat_hora">'.str_replace('.',':',$max_val).'</td>';
							 }

							 else if($nat_ref == "$"){
							 $a_excel4[]=min($valor);
						     echo '<td class="nat_moneda">$'.number_format(min($valor), 2, '.', ',').'</td>';
							 $a_excel5[]=max($valor);
						     echo '<td class="nat_moneda">$'.number_format(max($valor), 2, '.', ',').'</td>';
							 }

							 else if($nat_ref == "%"){
						     $a_excel4[]=number_format(min($valor), 2, '.', ',').'%';
						     echo '<td class="nat_porcentaje">'.number_format(min($valor), 2, '.', ',').'%</td>';
							 $a_excel5[]=number_format(max($valor), 2, '.', ',').'%';
						     echo '<td class="nat_porcentaje">'.number_format(max($valor), 2, '.', ',').'%</td>';
							 }

							 else {
							 $a_excel4[]= number_format(min($valor), 2, '.', ',');
						     echo '<td class="nat_numero">'.number_format(min($valor), 2, '.', ',').'</td>';
							 $a_excel5[]= number_format(max($valor), 2, '.', ',');
						     echo '<td class="nat_numero">'.number_format(max($valor), 2, '.', ',').'</td>';
							 }


							//consulta el promedio de valores
							$sql30b = mysqli_query($conn,"select valor, naturaleza from referencias t1 where id_cliente='$id_c' and id_parametro='$file' $option_fecha");
							$qty = 0;

							//$totaltime = 0;
							$totaltime = array();
							while($prom=mysqli_fetch_array($sql30b)){
							$n_prom = mysqli_num_rows($sql30b);

							if($prom['naturaleza'] == 'hh:mm'){
									$tiempo = str_replace(':','.',$prom['valor']);
									//$totaltime += (float)$tiempo;
									$totaltime[] = $prom['valor'];
									$b_horas = $prom['naturaleza'];
								}

							else {
									$qty += $prom['valor'];
									$b_numero = $prom['naturaleza'];
									}


							}

							if(isset($b_numero)){

							if($b_numero == "$"){
							$a_excel6[]=(float)$qty/$n_prom;
							echo '<td class="nat_moneda">$'.number_format((float)$qty/$n_prom, 2, '.', ',').'</td>';
							$a_excel7[]=(float)$qty;
							echo '<td class="nat_moneda">$'.number_format((float)$qty, 2, '.', ',').'</td>';
								}
							else if($b_numero == "%"){
							$a_excel6[]=number_format((float)$qty/$n_prom, 2, '.', ',').'%';
							echo '<td class="nat_porcentaje">'.number_format((float)$qty/$n_prom, 2, '.', ',').'%</td>';
							$a_excel7[]= number_format((float)$qty, 2, '.', ',').'%';
							echo '<td class="nat_porcentaje">'.number_format((float)$qty, 2, '.', ',').'%</td>';

								}
							else{
							$a_excel6[]=number_format((float)$qty/$n_prom, 2, '.', ',');
							echo '<td class="nat_numero">'.number_format((float)$qty/$n_prom, 2, '.', ',').'</td>';
							$a_excel7[]= number_format((float)$qty, 2, '.', ',');
							echo '<td class="nat_numero">'.number_format((float)$qty, 2, '.', ',').'</td>';
								}
							}

						    if(isset($b_horas)){
								$minutes = 0;
								foreach ($totaltime as $time) {
								list($hour, $minute) = explode(':', $time);
								$minutes += $hour * 60;
                                $minutes += $minute;
								}

								$hours = floor($minutes / 60);
								$minutes -= $hours * 60;

								$total_hour = sprintf('%02d:%02d', $hours, $minutes);
								$t1= str_replace(':','.',$total_hour);
								$t2= (float)$t1/$n_prom;

								$total_hora = sprintf('%02d:%02d', (int) $t1, fmod($t1, 1) * 60);
								$prom_hora =  sprintf('%02d:%02d', (int) $t2, fmod($t2, 1) * 60);

							/*	$total_hora=  $totaltime/$n_prom;
								$total_hora_f = number_format($total_hora,2);
								$prom_hora = str_replace('.',':',$total_hora_f);*/
								$a_excel6[]= $prom_hora;
								$a_excel7[]= $total_hora;
								echo '<td class="nat_hora">'.$prom_hora.'</td>';
								echo '<td class="nat_hora">'.$total_hora.'</td>';

							}

						}


						while($cli_list_param=mysqli_fetch_array($sql30)){
							$acli_list_param[] = $cli_list_param;

						}

						if(isset($acli_list_param)){


							$id = array_map(function ($item) {return $item["id_cliente"];}, $acli_list_param);
							$lists = array();
							$parent = array_filter($acli_list_param, function ($item){return $item['cliente_padre'] == 0;});
							$nolist_parent = array_filter($acli_list_param, function ($item){return $item['cliente_padre'] != 0;});


							$all_parent =array_merge($parent,$nolist_parent);


							$x=0;

							foreach ($parent as $value)
							{
								$lists[] = $value;
								$children = array_filter($acli_list_param, function ($item) use($value) {return $item['cliente_padre'] == $value['id_cliente'];});

								if(!empty($children)){
									foreach($children as $kids)
									{
										$lists[]  = $kids ;
									}


							}	//echo $x++;

							}

							$slist="";

							foreach ($nolist_parent as $nvalue)
							{
								//echo "<br>".$nvalue['id_cliente'];

							 foreach ($lists as $val) {

								//echo '<br>lista id'.$val['id_cliente'];

							   if ($val['id_cliente'] === $nvalue['id_cliente']) {
									//echo "si esta";
									$slist = 1;
									break;
								}else{
									//echo "NO esta";
									$slist = 0;
									//$lists[] = $nvalue;
								}

							}

								if($slist==0){
									$lists[]=$nvalue;
								}

							}

						}


						if(isset($lists)){
							$check_lists = $lists;
								foreach($check_lists as $lists_param){

								if($lists_param['cliente_padre']!=0){
									$pariente = 1;
								}
								 else{
									$pariente = 0;
								}

								imprime_cliente_indicador($lists_param,$file,$conn,$id_empresa,$option_grupo,$option_estatus,$option_fecha,$pariente);
							}
						}

					echo '</tbody><tfoot>';

					echo '<form action="exportar.php" method="post">';
					echo '<input type="hidden" name="empresa" value="'.$empresa_usuario.'">';
					echo '<input type="hidden" name="id_excel_empresa" value="'.$id_empresa.'">';
					echo '<input type="hidden" name="nombre" value="'.$nom_param.'">';
					echo '<input type="hidden" name="usuario" value="'.$id_usuario.'">';
					echo '<input type="hidden" name="nat" value="'.$nat.'">';
					echo '<input type="hidden" name="file" value="'.$file.'">';
					if(isset($lists)){

						echo '<input type="hidden" name="queryexcel" value="'.base64_encode(serialize($lists)).'">';
					  if(isset($_GET['option_grupo'])){
							echo '<input type="hidden" name="filtro1" value="'.$_GET['option_grupo'].'"/>';
							echo '<input type="hidden" name="filtro2" value="'.$_GET['option_estatus'].'"/>';
							echo '<input type="hidden" name="filtro3" value="'.$_GET['fecha_desde'].'"/>';
							echo '<input type="hidden" name="filtro4" value="'.$_GET['fecha_hasta'].'"/>';
					  }
						echo '<tr><td colspan="5"></td><td><button type="submit" name="exportar_listado" class="btn btn-success btn-xs"> <i class="glyphicon glyphicon-export"></i> Exportar</button></td></tr></form>';

						}
					}
		            echo '</tfoot></table>';
				}

				   //CARPETA  CONTROLES
					else if (isset($carpeta)&& isset($file)&& $carpeta=="compl"){
					    $fec = '';
					    $hor = '';
					    $userna='';
                   	echo '<table class="table table-hover table-compl">';

					while($cli_compl=mysqli_fetch_array($sql31)){

					$nom_param = $cli_compl['nombre_complemento'];
					$fec = $cli_compl['fecha'];
					$hor = $cli_compl['hora'];
					$id_com = $cli_compl['id_complemento'];
					$id_user = $cli_compl['id_usuario'];
					$sql31a = mysqli_query($conn,"select nombre from usuario where id_usuario='$id_user'");
					$rsql31a = mysqli_fetch_assoc($sql31a);
					$g_user = $rsql31a['nombre'];
					$userna = $rsql31a['nombre'];
					$hora = $cli_compl['hora'];
					$fecha = $cli_compl['fecha'];
					//asegura la naturaleza fecha del complemento
					if($cli_compl['naturaleza']=="d/m/a"){$cli_compl['naturaleza']="dd/mm/aa";}
					$nat = $cli_compl['naturaleza'];
					$id_param = $cli_compl['id_complemento'];
					}

					//Control para cliente
					if(isset($cliente)){
					$sql31d = mysqli_query($conn,"select nombre from cliente where id_cliente='$cliente'");
					$rsql31d = mysqli_fetch_assoc($sql31d);
					$pcliente = $rsql31d['nombre'];
					$sql31dr = mysqli_query($conn,"select razon from cliente where id_cliente='$cliente'");
					$rsql31dr = mysqli_fetch_assoc($sql31dr);
					$rcliente = $rsql31dr['razon'];
                    echo '<thead><tr><td><b>Control</b>&nbsp;&nbsp;'.$nom_param.'</td><td><b>Naturaleza</b>&nbsp;&nbsp;'.$nat.'</td></tr>';
//                    echo '<tr></td><td></td><td></td></tr>';
					echo '<tr><th><b>Referencia</b></th><th><b>Valor</b></th><th><b>Fecha</b></th><th>Accion</th></tr></thead><tbody>';


					$nref_compl = mysqli_num_rows($sql31b);

					while($cli_compl=mysqli_fetch_array($sql31b)){
					$nombre_ref = 'modal_param_'.$cli_compl['referencia'];
					echo '<tr id="ind_'.$cli_compl['id'].'"><td>'.$cli_compl['referencia'].'</td>';

					//aplicacion de mascaras
					if($cli_compl['naturaleza'] == "hh:mm" || $cli_compl['naturaleza'] == "abc" )
					{
					$valor = $cli_compl['valor'];
					echo '<td class="cmpl_valor_td">'.$cli_compl['valor'].'</td>';
					}

					if($cli_compl['naturaleza'] == "d/m/a"){
					$valor = $cli_compl['valor'];
					echo '<td class="cmpl_valor_td">'.date("d/m/Y",strtotime($cli_compl['valor'])).'</td>';
					}

					if($cli_compl['naturaleza'] == "$"){
						//setlocale(LC_MONETARY, 'en_US');
						$nv  = $cli_compl['valor'];
						$valor = number_format((float)$nv, 2, '.', ',');
						echo '<td>$ '.number_format((float)$nv, 2, '.', ',').'</td>';
						}
					if($cli_compl['naturaleza'] == "nn"){
						$valor = number_format((float)$cli_compl['valor'], 2, '.', ',');
						echo '<td>'.number_format((float)$cli_compl['valor'], 2, '.', ',').'</td>';
						}
					if($cli_compl['naturaleza'] == "%"){
						$valor = number_format((float)$cli_compl['valor'], 2, '.', ',');
						echo '<td>'.number_format((float)$cli_compl['valor'], 2, '.', ',').'%</td>';
						}

					?>
					<td><?php echo date_format(date_create($cli_compl['fecha']),'d/m/Y');?></td>

					<td class="cmpl_accion_td"><button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#<?php echo $cli_compl['id']; ?>"><i class="glyphicon glyphicon-pencil"></i> Editar</button>
					  <button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#del_<?php echo $cli_compl['id'];; ?>"><i class="glyphicon glyphicon-remove"></i> Eliminar</button></td></tr>

					<!-- MODAL ACTUALIZAR REFERENCIA DE COMPLEMENTO-->
					<div id="<?php echo $cli_compl['id']; ?>" class="modal fade" role="dialog">
                    <div class="modal-dialog">
					<!-- Modal content-->
                    <div class="modal-content">
                    <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Actualizar Referencia de Control</h4> </div>
                    <div class="modal-body">
                    <form name="contact" enctype="multipart/form-data" class="referencia_compl" id="upref_control" method="post" action="accion_complemento.php">
		            <div class="row">
                        <input type="hidden" name="id_com" value="<?php echo $id_com; ?>">
					<div class="col-md-6">
		            <div class="form-group">
						<input type="text" name="referencia" class="form-control" placeholder="Referencia" value="<?php echo $cli_compl['referencia'] ?>">
		            </div>

	                <div class="form-group">

					<?php if($cli_compl['naturaleza'] == 'd/m/a') {
						$control_natf = 1;
					?>
					<label>Valor</label>
					<div class="datepicker_act_compl2" style="font-size:10px;"></div>

					<?php } else {

					 if($cli_compl['naturaleza']=='hh:mm'){$class_valor="valord";}

					 if($cli_compl['naturaleza']=='abc'){
								$class_valor="";?>
							<textarea rows="4" cols="60" maxlength="140" name="valor"  placeholder="Valor" class="textarea_cpl form-control <?php echo $class_valor;?>"><?php echo $valor;?></textarea>
						<?php	} else  {
							$class_valor="valor_num";?>
			        <input type="text" name="valor" class="form-control <?php echo $class_valor;?>"
					 onkeypress="<?php if ($cli_compl['naturaleza'] == '$' || $cli_compl['naturaleza'] == 'nn' || $cli_compl['naturaleza'] == '%' ) {echo 'return isNumberKey(this,event)';}?>"
	                 placeholder="Valor" value="<?php echo $valor;?>">

					<?php } } ?>

					</div>
					</div>

					<?php $fecha_upref = date('m/d/Y',strtotime($cli_compl['fecha'])); ?>

					<div class="col-md-6">
						<label>Fecha</label>
						<div id="date_upref_control" style="font-size:10px;"></div>
					</div>

					</div></div>
                    <div class="modal-footer">
	  	            <div class="col-md-6 text-left col-md-offset-8">
					<input type="hidden" name="id_referencia" value="<?php echo $cli_compl['id']?>"/>
					<input type="hidden" name="id_parametro" value="<?php echo $cli_compl['id_complemento']?>"/>
					<input type="hidden" name="tipo" value="<?php echo $cli_compl['naturaleza']?>"/>
					<input type="hidden" name="cliente" value="<?php echo $cliente ?>"/>
					<button class="btn btn-primary" type="submit" value="Crear" name="up_ref"><i class="glyphicon glyphicon-check"></i> Guardar</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
		            </form></div></div></div></div></div>


					<!-- MODAL ELIMINAR REFERENCIA-->
					<div id="del_<?php echo $cli_compl['id'];; ?>" class="modal fade" role="dialog">
                    <div class="modal-dialog">
					<!-- Modal content-->
                    <div class="modal-content">
                    <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Eliminar Referencia</h4> </div>
                    <div class="modal-body">
                    <form class="cliente" name="contact" enctype="multipart/form-data" method="post" action="accion_complemento.php">
		            <div class="row">
					<div class="col-md-12">

		             <p> ¿ Esta seguro que desea eliminar la referencia <?php echo $cli_compl['referencia'] ?>? </p>
				   </div>
				   </div></div>
                    <div class="modal-footer">
	  	            <div class="col-md-6 text-left col-md-offset-8">
					<input type="hidden" name="id_referencia" value="<?php echo $cli_compl['id']?>"/>
					<input type="hidden" name="id_parametro" value="<?php echo $cli_compl['id_complemento']?>"/>
					<input type="hidden" name="cliente" value="<?php echo $cliente ?>"/>
	                <button class="btn btn-primary" type="submit" value="Crear" name="del_ref"><i class="glyphicon glyphicon-remove"></i> Eliminar</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
		            </form></div></div></div></div></div>

		            <?php


					$ref_idusuario = $cli_compl['id_usuario'];
					$sqlref_usuario = mysqli_query($conn,"select nombre from usuario where id_usuario='$ref_idusuario'");
					$nameref = mysqli_fetch_assoc($sqlref_usuario);
					$fecharef = date_format(date_create($cli_compl['fecha']),'d/m/Y');
					$horaref = $cli_compl['hora'];

					}

					echo '</tbody><tfoot>';


					if(isset($nameref['nombre'])){
						echo '<tr><td><b>Modificado Por</b></td><td><b>Fecha</b></td><td><b>Hora</b></td></tr>';
						echo '<tr><td>'.$userna.'</td>';
					}
					if(isset($fecharef)){
						echo '<td>'.date_format(date_create($fec),'d/m/Y').'</td>';
					}
					if(isset($horaref)){
						echo '<td>'.$hor.'</td>';
					}
					else{
						echo '<tr><td></td></tr>';
					}


					//solo una referencia por complemento
					if(isset($nref_compl) && $nref_compl ==0){
					echo '<tr><td colspan="2"><button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal_compl_up"><i class="glyphicon glyphicon-pencil"></i> Agregar Referencia</button>&nbsp;';
					}
					echo '</tfoot></table>';
					}

					//fin de controles para cliente

					/*Panel de control controles*/
				else {

						$ac_excel1 = array();
						$ac_excel2 = array();
						$ac_excel3 = array();
						$ac_excel4 = array();
						$ac_excel5 = array();


						echo '<table class="table table-hover table-responsive">
                        <tbody>
                        <tr>
                            <td><b>Control </b>'.$nom_param.'</td>
                            <td><b>Naturaleza</b> '.$nat.'</td>
                            <td>';
						if($tipo_usuario != 2 ){
                           echo '<button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal_compl_up2"><i class="glyphicon glyphicon-pencil"></i> Editar</button>&nbsp;';
                            $sql31d = mysqli_query($conn,"select count(*) from referencias_complementos where id_complemento='$id_param'");
                            $rsql31d = mysqli_fetch_assoc($sql31d);
                            $c_param = $rsql31d['count(*)'];
                            if ($c_param == 0){
                            echo '<button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal_compl_del"><i class="glyphicon glyphicon-remove"></i> Eliminar</button>';
                                }
                        }
                    echo '</td></tr>';
					echo '<tr>
                            <td><b>Generado Por</b> '.$g_user.'</td>
                            <td><b>Fecha</b> '.date_format(date_create($fecha),'d/m/Y').'</td>
                            <td><b>Hora</b> '.$hora.'</td>
                            </tr>
                            </tbody>
                            </table>';
					echo '<table class="table table-hover table-responsive"><tbody>';
//					echo '<table class="table table-hover table-responsive"><tr><td><b>Control</b> '.$nom_param.'</td><td><b>Naturaleza</b> '.$nat.'</td></tr>';
//					echo '<tr></tr>';
//					echo '<tr><td><b>Generado Por </b> '.$g_user.'</td><td><b>Fecha </b> '.date_format(date_create($fecha),'d/m/Y').'</td><td><b>Hora </b> '.$hora.'</td></tr>';

				if($tipo_usuario != 2 ){

//				   echo '<tr><td colspan="3"><button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal_compl_up2"><i class="glyphicon glyphicon-pencil"></i> Editar</button>&nbsp;';
//					$sql31d = mysqli_query($conn,"select count(*) from referencias_complementos where id_complemento='$id_param'");
//					$rsql31d = mysqli_fetch_assoc($sql31d);
//					$c_param = $rsql31d['count(*)'];
//					if ($c_param == 0){
//					echo '<button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal_compl_del"><i class="glyphicon glyphicon-remove"></i> Eliminar</button></td></tr>';
//						}
				}
					echo '<tr><td></td></tr>';
					echo '<tr><td><select id="list_client_compl1" class="form-control">';
					echo '<option value="">Todos</option>';

					$complgrupo = mysqli_query($conn, "SELECT * FROM grupos WHERE id_empresa = '$id_empresa'");

					while($rcomplgrupo = mysqli_fetch_array($complgrupo)){

					if(!empty($rcomplgrupo['nombre_grupo'])){
						if($option_grupo == $rcomplgrupo['id']){
							echo '<option selected value="'.$rcomplgrupo['id'].'">'.$rcomplgrupo['nombre_grupo'].'</option>';
						}else{
							echo '<option value="'.$rcomplgrupo['id'].'">'.$rcomplgrupo['nombre_grupo'].'</option>';
						}
					  }

					}


					echo '</select></td>';


					echo '<td><select id="list_client_compl2" class="form-control"><option selected value="">Todos</option>';

					$complestatus = mysqli_query($conn, "SELECT * FROM estatus WHERE id_empresa = '$id_empresa' AND id_grupo='$option_grupo'");

					while($rcomplestatus = mysqli_fetch_array($complestatus)){

					if(!empty($rcomplestatus['nombre_estatus'])){
						if($option_estatus == $rcomplestatus['id']){
							echo '<option selected value="'.$rcomplestatus['id'].'">'.$rcomplestatus['nombre_estatus'].'</option>';
						}else{
							echo '<option value="'.$rcomplestatus['id'].'">'.$rcomplestatus['nombre_estatus'].'</option>';
						}
					  }

					}


					echo '</select></td>';
					echo '<td><input type="text" class="form-control" name="cfdesde" id="cfdesde" value="'.$fecha_desde.'" placeholder="Fecha desde"></td>';
					echo '<td><input type="text" class="form-control" name="cfhasta" id="cfhasta" value="'.$fecha_hasta.'" placeholder="Fecha hasta"></td>';
					echo '</tr>';
					echo '</tr><input type="hidden" id="compl_number" value="'.$file.'"/></td></tr></table>';
					echo '<table class="table table-hover table-list-cli">
					<thead><tr><th class="tvalor">Cliente</th><th class="tvalor">Status</th><th class="tvalor">Fecha</th><th class="tvalor">Referencia</th><th class="tvalor" data-sorter="shortDate" data-date-format="ddmmyyyy">Valor</th></tr></thead><tbody>';

					//listado de clientes para el control

					/*Funcion para imprimir listado de clientes control */
					function imprime_cliente_control($cli_list_compl,$file,$conn,$id_empresa,$hpadre){

						$id_c = $cli_list_compl['id_cliente'];

						 $ac_excel1[]= $cli_list_compl['nombre'];

						 echo '<tr>';

						 if($hpadre !=0){

							echo '<td class="text-center">';

						 }else{
							echo '<td>';
						 }
						 echo '<a class="btn btn-xs btn-info" href="brm.php?cl='.$id_c.'&carp=compl&file='.$file.'">'.$cli_list_compl['nombre'].'</a></td>';

						 if($cli_list_compl['estatus'] == "si" || $cli_list_compl['estatus'] == "Si"){
						 $ac_excel2[]= 'Activo';
						 echo '<td>Activo</td>';}

						 elseif($cli_list_compl['estatus'] == "no" || $cli_list_compl['estatus'] == "No"){
                         $ac_excel2[]= 'Inactivo';
						 echo '<td>Inactivo</td>';}

						 else {
								$cestatus = "";

								$list_estatus = $cli_list_compl['estatus'];
								$getcstatus = mysqli_query($conn, "SELECT * FROM estatus WHERE id = '$list_estatus' AND id_empresa='$id_empresa'");
								while($rgetcstatus = mysqli_fetch_array($getcstatus)){
									$cestatus = $rgetcstatus['nombre_estatus'];
									$a_excel2[]= $cestatus;
								}

								echo '<td>'.$cestatus.'</td>';
						 }

						 $ac_excel3 []= $cli_list_compl['fecha'];
						 echo '<td>'.date_format(date_create($cli_list_compl['fecha']),"d/m/Y").'</td>';

						 $ac_excel4 []= $cli_list_compl['referencia'];
						 echo '<td>'.$cli_list_compl['referencia'].'</td>';

							//imprime referencia de complemento segun su naturaleza
							$c_nat=$cli_list_compl['naturaleza'];
							$com_valor  = $cli_list_compl['valor'];

							if($c_nat == "$"){
							$ac_excel5[]='$ '.number_format((float)$com_valor, 2, '.', ',');
							echo '<td class="nat_moneda">$'.number_format((float)$com_valor, 2, '.', ',').'</td>';
								}

							else if($c_nat == "%"){
							$ac_excel5[]=number_format((float)$com_valor, 2, '.', ',').'%';
							echo '<td class="nat_porcentaje">'.number_format((float)$com_valor, 2, '.', ',').'%</td>';
								}

							else if($c_nat == "nn"){
							$ac_excel5[]=number_format((float)$com_valor, 2, '.', ',');
							echo '<td class="nat_numero">'.number_format((float)$com_valor, 2, '.', ',').'</td>';
							}

							else if($c_nat == "d/m/a"){
								$ac_excel5[]=date("d/m/Y",strtotime($com_valor));
								echo '<td>'.date("d/m/Y",strtotime($com_valor)).'</td>';
							}

							else{
							$ac_excel5[]= $com_valor;
							echo '<td class="nat_hora">'.$com_valor.'</td>';
								}


					}


						while($cli_list_compl=mysqli_fetch_array($sql32)){
							$acli_list_compl[] = $cli_list_compl;

						}

						if(isset($acli_list_compl)){


							$id = array_map(function ($item) {return $item["id_cliente"];}, $acli_list_compl);
							$lists = array();
							$parent = array_filter($acli_list_compl, function ($item){return $item['cliente_padre'] == 0;});
							$nolist_parent = array_filter($acli_list_compl, function ($item){return $item['cliente_padre'] != 0;});

							foreach ($parent as $value)
							{
								$lists[] = $value;
								$children = array_filter($acli_list_compl, function ($item) use($value) {return $item['cliente_padre'] == $value['id_cliente'];});

								if(!empty($children)){
									foreach($children as $kids)
									{
										$lists[]  = $kids ;
									}


							}	//echo $x++;

							}

							$slist="";

							foreach ($nolist_parent as $nvalue)
							{
								//echo "<br>".$nvalue['id_cliente'];

							 foreach ($lists as $val) {

								//echo '<br>lista id'.$val['id_cliente'];

							   if ($val['id_cliente'] === $nvalue['id_cliente']) {
									//echo "si esta";
									$slist = 1;
									break;
								}else{
									//echo "NO esta";
									$slist = 0;
									//$lists[] = $nvalue;
								}

							}

								if($slist==0){
									$lists[]=$nvalue;
								}

							}

						}


						if(isset($lists)){
							$check_lists = $lists;
								foreach($check_lists as $lists_control){

								if($lists_control['cliente_padre']!=0){
									$pariente = 1;
								}
								 else{
									$pariente = 0;
								}

								imprime_cliente_control($lists_control,$file,$conn,$id_empresa,$pariente);
							}
						}




					echo '</tbody><tfoot>';

					echo '<form action="exportar.php" method="post">';
					echo '<input type="hidden" name="empresa" value="'.$empresa_usuario.'">';
					echo '<input type="hidden" name="id_excel_empresa" value="'.$id_empresa.'">';
					echo '<input type="hidden" name="nombre" value="'.$nom_param.'">';
					echo '<input type="hidden" name="usuario" value="'.$id_usuario.'">';
					echo '<input type="hidden" name="nat" value="'.$nat.'">';

					if(isset($lists)){
						echo '<input type="hidden" name="query_excel" value="'.base64_encode(serialize($lists)).'">';

						  if(isset($_GET['option_grupo'])){
							echo '<input type="hidden" name="filtro1" value="'.$_GET['option_grupo'].'"/>';
							echo '<input type="hidden" name="filtro2" value="'.$_GET['option_estatus'].'"/>';
							echo '<input type="hidden" name="filtro3" value="'.$_GET['fecha_desde'].'"/>';
							echo '<input type="hidden" name="filtro4" value="'.$_GET['fecha_hasta'].'"/>';
					  }

						echo '<tr><td colspan="5"></td><td><button type="submit" name="exportar_listado_compl" class="btn btn-success btn-xs"> <i class="glyphicon glyphicon-export"></i> Exportar</button></td></tr></form>';
					}

					  echo '</tfoot></table>';
				}

			}

				   //CARPETA ALERTAS SELECCIONADA
                   	else if (isset($carpeta)&& isset($file)&& $carpeta=="ale"){
					echo '<table class="table table-hover table-alert">';
                    while($cli21=mysqli_fetch_array($sql21)){ //alertas de mensajes
				    echo '<tr><td><b>Asunto</b></td><td><b>Estatus</b></td><td><b>Cliente</b></td></tr>';
					echo '<tr><td>'.$cli21['asunto'].'</td>';
					if($cli21['status']=="si"){echo '<td>Enviado</td>';}
					else if ($cli21['status']=="no"){echo '<td>No enviado</td>';}
					$id_des = $cli21['id_cliente'];
					$sql21a = mysqli_query($conn,"select nombre from cliente where id_cliente='$id_des'");
					$rsql21a = mysqli_fetch_assoc($sql21a);
					$des_cli = $rsql21a['nombre'];
					echo '<td><a href="brm.php?cl='.$id_des.'&carp=cli&file=datos" class="btn btn-xs btn-info">'.$des_cli.'</a></td></tr>';
					echo '<tr><td colspan="3"><b>Contenido</b></td></tr>';
					echo '<tr><td colspan="3">'.$cli21['contenido'].'</td></tr>';
					$id_user = $cli21['id_usuario'];
					$sql21b = mysqli_query($conn,"select nombre_completo from usuario where id_usuario='$id_user'");
					$rsql21b = mysqli_fetch_assoc($sql21b);
					$g_user = $rsql21b['nombre_completo'];
					echo '<tr><td><b>Generado Por</b></td><td>'.$g_user.'</td><td><b>Fecha</b></td><td>'.date_format(date_create($cli21['fecha']),'d/m/Y').'</td><td><b>Hora</b></td><td>'.$cli21['hora'].'</td></tr>';
					echo '</table>';

					echo '<div class="col-md-6">
					<form action="crear_mensaje.php" method="post">';

				   if($tipo_usuario != 2 ){
					echo '<button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal_msj_del"> <i class="glyphicon glyphicon-remove"></i> Eliminar</button>&nbsp;';
				   }

				   if($tipo_usuario == 2 && $id_user != $id_usuario){
						echo "";

				   }else{

					   echo '<button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal_msj_up"><i class="glyphicon glyphicon-pencil"></i> Editar</button>&nbsp;';
					   echo '<button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal_enviar"> <i class="glyphicon glyphicon-ok"></i> Enviar</button>&nbsp;';
					   echo '<input type="hidden" name="id_mensaje" value="'.$cli21['id_mensaje'].'">';
					   echo '<button type="submit" name="tratado_msj" class="btn btn-primary btn-xs" data-toggle="modal"> <i class="glyphicon glyphicon-flag"></i> Marcar como tratado</button></form></div>';

				   }


				}
                  while($cli22=mysqli_fetch_array($sql22)){ //alertas de recordatorios
				    if($cli22['asunto']== "Recordatorio de pago"){
				    echo '<tr><td><b>Liquidación</b></td><td><b>Asunto</b></td><td><b>Estatus</b></td><td><b>Cliente</b></td></tr>';
					echo '<tr><td>'.$cli22['id_recordatorio'].'</td><td>'.$cli22['asunto'].'</td>';}
					if($cli22['asunto']!= "Recordatorio de pago"){
				    echo '<tr><td><b>Número</b></td><td><b>Asunto</b></td><td><b>Estatus</b></td><td><b>Cliente</b></td></tr>';
					echo '<tr><td>'.$cli22['id_recordatorio'].'</td><td>'.$cli22['asunto'].'</td>';}

					if($cli22['status']=="si"){echo '<td>Enviado</td></tr>';}
					else if ($cli22['status']=="no"){echo '<td>No enviado</td>';}

					$id_des = $cli22['id_cliente'];
					$sql22a = mysqli_query($conn,"select nombre from cliente where id_cliente='$id_des'");
					$rsql22a = mysqli_fetch_assoc($sql22a);
					$des_cli = $rsql22a['nombre'];

					echo '<td><a href="brm.php?cl='.$id_des.'&carp=cli&file=datos" class="btn btn-xs btn-info">'.$des_cli.'</a></td><td></tr>';

					echo '<tr><td colspan="4"><b>Contenido</b></td></tr>';

					if($cli22['asunto']== "Recordatorio de pago"){
					echo '<tr><td colspan="4"><textarea disabled class="recor_text">'.$cli22['contenido'].'</textarea></td></tr>';
					}
					if($cli22['asunto']!= "Recordatorio de pago"){
					echo '<tr><td colspan="4">'.$cli22['contenido'].'</td></tr>';
					}

					$id_user = $cli22['id_usuario'];
					$sql22b = mysqli_query($conn,"select nombre_completo from usuario where id_usuario='$id_user'");
					$rsql22b = mysqli_fetch_assoc($sql22b);
					$g_user = $rsql22b['nombre_completo'];
					echo '<tr><td><b>Generado Por</b></td><td>'.$g_user.'</td><td><b>Fecha</b></td><td>'.date_format(date_create($cli22['fecha']),'d/m/Y').'</td><td><b>Hora</b></td><td>'.$cli22['hora'].'</td></tr>';
					echo '</table>';

					echo '<div class="col-md-6">
					<form action="crear_recordatorio.php" method="post">';

					if($tipo_usuario != 2 ){
						echo '<button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal_recor_del"> <i class="glyphicon glyphicon-remove"></i> Eliminar</button>&nbsp;';
				    }

					if($tipo_usuario == 2 && $id_user != $id_usuario){

						echo "";

				   }else{
						echo '<button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal_recor_up"><i class="glyphicon glyphicon-pencil"></i> Editar</button>&nbsp;';
						echo '<button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal_enviar"> <i class="glyphicon glyphicon-ok"></i> Enviar</button>&nbsp;';
						echo '<input type="hidden" name="id_recordatorio" value="'.$cli22['id_recordatorio'].'">';
						echo '<button type="submit" name="tratado_recor" class="btn btn-primary btn-xs" data-toggle="modal"> <i class="glyphicon glyphicon-flag"></i> Marcar como tratado</button></form></div>';

				   }


				}


					 }

					//CARPETA TRATAMIENTO AL CLIENTE
					else if (isset($file)&& $carpeta=="prot" && $file=="trata"){
					echo '<table class="table table-hover table-trata">';
				while($cli19=mysqli_fetch_array($sql19)){
				    echo '<tr><td><b>Tratamiento</b></td></tr>';
					echo '<tr><td colspan="3">'.$cli19['descripcion'].'</td></tr>';

					}
               echo '<tr><td colspan="3"><tr><td colspan="3"><button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal_cli_up1"><i class="glyphicon glyphicon-pencil"></i> Editar</button>&nbsp;</td></tr>';
                echo '</table>';
				}

				//CARPETA SLOGAN
				else if (isset($file)&& $carpeta=="prot" && $file=="slogan"){
				echo '<table class="table table-hover table-slogan">';
				while($cli19=mysqli_fetch_array($sql19)){
					echo '<tr><td><b>Atributos</b></td></tr>';
					echo '<tr><td colspan="3">'.$cli19['slogan'].'</td></tr>';

				}
				echo '<tr><td colspan="3"><tr><td colspan="3"><button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal_cli_up1"><i class="glyphicon glyphicon-pencil"></i> Editar</button>&nbsp;</td></tr>';
        		echo '</table>';
				}

			   //CARPETA CONTESTACION TELEFONICA
			   else if (isset($file)&& $carpeta=="prot" && $file=="contes"){
			   echo '<table class="table table-hover table-contes">';
				while($cli19=mysqli_fetch_array($sql19)){
					echo '<tr><td><b>Contestación Telefónica</b></td></tr>';
					echo '<tr><td colspan="3">'.$cli19['contestacion'].'</td></tr>';

				}

				echo '<tr><td colspan="3"><tr><td colspan="3"><button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal_cli_up1"><i class="glyphicon glyphicon-pencil"></i> Editar</button>&nbsp;</td></tr>';
				echo '</table>';
				}

				//CARPETA COLABORADORES
                else if (isset($file)&& $carpeta=="prot" && $file=="cola"){
				echo '<table class="table table-hover table-cola">';
					echo '<thead><tr class="text-center" id="col_0"><td><b>Colaborador</b></td><td><b>Relacion</b></td>
					<td><b>Email</b></td><td><b>Telefono</b></td>
					<td><b>Twitter</b><td><b>Whatsapp</b></td>
					<td><b>Foto</b></td>
					<td><b>Accion</b></td>
					</tr></thead>';

				 echo '<tbody>';
				while($cli19_col=mysqli_fetch_array($sql19_col)){

				    echo '<tr class="text-center" id="cola_'.$cli19_col['id'].'">';
		            echo '<td class="valign">'.$cli19_col['nombre'].'</td>';
					echo '<td class="valign">'.$cli19_col['jerarquia'].'</td>';
					echo '<td class="valign">'.$cli19_col['email'].'</td>';
					echo '<td class="valign">'.$cli19_col['telefono'].'</td>';
					echo '<td class="valign">'.$cli19_col['twitter'].'</td>';
					echo '<td class="valign">'.$cli19_col['whatsapp'].'</td>';
					echo '<td class="valign"><a href="'.$cli19_col['foto'].'" target="_blank"><img class="foto-col" src="'.$cli19_col['foto'].'"/></a></td>';
					echo '<td class="valign"><button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal_edit_col_'.$cli19_col['id'].'"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> </button>
					<button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal_del_col_'.$cli19_col['id'].'"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> </button>
					</td>';
					echo '</tr>';

					?>

					<!-- MODAL EDITAR COLABORADORES-->
		<div id="modal_edit_col_<?php echo $cli19_col['id'];?>" class="modal fade" role="dialog">
		<div class="modal-dialog modal-sm">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Editar Colaborador</h4>
      </div>
	 <div class="modal-body">
	 <div class="row">
	<div  class="col-md-12">
<form class="cliente" name="contact" enctype="multipart/form-data" id="cliente" method="post" action="accion_protocolo.php">
			<div id="contenedor2">

			<input type="text" name="colaborador" placeholder="Nombre" class="form-control" value="<?php echo $cli19_col['nombre'];?>"/></br>
			<input type="text" name="jerarquia" placeholder="Relacion" class="form-control" value="<?php echo $cli19_col['jerarquia'];?>"/></br>
			<input type="text" name="email" placeholder="Email" class="form-control" value="<?php echo $cli19_col['email'];?>"/></br>
			<input type="text" name="tel" placeholder="Telefono" class="form-control" value="<?php echo $cli19_col['telefono'];?>"/></br>
			<input type="text" name="tw" placeholder="Twitter" class="form-control" value="<?php echo $cli19_col['twitter'];?>"/></br>
			<input type="text" name="wh" placeholder="Whatsapp" class="form-control" value="<?php echo $cli19_col['whatsapp'];?>"/></br>
			<div class="input-group">
			<span class="input-group-btn">
                    <span class="btn btn-default btn-file">
					<span class="glyphicon glyphicon-folder-open"></span>
                    <b>&nbsp;Img.</b>
					<input type="file" name="foto" class="form-control" accept="image/*"/>
                    </span>
                </span>
                <input type="text" class="form-control" readonly>
            </div>

			<input type="hidden" name="foto2" value="<?php echo $cli19_col['foto'];?>">

			</div>
		</div>
    </div>
    </div>
		    <div class="modal-footer">
	  	<div class="col-md-12 text-left">
		<input type="hidden" name="id_col" value="<?php echo $cli19_col['id'];?>">
		<input type="hidden" name="id" value="<?php echo $cliente;?>">
	     <button class="btn btn-primary btn-sm" type="submit"  name="col_edit"><i class="glyphicon glyphicon-check"></i> Guardar</button>
        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cancelar</button>
		 </form>
				</div>
			</div>
			</div>
			</div>
			</div>

	<!-- MODAL ELIMINAR COLABORADORES-->
<div id="modal_del_col_<?php echo $cli19_col['id'];?>" class="modal fade" role="dialog">
  <div class="modal-dialog modal-sm">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Eliminar Colaborador</h4>
      </div>
	 <div class="modal-body">
	 <div class="row">
	<div  class="col-md-12">
<form class="cliente" name="contact" enctype="multipart/form-data" id="cliente" method="post" action="accion_protocolo.php">
        <p>¿ Esta seguro que desea eliminar el colaborador <?php echo $cli19_col['nombre']; ?> ?</p>
		</div>
    </div>
    </div>
		    <div class="modal-footer">
	  	<div class="col-md-12 text-left">
		<input type="hidden" name="id_col" value="<?php echo $cli19_col['id'];?>">
			<input type="hidden" name="id" value="<?php echo $cliente;?>">
	     <button class="btn btn-primary btn-sm" type="submit"  name="col_del"><i class="glyphicon glyphicon-remove"></i> Eliminar</button>
        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cancelar</button>
		 </form>
		</div>
			</div>
			</div>
			</div>
			</div>
		<?php
					}

				echo '</tbody>';
				if (mysqli_num_rows($sql19_col) < 10){
				echo '<tfoot><tr><td colspan="8"><button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal_col"><i class="glyphicon glyphicon-plus"></i> Agregar</button>&nbsp;</td></tr>';
				}
				echo '</tfoot></table>';
				}

				//CARPETA FOTOS
				else if (isset($file)&& $carpeta=="prot" && $file=="pic"){


				echo '<table class="table table-hover table-pic">';
				 echo '<tr><td><b>Información</b></td></tr>';
				$cf = 0;

				while($cli20=mysqli_fetch_array($sql20a)){

					$pic = explode (",",$cli20['fotos']);
					$maxp = sizeof($pic);
					$p =0 ;
		            while($p < $maxp){
					$ci = $cf+1;
				    echo '<tr>';
					echo '<td td colspan="2"><img class="vimg" src="'.$pic[$p].'"/></td></tr>';
					echo '<tr><td><a class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal_foto_up"> <i class="glyphicon glyphicon-folder-open"></i> Actualizar</a></td>';
                    echo '<td><form method="post" action="accion_protocolo.php">';
					echo '<input type="hidden" value="'.$cli20['id'].'" name="id_pic">';
					echo '<input type="hidden" value="'.$_GET['cl'].'" name="cl">';
	                echo '<button type="submit" class="btn btn-primary btn-xs" name="pic_del"> <i class="glyphicon glyphicon-remove"></i> Eliminar</button></td>';
					echo '</form>';
					echo '</tr>';
					?>

<!-- MODAL FOTOS-->
<div id="modal_foto_up" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Actualizar Foto</h4>
      </div>
	 <div class="modal-body">
	 <div class="row">
	<div  class="col-md-8">
		<form class="cliente" name="contact" enctype="multipart/form-data" id="cliente" method="post" action="accion_protocolo.php">
			<div class="input-group">
                <span class="input-group-btn">
                    <span class="btn btn-default btn-file">
					<span class="glyphicon glyphicon-folder-open"></span>
                    <b>&nbsp;Fotos</b>
						<input type="file" name="fotos[]" id="fotos" multiple="true"/>
                    </span>
                </span>
                <input type="text" class="form-control" readonly>
            </div>

		</div>


        </div>

      </div>
			<div class="modal-footer">
			<div class="col-md-6 text-left col-md-offset-7">
			 <input type="hidden" value="<?php echo $_GET['cl'];?>" name="cl">
				<input type="hidden" value="<?php if(isset($cli20['id'])){echo $cli20['id']; }?>"  name="id_pic">
			 <button class="btn btn-primary" type="submit"  name="foto_up"><i class="glyphicon glyphicon-check"></i> Guardar</button>
			<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
			 </form>
							</div>
						</div>
					</div>
				</div>
		</div>
				<?php
					$p++;

					}

				}
				   if ($cf < 10){
					echo '<tr><td><button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modal_fotos"> <i class="glyphicon glyphicon-folder-open"></i> Agregar </button></td></tr>';
				   }
				   echo '</table>';
				} // Fin carpeta fotos

				else if (isset($search) && mysqli_num_rows($sql1) == 0 && mysqli_num_rows($sql2) == 0 && mysqli_num_rows($sql3)==0  && mysqli_num_rows($sql4)==0){
				echo '</br><h4 class="text-center">La búsqueda general no produjo ningún resultado</h4>';
					}

				else if (isset($search2) && mysqli_num_rows($sql26) == 0 && mysqli_num_rows($sql27) == 0 && mysqli_num_rows($sql28)==0  && mysqli_num_rows($sql29)==0){
				echo '</br><h4 class="text-center">La búsqueda en la carpeta no produjo ningún resultado</h4>';
					}

					 else {
						 $rsql5 = mysqli_fetch_assoc($sql5);
						 $r_emp = $rsql5['logo'];
						 echo '<div class="col-md-6 col-md-offset-4 logo-img"><img src="'.$r_emp.'"/></div>';
					 }

				 ?>
						</div> <!-- FIN tercera columna -->
					</div><!--FIN Row -->
				</div><!--FIN CONTAINER COLUMNS-->



<div class="row bg-primary row-footer">
<?php if(isset($_GET['msj'])){
$mensaje = $_GET['msj'];
echo ' &nbsp;'.$mensaje;
}
else {
echo ' &nbsp;BRM';}
?>
</div><!--FIN FOOTER -->

</div><!--FIN FRAME -->
<?php
include 'cliente.php';
include 'cliente2.php';
include 'recordatorio.php';
include 'recordatorio2.php';
include 'parametro.php';
include 'parametro2.php';
include 'mensaje.php';
include 'mensaje2.php';
include 'protocolo.php';
include 'usuario.php';
include 'enviar.php';
include 'sortable.php';
include 'complementos.php';
include 'complementos2.php';
?>

<script>

$(document).ready(function() {
    var body_cli = $("#body_cli");
    var fecha = false;
    var valor = true;
    var subtotal = 0;
    var referencia = true;
    var orden = $("#orden");
    var fecha_desc = $("#fecha_desc");
    var fecha_asc = $("#fecha_asc");
    var valor_desc = $("#valor_desc");
    var valor_asc = $("#valor_asc");
    var referencia_desc = $("#referencia_desc");
    var referencia_asc = $("#referencia_asc");
    var btnreferencia = $("#btnreferencia");
    var btnvalor = $("#btnvalor");
    var btnfecha = $("#btnfecha");


    orden.removeClass('hidden');
    btnreferencia.click(function (){
        if (referencia){
            orden.addClass('hidden');
            fecha_desc.addClass('hidden');
            fecha_asc.addClass('hidden');
            valor_desc.addClass('hidden');
            valor_asc.addClass('hidden');
            referencia_desc.removeClass('hidden');
            referencia_asc.addClass('hidden');
            referencia = false;
            fecha = true;
            valor = true;
        } else {
            orden.addClass('hidden');
            fecha_desc.addClass('hidden');
            fecha_asc.addClass('hidden');
            valor_desc.addClass('hidden');
            valor_asc.addClass('hidden');
            referencia_desc.addClass('hidden');
            referencia_asc.removeClass('hidden');
            referencia = true;
            fecha = true;
            valor = true;
        }
    });
    btnvalor.click(function (){
        if (valor){
            orden.addClass('hidden');
            fecha_desc.addClass('hidden');
            fecha_asc.addClass('hidden');
            valor_desc.removeClass('hidden');
            valor_asc.addClass('hidden');
            referencia_desc.addClass('hidden');
            referencia_asc.addClass('hidden');
            valor = false;
            referencia = true;
            fecha = true;
        } else {
            orden.addClass('hidden');
            fecha_desc.addClass('hidden');
            fecha_asc.addClass('hidden');
            valor_desc.addClass('hidden');
            valor_asc.removeClass('hidden');
            referencia_desc.addClass('hidden');
            referencia_asc.addClass('hidden');
            referencia = true;
            fecha = true;
            valor = true;
        }
    });
    btnfecha.click(function (){
        if (fecha){
            orden.addClass('hidden');
            fecha_desc.removeClass('hidden');
            fecha_asc.addClass('hidden');
            valor_desc.addClass('hidden');
            valor_asc.addClass('hidden');
            referencia_desc.addClass('hidden');
            referencia_asc.addClass('hidden');
            fecha = false;
            referencia = true;
            valor = true;
        } else {
            orden.addClass('hidden');
            fecha_desc.addClass('hidden');
            fecha_asc.removeClass('hidden');
            valor_desc.addClass('hidden');
            valor_asc.addClass('hidden');
            referencia_desc.addClass('hidden');
            referencia_asc.addClass('hidden');
            referencia = true;
            fecha = true;
            valor = true;
        }
    });


    $("#fecha_desc").sortable();
    $("#fecha_desc").disableSelection();
    $("#fecha_asc").sortable();
    $("#fecha_asc").disableSelection();
    $("#valor_desc").sortable();
    $("#valor_desc").disableSelection();
    $("#valor_asc").sortable();
    $("#valor_asc").disableSelection();
    $("#referencia_desc").sortable();
    $("#referencia_desc").disableSelection();
    $("#referencia_asc").sortable();
    $("#referencia_asc").disableSelection();


$('#search_main').change(function () {

	$('.search-text').css('color','white');
	var searchs = $(this).val();

	if($(this).val()) {
        var dataString = 'd_search='+searchs;

        $.ajax({
            type: "GET",
            url: "busqueda.php",
            data: dataString,
                success:function (data) {
                    $(".c-hall").html(data);
					}
				});

			}

});

    $('#search_main').keyup(function () { //Muestra resultados de search sin click cuando se escribe

		$('.search-text').css('color','white');
        var search = $(this).val();

		//console.log(search);

		if($(this).val()) {
        var dataString = 'd_search='+search;

        $.ajax({
            type: "GET",
            url: "busqueda.php",
            data: dataString,
                success:function (data) {
                    $(".c-hall").html(data);
					}
				});

			}
    });

		$('#search_carpeta').on('keyup',function () { //Identifica cuando se introduce un valor en busqueda carpeta
		$('.search-text-carpeta').css('color','white');
	    });

		if($('#search_carpeta').is(':disabled')){ // Coloca cursor en las cajas de busqueda al cargar la pagina
				$("#search_main").focus();

		}

		else {
			$("#search_carpeta").focus();
		}


		$(".modal").on("hidden.bs.modal", function () { //coloca el cursor en la caja de busqueda al cerrar modals

					if($('#search_carpeta').is(':disabled')){
					setTimeout(function(){ $("#search_main").focus();},300);
					}

				else {
					setTimeout(function(){ $("#search_carpeta").focus();},300);
				}

		});

		$('#search_main').blur(function()  /*cuando el cursor no esta en el campo busqueda general
															 y este no tiene ningun valor coloca el texto placeholder*/
		{
			if( !$(this).val() ) {
			$('.search-text').css('color','gray');
		}
		});


		$('#search_carpeta').blur(function()  /*cuando el cursor no esta en el campo busqueda en carpeta
															 y este no tiene ningun valor coloca el texto placeholder*/
		{
			if( !$(this).val() ) {
			$('.search-text-carpeta').css('color','gray');
		}
		});

		 $('[data-toggle="tooltip"]').tooltip(); //Activa los tooltip

		 $('#guarda_logo').prop('disabled','true');
		 $('#guarda_foto').prop('disabled','true');

});


function readURL(input) { //Vista previa de logo de cliente
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#logo-img')
                        .attr('src', e.target.result);
						$('#guarda_logo').removeAttr('disabled');
                };

                reader.readAsDataURL(input.files[0]);

            }


        }


function readfURL(input) { //vista previa de foto de persona de contacto
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#foto-img')
                        .attr('src', e.target.result);
						 $('#guarda_foto').removeAttr('disabled');
                };

                reader.readAsDataURL(input.files[0]);

            }

        }

// $(".table-list-cp").tablesorter({
//     debug:false,
//
//     dateFormat :"uk", // set the default date format
//
//     // alert("<php echo $sub ?>");
//
//     textExtraction: function(node) {
//         var $node = $(node)
//         var text = $node.text();
//         if ($node.hasClass('nat_moneda')) {
//             text = text.replace(/[$,]/g, '');
//         };
//         if ($node.hasClass('nat_porcentaje')) {
//             text = text.replace(/[%,]/g, '');
//         };
//         if ($node.hasClass('nat_hora')) {
//             text = text.replace(':', '.');
//         };
//         if ($node.hasClass('nat_numero')) {
//             text = text.replace(',', '');
//         };
//         return text;
//     }
// });

</script>

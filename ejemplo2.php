<script src="http://code.jquery.com/jquery-1.9.1.js" type="text/javascript"></script>
<style>
body{padding:20px;}
.btncls{padding: 10px; background-color:#000; color:#fff; border:none; cursor:pointer; margin:10px;}
.txtcls{color:#44f; font-size:20px;}
</style>

<button class="btncls" id="obtan">Obtener ancho</button>
<button class="btncls" id="obtal">Obtener alto</button>
<div class="txtcls" id="anvent">&nbsp;</div>
<div class="txtcls" id="alvent">&nbsp;</div>

<script>
<!-- Funcion para obtener el Ancho(Height) --> 
function obtenerAncho( obj, ancho ) {
  $( "#anvent" ).text( "El ancho de la " + obj + " es " + ancho + "px. (Width)" );
}
$("#obtan").click(function() {
  obtenerAncho( "ventana", $( window ).width() );
});

<!-- Funcion para obtener el Alto(Height) --> 
function obtenerAlto( obj, alto ) {
  $( "#alvent" ).text( "El alto de la " + obj + " es " + alto + "px. (Height)" );
}
$( "#obtal" ).click(function() {
  obtenerAlto( "ventana", $( window ).height() );
});
</script>
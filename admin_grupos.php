<?php 

 include 'set.php';
   session_start();
   error_reporting(E_ALL);
	ini_set('display_errors', 1);
   
if(isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == 1){ 
	$id_usuario = $_SESSION['id_usuario'];
	$nombre = $_SESSION['nombre_usuario'];
	$tipo_usuario = $_SESSION['tipo_usuario'] ;
}

if(isset($_POST['guardar_grupos'])){
	
	$id_empresa = $_GET['empresa'];
	
	
	for($s = 1; $s < 6; $s++){ 	
	
		$ngrupo = $_POST['nombregrupo'.$s];
		
		$sel_grupo = "SELECT * FROM grupos WHERE id_empresa='$id_empresa' AND posicion='$s'";
		$res_grupo = mysqli_query($conn,$sel_grupo);
		$vgrupo = mysqli_num_rows($res_grupo);
		
			
		
		if($vgrupo > 0){
			
			$qgrupo = "UPDATE grupos SET nombre_grupo='$ngrupo' WHERE id_empresa='$id_empresa' AND posicion='$s'";			
			mysqli_query($conn,$qgrupo) or die (mysqli_error($conn));
			while($dgrupo = mysqli_fetch_assoc($res_grupo)){
				$id_grupo = $dgrupo['id']; 
			}
		
			
		}else{
			
			$qgrupo =  "INSERT INTO grupos (id_empresa,nombre_grupo,posicion) VALUES ('$id_empresa','$ngrupo','$s');"; 
			mysqli_query($conn,$qgrupo) or die (mysqli_error($conn));
			$id_grupo = mysqli_insert_id($conn);
		
		}			
		
		
		
		
		for($e = 1; $e < 8; $e++){						
			
			$nestatus = $_POST['estatusgrupo_'.$s.'_'.$e];
			
			$sel_estatus = "SELECT * FROM estatus WHERE id_grupo='$id_grupo' AND id_empresa='$id_empresa' AND posicion='$e'";
			$res_estatus = mysqli_query($conn,$sel_estatus);
			$vestatus = mysqli_num_rows($res_estatus);
			
			if($vestatus > 0){
				$qestatus = "UPDATE estatus SET nombre_estatus='$nestatus' WHERE id_grupo='$id_grupo' AND id_empresa='$id_empresa' AND posicion='$e'";
			
			}else {
				$qestatus = "INSERT INTO estatus (id_empresa,id_grupo,nombre_estatus,posicion) VALUES ('$id_empresa','$id_grupo','$nestatus','$e');";
						
			}
			
			
			mysqli_query($conn,$qestatus) or die (mysqli_error($conn));	
				
			
		}
		
	}

		header('location:admin_grupos.php?empresa='.$id_empresa);	
		
	
}


$id_empresa = $_GET["empresa"];
$list_grupos = mysqli_query($conn,"SELECT * FROM grupos WHERE id_empresa='$id_empresa' ORDER BY posicion");							

			

require 'header.php';
?>

<body class="body-login">
 
<div class="wrapper container">   
     
<form enctype="multipart/form-data"  method="post" action="admin_grupos.php?empresa=<?php echo $id_empresa;?>" id="form-grupos-empresa" class="form-horizontal">
	
	<div class="row">
		<div class="col-md-10">
			<h2>Grupos de Empresa</h2>
			<hr></hr>
		</div>
	</div>
	
	
	
	<?php		
		
	for($g = 1; $g < 6; $g++){ 
		
	  $valor_grupo = "";	
		
		foreach($list_grupos as $lg){
			if($lg['posicion']==$g){
				
				$valor_grupo = $lg['nombre_grupo'];
				$id_grupo = $lg['id'];
				
			}
		}
		
	if(!empty($id_grupo)){	
		
	   $list_estatus = mysqli_query($conn,"SELECT * FROM estatus WHERE id_empresa='$id_empresa' AND id_grupo='$id_grupo' AND posicion <> '6' AND posicion <> '7' ORDER BY posicion ");		
	}	
		?>
								
			<div class="row">
			<div class="col-md-4 cont-nombre-grupo">
				<label>Nombre del grupo</label>
				<input type="text" class="form-control campo-nombre-grupo" name="nombregrupo<?php echo $g;?>"
						value="<?php echo $valor_grupo; ?>">
			</div>
						
			</div>
			
			<div class="row"><br>
			<p><b>&nbsp;&nbsp;&nbsp;&nbsp;Estatus</b></p>
			
		<?php 
			
		for($s = 1; $s < 8; $s++){ 	
			
		$valor_estatus = "";
		
		if(!empty($list_estatus)){
			
		  foreach($list_estatus as $ls){												
			
			if($ls['posicion']==$s){
				$valor_estatus = $ls['nombre_estatus'];				
			}
		  }	
		
		}
		?>
		<div class="cont-estatus">				
			<div class="col-sm-2">
			<?php if($s==6){ ?>
					<input type="hidden" name="estatusgrupo_<?php echo $g.'_'.$s;?>" class="form-control campo-estatus" value="Activo">	
		<?php	}	elseif($s==7) {?>		
				<input type="hidden" name="estatusgrupo_<?php echo $g.'_'.$s;?>" class="form-control campo-estatus" value="Inactivo">	
		<?php }else{ ?>		
				<input type="text" name="estatusgrupo_<?php echo $g.'_'.$s;?>" class="form-control campo-estatus" value="<?php echo $valor_estatus; ?>">
			<?php } ?>		
			</div>
			
		</div>	
		<?php } ?>
		
			</div>	
			<hr></hr>
			
	<?php 	} ?>
		
<div class="row">

	<div class="form-group">
	<div class="col-md-4 col-md-offset-8">				
		<button type="submit" class="btn btn-primary" value="Registrar" name="guardar_grupos"/> Guardar</button>
		<a href="admin.php" class="btn btn-info"> Volver</a>
	</div>
	</div>
</div>
			
</form>

</div>


</body>


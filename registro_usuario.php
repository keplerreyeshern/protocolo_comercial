<?php 

 include 'set.php';
   session_start();
   $error = "";   
   error_reporting(E_ALL);
ini_set('display_errors', 1);

if(isset($_SESSION['loggedin'])){ 
 $id_usuario = $_SESSION['id_usuario'];
$nombre = $_SESSION['nombre_usuario'];
 $tipo_usuario = $_SESSION['tipo_usuario'] ;
 $rest=1;
}

else {
	$rest = 0;
}

$query = mysqli_query($conn,"select * from empresa");

if(isset($_POST["registrar"])) {

if(isset($_POST["empresa"])) {$id_empresa = $_POST["empresa"];}
if(isset($_POST["username"])) {$nombre_usuario = $_POST["username"];}
if(isset($_POST["nombre"])) {$nombre = $_POST["nombre"];}
if(isset($_POST["email"])) {$email = $_POST["email"];}
if(isset($_POST["password"])) {$password = $_POST["password"];} 
if(isset($_POST["tipo"])) {$tipo = $_POST["tipo"];}     	

/*
//verifica quien esta creando la cuenta
if(isset($rest)){
$query2 = mysqli_query($conn,"select count(*) from usuario where id_empresa ='$id_empresa'");
$conteo = mysqli_result($query2,0);
}


if(isset($conteo) && $conteo >= 4){
$error = '<div class="alert alert-danger">Esta empresa ya alcanzo el número máximo de usuarios.</div>';
}	
*/
if($rest ==1) {
//crea la cuenta del usuario
$query3 = "INSERT INTO usuario(nombre,nombre_completo,email,password,tipo,status,id_empresa)VALUES('$nombre_usuario','$nombre','$email','$password','$tipo','activo','$id_empresa')";	
 mysqli_query($conn,$query3) or die (mysqli_error());
 header('location:admin.php?empresa='.$id_empresa.'');
 exit();
}
   }
  require 'header.php'; 
?>
    <body class="body-login">
 
        <div class="wrapper-usuario">  
     

<form id="defaultForm" method="post" action="registro_usuario.php" class="form-usuario col-md-4 col-md-offset-4 form-horizontal">
 <h2 class="form-signin-heading">Registro de Usuario<hr></hr></h2>
  <?php if(isset($error)&& $error != ""){echo $error;}?>
<div class="form-group">
<label class="col-lg-3 control-label">Empresa</label>
<div class="col-lg-9">

<?php
if(isset($_GET['empresa'])){
	$q_empresa = $_GET['empresa']; 
	 $query2 = mysqli_query($conn,"select id_empresa, nombre_empresa from empresa where id_empresa = '$q_empresa'");
    while($row2 = mysqli_fetch_array($query2))
 {
echo '<label class="col-lg-3 control-label">'.$row2["nombre_empresa"].'</label>';
echo '<input type="hidden" name="empresa" value="'.$row2["id_empresa"].'">'; 
 }
} 

else if(isset($query)){ 
echo '<select name="empresa" class="form-control">';
while($row = mysqli_fetch_array($query))
 {
	   if($row["nombre_empresa"] != "master"){	
echo '<option value="'.$row["id_empresa"].'">'.$row["nombre_empresa"].'</option>'; 
 }}
echo '</select>';
}?>

</div>
</div>
<div class="form-group">
<label class="col-lg-3 control-label">Usuario</label>
<div class="col-lg-9">
<input type="text" class="form-control" name="username" />
</div>
</div>
<div class="form-group">
<label class="col-lg-3 control-label">Nombre</label>
<div class="col-lg-9">
<input type="text" class="form-control" name="nombre" />
</div>
</div>
<div class="form-group">
<label class="col-lg-3 control-label">Email</label>
<div class="col-lg-9">
<input type="email" class="form-control" name="email" />
</div>
</div>
<div class="form-group">
<label class="col-lg-3 control-label">Password</label>
<div class="col-lg-9">
<input type="password" class="form-control" name="password" />
</div>
</div>
<div class="form-group">
<label class="col-lg-3 control-label">Password Nuevamente</label>
<div class="col-lg-9">
<input type="password" class="form-control" name="confirmPassword" />
</div>
</div>
<div class="form-group">
<label class="col-lg-3 control-label">Tipo</label>
<div class="col-lg-9">
<select name="tipo" class="form-control">
<option value="2">Usuario General</option>
<option value="3">Supervisor</option>
</select>
</div>
</div>

<div class="form-group">
<label class="col-lg-3 control-label" id="captchaOperation"></label>
<div class="col-lg-6">
<input type="text" class="form-control" name="captcha" />
</div>
</div>


<div class="form-group">
<div class="col-lg-9 col-lg-offset-3">
<button type="submit" class="btn btn-primary" name="registrar"><i class="glyphicon glyphicon-check"></i> Registrar</button>
<a href="admin.php?empresa=<?php echo $_GET["empresa"];?>" class="btn btn-info"><i class="glyphicon glyphicon-menu-left"></i> Volver</a>
</div>
</div>
</form>
</div>

<script type="text/javascript">

$(function () {
    // Generate a simple captcha
    function randomNumber(min, max) {
        return Math.floor(Math.random() * (max - min + 1) + min);
    };
    $('#captchaOperation').html([randomNumber(1, 100), '+', randomNumber(1, 200),'='].join(' '));

    $('#defaultForm').bootstrapValidator({
        message: 'El valor introducido no es válido',
        fields: {
			    
			
            username: {
                message: 'El nombre de usuario no es válido',
                validators: {
                    notEmpty: {
                        message: 'El nombre de usuario no puede estar vacío'
                    },
                    stringLength: {
                        min: 6,
                        max: 30,
                        message: 'El nombre de usuario debe tener mínimo 6 caracteres'
                    },
                    regexp: {
                        regexp: /^[a-zA-Z0-9_\.]+$/,
                        message: 'El nombre de usuario sólo puede contener , números, puntos o subrayados'
                    },
                    different: {
                        field: 'password',
                        message: 'El nombre de usuario y la contraseña no pueden ser iguales'
                    }
                }
            },
			nombre: {
                validators: {
                    notEmpty: {
                        message: 'El nombre no puede estar vacío'
                    },
                }
            },
			
			email: {
                validators: {
                    notEmpty: {
                        message: 'El email no puede estar vacío'
                    },
                    
                }
            },
			
            password: {
                validators: {
                    notEmpty: {
                        message: 'La contraseña no puede estar vacía'
                    },
                    identical: {
                        field: 'confirmPassword',
                        message: 'La contraseña y su confirmación no son iguales'
                    },
                    different: {
                        field: 'username',
                        message: 'La contraseña y el nombre de usuario no pueden ser iguales'
                    }
                }
            },
            confirmPassword: {
                validators: {
                    notEmpty: {
                        message: 'La confirmación de contraseña no puede estar vacía'
                    },
                    identical: {
                        field: 'password',
                        message: 'La contraseña y su confirmación no son iguales'
                    },
                    different: {
                        field: 'username',
                        message: 'La contraseña y el nombre de usuario no pueden ser iguales'
                    }
                }
            },
			

            captcha: {
                validators: {
                    callback: {
                        message: 'Respuesta Incorrecta',
                        callback: function(value, validator) {
                            var items = $('#captchaOperation').html().split(' '), sum = parseInt(items[0]) + parseInt(items[2]);
                            return value == sum;
                        }
                    }
                }
            }
        }
    });
});

//introducir logo formulario
$(document).on('change', '.btn-file :file', function() {
  var input = $(this),
      numFiles = input.get(0).files ? input.get(0).files.length : 1,
      label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
  input.trigger('fileselect', [numFiles, label]);
});

$(document).ready( function() {
    $('.btn-file :file').on('fileselect', function(event, numFiles, label) {
        
        var input = $(this).parents('.input-group').find(':text'),
            log = numFiles > 1 ? numFiles + ' files selected' : label;
        
        if( input.length ) {
            input.val(log);
        } else {
            if( log ) alert(log);
        }
        
    });
});
</script>

</body>


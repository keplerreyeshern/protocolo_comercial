<?php ob_start();
/** Error reporting */
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
ini_set("max_execution_time", 3000);
ini_set('memory_limit', '4095M');

/*Determina fecha y hora*/
date_default_timezone_set('America/Mexico_City');
$fecha = date('d/m/y');
$hora = date('H:i:s');

/** Include PHPExcel */
require_once dirname(__FILE__) . '/Classes/PHPExcel.php';
include 'set.php';

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();


/*Exporta referencias de indicadores a nivel de cliente*/
if(isset($_POST['exportar'])){

	// Rename worksheet
	$titulo = substr($_POST['nombre'], 0, 15);
    $objPHPExcel->getActiveSheet()->setTitle('Indicador_'.$titulo);
	$filename = "Indicador_".$_POST['nombre'].".xlsx";
	$valores1= unserialize(base64_decode($_POST['excel1']));
	$valores2 = unserialize(base64_decode($_POST['excel2']));
	$valores3 = unserialize(base64_decode($_POST['excel3']));
	$valores4 = unserialize(base64_decode($_POST['excel4']));
	//print_r($);exit;
    $cliente = $_POST['cliente'];
	$act = $_POST['active'];
	$razon = $_POST['razon'];
	$empresa = $_POST['empresa'];
	$nusuario = $_POST['usuario'];
	$query = mysqli_query($conn,"select nombre_completo from usuario where id_usuario = '$nusuario'");
	$rquery = mysqli_fetch_assoc($query);
	$usuario = $rquery["nombre_completo"];

	$nat = $_POST['nat'];
	$active = false;
	if($act == '1'){
		$active=true;
	}

	$styleArray = array(
    'font' => array(
        'bold' => true
				));

$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('A1', 'Indicador')
            ->setCellValue('B1', $_POST['nombre'])
            ->setCellValue('A3', 'Cliente')
            ->setCellValue('B3', $cliente)
            ->setCellValue('E3', 'Razón Social')
            ->setCellValue('G3', $razon)
			->setCellValue('A5', 'Referencia')
            ->setCellValue('B5', 'Valor')
			->setCellValue('E5', 'Naturaleza')
			->setCellValue('F5', $nat);
if ($active){
	$objPHPExcel->setActiveSheetIndex(0)
		->setCellValue('C5', 'SubTotal')
		->setCellValue('D5', 'Fecha');
} else {
	$objPHPExcel->setActiveSheetIndex(0)
		->setCellValue('C5', 'Fecha');
}



		$objPHPExcel->getActiveSheet()->getStyle("A1")->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle("A3")->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle("E3")->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle("A5")->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle("B5")->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle("E5")->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle("C5")->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle("D5")->getFont()->setBold(true);


      $i = 6; //Numero de fila donde se va a comenzar a rellenar con los valores
	 foreach ($valores1 as $valor1){
     $objPHPExcel->setActiveSheetIndex(0)
		 ->setCellValue('A'.$i, $valor1);
//		 ->getNumberFormat();
//		 ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
     $i++;
     }
	 $i = 6;
	 foreach ($valores2 as $valor2){
     $objPHPExcel->setActiveSheetIndex(0)
		 ->setCellValue('B'.$i, $valor2);
     $i++;
     }
	if ($active){
		$i = 6;
		foreach ($valores3 as $valor3){
			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('D'.$i, $valor3);
			$i++;
		}
		$i = 6;
		foreach ($valores4 as $valor4){
			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('C'.$i, $valor4);
			$i++;
		}
	} else {
		$i = 6;
		foreach ($valores3 as $valor3){
			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('C'.$i, $valor3);
			$i++;
		}
	}



	 $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A'.($i+1), 'Empresa')
            ->setCellValue('B'.($i+1), $empresa)
            ->setCellValue('A'.($i+2), 'Generado por')
            ->setCellValue('B'.($i+2), $usuario)
			->setCellValue('A'.($i+3), 'Fecha')
            ->setCellValue('B'.($i+3), $fecha)
            ->setCellValue('A'.($i+4), 'Hora')
            ->setCellValue('B'.($i+4), $hora);


			$objPHPExcel->getActiveSheet()->getStyle("A".($i+1))->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle("A".($i+2))->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle("A".($i+3))->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle("A".($i+4))->getFont()->setBold(true);


}

/*Exporta listado de indicadores desde panel de control*/
if(isset($_POST['exportar_listado'])){

	$queryexcel = unserialize(base64_decode($_POST['queryexcel']));
	$empresa = $_POST['empresa'];
	$id_empresa = $_POST['id_excel_empresa'];
	$file = $_POST["file"];
	$nusuario = $_POST['usuario'];
		$query = mysqli_query($conn,"select nombre_completo from usuario where id_usuario = '$nusuario'");
		$rquery = mysqli_fetch_assoc($query);
	$usuario = $rquery["nombre_completo"];
	$nat = $_POST['nat'];

	$id_grupo = $_POST['filtro1'];
		$queryg = mysqli_query($conn, "SELECT nombre_grupo FROM grupos WHERE id='$id_grupo'");
		$gquery = mysqli_fetch_assoc($queryg);

	if(!empty($gquery["nombre_grupo"])){
		$grupo = $gquery["nombre_grupo"];
	}else{
		$grupo = "Todos";
	}

	$id_estatus = $_POST['filtro2'];
		$querye = mysqli_query($conn, "SELECT nombre_estatus FROM estatus WHERE id='$id_estatus'");
		$equery = mysqli_fetch_assoc($querye);

	if(!empty($equery["nombre_estatus"])){
		$estatus = $equery["nombre_estatus"];
	}else{
		$estatus = "Todos";
	}

	if(!empty($_POST['filtro3'])){
		$fecha_desde = date_format(date_create($_POST['filtro3']),'d/m/Y');
	}else{
		$fecha_desde = "";
	}

	if(!empty($_POST['filtro4'])){
		$fecha_hasta = date_format(date_create($_POST['filtro4']),'d/m/Y');
	}else{
		$fecha_hasta = "";
	}

	foreach($queryexcel as $cli_list_param){

						 $id_c = $cli_list_param['id_cliente'];
						 $a_excel1[]= $cli_list_param['nombre'];

						 if($cli_list_param['estatus'] == "si" || $cli_list_param['estatus'] == "Si"){
							$a_excel2[]= 'Activo';
						 }

						 else if($cli_list_param['estatus'] == "no" || $cli_list_param['estatus'] == "No"){
							$a_excel2[]= 'Inactivo';
						}

						else {

								$iestatus="";

								$list_estatus = $cli_list_param['estatus'];
								$getcstatus = mysqli_query($conn, "SELECT * FROM estatus WHERE id = '$list_estatus' AND id_empresa='$id_empresa'");
								while($rgetcstatus = mysqli_fetch_array($getcstatus)){
									$iestatus =$rgetcstatus['nombre_estatus'];
									$a_excel2[]= $iestatus;
								}

						 }



							//consulta el minimo y maximo valor de referencia
							$valor = array();
							$sql30a = mysqli_query($conn,"select valor, naturaleza from referencias t1 where id_cliente='$id_c' and id_parametro='$file' $option_fecha");
							while($refer =mysqli_fetch_array($sql30a)){

							 $nat_ref = $refer['naturaleza'];

							 //procesa el arreglo de acuerdo a la naturaleza

							 if($nat_ref == "hh:mm"){
								 $valor_t = str_replace(':','.',$refer['valor']);
								 $valor [] = (float)$valor_t;
							}

							 else {
								 $valor_t = $refer['valor'];
								 $valor [] = (float)$valor_t;
								}
							}

							 //imprime los valores de minimo y maximo segun la naturaleza
							 if($nat_ref == "hh:mm"){

								 $min_val = number_format(min($valor),2);
								 $max_val = number_format(max($valor),2);
								 $a_excel3 [] = str_replace('.',':',$min_val);
								 $a_excel4 [] = str_replace('.',':',$max_val);

							 }

							 else if($nat_ref == "$"){
								 $a_excel3[]=number_format(min($valor), 2, '.', ',');
								 $a_excel4[]=number_format(max($valor), 2, '.', ',');

							 }

							 else if($nat_ref == "%"){
								 $a_excel3[]=number_format(min($valor), 2, '.', ',').'%';
								 $a_excel4[]=number_format(max($valor), 2, '.', ',').'%';

							 }

							 else {
								 $a_excel3[]= number_format(min($valor), 2, '.', ',');
								 $a_excel4[]= number_format(max($valor), 2, '.', ',');

							 }


							//consulta el promedio de valores
							$sql30b = mysqli_query($conn,"select valor, naturaleza from referencias t1 where id_cliente='$id_c' and id_parametro='$file' $option_fecha");
							$qty = 0;

							//$totaltime = 0;
							$totaltime = array();
							while($prom=mysqli_fetch_array($sql30b)){
							$n_prom = mysqli_num_rows($sql30b);

							if($prom['naturaleza'] == 'hh:mm'){
									$tiempo = str_replace(':','.',$prom['valor']);
									//$totaltime += (float)$tiempo;
									$totaltime[] = $prom['valor'];
									$b_horas = $prom['naturaleza'];
								}

							else {
									$qty += $prom['valor'];
									$b_numero = $prom['naturaleza'];
									}


							}

							if(isset($b_numero)){

							if($b_numero == "$"){
									$a_excel5[]=number_format((float)$qty/$n_prom, 2, '.', ',');
									$a_excel6[]=number_format((float)$qty, 2, '.', ',');
							}
							else if($b_numero == "%"){

								$a_excel5[]=number_format((float)$qty/$n_prom, 2, '.', ',').'%';
								$a_excel6[]= number_format((float)$qty, 2, '.', ',').'%';

								}
							else{
									$a_excel5[]=number_format((float)$qty/$n_prom, 2, '.', ',');
									$a_excel6[]= number_format((float)$qty, 2, '.', ',');

								}
							}

						    if(isset($b_horas)){
								$minutes = 0;
								foreach ($totaltime as $time) {
								list($hour, $minute) = explode(':', $time);
								$minutes += $hour * 60;
                                $minutes += $minute;
								}

								$hours = floor($minutes / 60);
								$minutes -= $hours * 60;

								$total_hour = sprintf('%02d:%02d', $hours, $minutes);
								$t1= str_replace(':','.',$total_hour);
								$t2= (float)$t1/$n_prom;

								$total_hora = sprintf('%02d:%02d', (int) $t1, fmod($t1, 1) * 60);
								$prom_hora =  sprintf('%02d:%02d', (int) $t2, fmod($t2, 1) * 60);

								$a_excel5[]= $prom_hora;
								$a_excel6[]= $total_hora;

							}
	}

	// Rename worksheet
	$titulo = substr($_POST['nombre'], 0, 15);
    $objPHPExcel->getActiveSheet()->setTitle('Indicador_'.$titulo);
	$filename = "Estadistica_indicador_".$_POST['nombre'].".xlsx";


	$valores1 = $a_excel1;
	$valores2 = $a_excel2;
	$valores3 = $a_excel3;
	$valores4 = $a_excel4;
	$valores5 = $a_excel5;
	$valores6 = $a_excel6;
	$valores7 = $a_excel7;


	$styleArray = array(
    'font' => array(
        'bold' => true
				));

$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'Indicador')
			->setCellValue('B1', $_POST['nombre'])
			->setCellValue('A2', 'Grupo')
			->setCellValue('B2', $grupo)
			->setCellValue('C2', 'Estatus')
			->setCellValue('D2', $estatus)
			->setCellValue('E2', 'Fecha desde')
			->setCellValue('F2', $fecha_desde)
			->setCellValue('G2', 'Fecha hasta')
			->setCellValue('H2', $fecha_hasta)
			->setCellValue('A4', 'Cliente')
            ->setCellValue('D4', 'Status')
			->setCellValue('E4', 'Valor+Bajo')
            ->setCellValue('F4', 'Valor+Alto')
			->setCellValue('G4', 'Promedio')
			->setCellValue('H4', 'Suma');


		$objPHPExcel->getActiveSheet()->getStyle("A1")->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle("A2")->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle("C2")->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle("E2")->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle("G2")->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle("A4")->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle("D4")->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle("E4")->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle("F4")->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle("G4")->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle("H4")->getFont()->setBold(true);


      $i = 5; //Numero de fila donde se va a comenzar a rellenar con los valores
	 foreach ($valores1 as $valor1){
     $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$i, $valor1);
     $i++;
     }
	 $i = 5;
	 foreach ($valores2 as $valor2){
     $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$i, $valor2);
     $i++;
     }

	 $i = 5;
	 foreach ($valores3 as $valor3){
     $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.$i, $valor3);
     $i++;
     }
	 $i = 5;
	 foreach ($valores4 as $valor4){
     $objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$i, $valor4);
     $i++;
     }
	 $i = 5;
	 foreach ($valores5 as $valor5){
     $objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$i, $valor5);
     $i++;
     }
	 $i = 5;
	 foreach ($valores6 as $valor6){
     $objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$i, $valor6);
     $i++;
     }



	 $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A'.($i+1), 'Empresa')
            ->setCellValue('B'.($i+1), $empresa)
            ->setCellValue('A'.($i+2), 'Generado por')
            ->setCellValue('B'.($i+2), $usuario)
			->setCellValue('A'.($i+3), 'Fecha')
            ->setCellValue('B'.($i+3), $fecha)
            ->setCellValue('A'.($i+4), 'Hora')
            ->setCellValue('B'.($i+4), $hora);


			$objPHPExcel->getActiveSheet()->getStyle("A".($i+1))->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle("A".($i+2))->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle("A".($i+3))->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle("A".($i+4))->getFont()->setBold(true);


}


if(isset($_POST['exportar_listado_compl'])){ /* Exporta listado de controles */

	// Rename worksheet
	$titulo = substr($_POST['nombre'], 0, 15);
    $objPHPExcel->getActiveSheet()->setTitle('Control_'.$titulo);
	$filename = "Estadistica_cntrl_".$_POST['nombre'].".xlsx";

	//print_r($valores1);exit;
	$empresa = $_POST['empresa'];
	$id_empresa = $_POST['id_excel_empresa'];
	$nusuario = $_POST['usuario'];
	$query = mysqli_query($conn,"select nombre_completo from usuario where id_usuario = '$nusuario'");
	$rquery = mysqli_fetch_assoc($query);
	$usuario = $rquery["nombre_completo"];
	$nat = $_POST['nat'];

	$id_grupo = $_POST['filtro1'];
		$queryg = mysqli_query($conn, "SELECT nombre_grupo FROM grupos WHERE id='$id_grupo'");
		$gquery = mysqli_fetch_assoc($queryg);

	if(!empty($gquery["nombre_grupo"])){
		$grupo = $gquery["nombre_grupo"];
	}else{
		$grupo = "Todos";
	}

	$id_estatus = $_POST['filtro2'];
		$querye = mysqli_query($conn, "SELECT nombre_estatus FROM estatus WHERE id='$id_estatus'");
		$equery = mysqli_fetch_assoc($querye);


	if(!empty($equery["nombre_estatus"])){
		$estatus = $equery["nombre_estatus"];
	}else{
		$estatus = "Todos";
	}

	if(!empty($_POST['filtro3'])){
		$fecha_desde = date_format(date_create($_POST['filtro3']),'d/m/Y');
	}else{
		$fecha_desde = "";
	}

	if(!empty($_POST['filtro4'])){
		$fecha_hasta = date_format(date_create($_POST['filtro4']),'d/m/Y');
	}else{
		$fecha_hasta = "";
	}


	$query_excel = unserialize(base64_decode($_POST['query_excel']));


	foreach($query_excel as $cli_list_compl){

				$id_c = $cli_list_compl['id_cliente'];

						 $ac_excel1[]= $cli_list_compl['nombre'];

						if($cli_list_compl['estatus'] == "si" || $cli_list_compl['estatus'] == "Si"){
							$ac_excel2[]= 'Activo';
						}

						 elseif($cli_list_compl['estatus'] == "no" || $cli_list_compl['estatus'] == "No"){
							$ac_excel2[]= 'Inactivo';
						 }

						 else {
								$cestatus = "";

								$list_estatus = $cli_list_compl['estatus'];
								$getcstatus = mysqli_query($conn, "SELECT * FROM estatus WHERE id = '$list_estatus' AND id_empresa='$id_empresa'");
								while($rgetcstatus = mysqli_fetch_array($getcstatus)){
									$cestatus = $rgetcstatus['nombre_estatus'];
									$ac_excel2[]= $cestatus;
								}


						 }



						 $ac_excel3 []= $cli_list_compl['razon'];


						 $ac_excel4 []= $cli_list_compl['referencia'];


							//imprime referencia de complemento segun su naturaleza
							$c_nat=$cli_list_compl['naturaleza'];
							$com_valor  = $cli_list_compl['valor'];

							if($c_nat == "$"){
								$ac_excel5[]=number_format((float)$com_valor, 2, '.', ',');

							}

							else if($c_nat == "%"){
								$ac_excel5[]=number_format((float)$com_valor, 2, '.', ',').'%';

							}

							else if($c_nat == "nn"){
								$ac_excel5[]=number_format((float)$com_valor, 2, '.', ',');
							}

							else if($c_nat == "d/m/a"){
								$ac_excel5[]=date("d/m/Y",strtotime($com_valor));
							}

							else{
								$ac_excel5[]= $com_valor;
							}


	}


	$valores1= $ac_excel1;
	$valores2 = $ac_excel2;
	$valores3= $ac_excel3;
	$valores4 = $ac_excel4;
	$valores5= $ac_excel5;

	$styleArray = array(
    'font' => array(
        'bold' => true
				));

$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('A1', 'Control')
			->setCellValue('C1', $_POST['nombre'])
            ->setCellValue('A2', 'Grupo')
			->setCellValue('B2', $grupo)
			->setCellValue('C2', 'Estatus')
			->setCellValue('D2', $estatus)
			->setCellValue('E2', 'Fecha desde')
			->setCellValue('F2', $fecha_desde)
			->setCellValue('G2', 'Fecha hasta')
			->setCellValue('H2', $fecha_hasta)
			->setCellValue('A4', 'Cliente')
            ->setCellValue('D4', 'Status')
            ->setCellValue('E4', 'Razón Social')
			->setCellValue('J4', 'Referencia')
            ->setCellValue('L4', 'Valor');


	$objPHPExcel->getActiveSheet()->getStyle("A1")->getFont()->setBold(true);
	$objPHPExcel->getActiveSheet()->getStyle("A2")->getFont()->setBold(true);
	$objPHPExcel->getActiveSheet()->getStyle("C2")->getFont()->setBold(true);
	$objPHPExcel->getActiveSheet()->getStyle("E2")->getFont()->setBold(true);
	$objPHPExcel->getActiveSheet()->getStyle("G2")->getFont()->setBold(true);
	$objPHPExcel->getActiveSheet()->getStyle("A4")->getFont()->setBold(true);
	$objPHPExcel->getActiveSheet()->getStyle("D4")->getFont()->setBold(true);
	$objPHPExcel->getActiveSheet()->getStyle("E4")->getFont()->setBold(true);
	$objPHPExcel->getActiveSheet()->getStyle("J4")->getFont()->setBold(true);
	$objPHPExcel->getActiveSheet()->getStyle("L4")->getFont()->setBold(true);




      $i = 5; //Numero de fila donde se va a comenzar a rellenar con los valores
	 foreach ($valores1 as $valor1){
     $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$i, $valor1);
     $i++;
     }
	 $i = 5;
	 foreach ($valores2 as $valor2){
     $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$i, $valor2);
     $i++;
     }

	 $i = 5;
	 foreach ($valores3 as $valor3){
     $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.$i, $valor3);
     $i++;
     }
	 $i = 5;
	 foreach ($valores4 as $valor4){
     $objPHPExcel->setActiveSheetIndex(0)->setCellValue('J'.$i, $valor4);
     $i++;
     }
	 $i = 5;
	 foreach ($valores5 as $valor5){
     $objPHPExcel->setActiveSheetIndex(0)->setCellValue('L'.$i, $valor5);
     $i++;
     }


	 $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A'.($i+1), 'Empresa')
            ->setCellValue('B'.($i+1), $empresa)
            ->setCellValue('A'.($i+2), 'Generado por')
            ->setCellValue('B'.($i+2), $usuario)
			->setCellValue('A'.($i+3), 'Fecha')
            ->setCellValue('B'.($i+3), $fecha)
            ->setCellValue('A'.($i+4), 'Hora')
            ->setCellValue('B'.($i+4), $hora);


			$objPHPExcel->getActiveSheet()->getStyle("A".($i+1))->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle("A".($i+2))->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle("A".($i+3))->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getStyle("A".($i+4))->getFont()->setBold(true);


}


//Exporta clientes
if(isset($_POST['exportacli'])){

		$opcion_cli= $_POST['exporta_cliente'];
		$id_empresa = $_POST['id_empresa_clientes'];
		$modificado = $_POST['modificado_por'];



		$objPHPExcel->getActiveSheet()->setTitle('Clientes '.$def);
		$filename = "Clientes ".$def.".xlsx";


			  $styleArray = array(
					'font' => array(
					'bold' => true
				));

		$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('A1', 'Clientes')
			->setCellValue('A2', 'Nombre')
            ->setCellValue('B2', 'Grupo de Clientes')
            ->setCellValue('C2', 'Última Modificación')
			->setCellValue('D2', 'Modificado Por')
            ->setCellValue('E2', 'Claves Telefónicas Asignadas')
			->setCellValue('F2', 'Razón Social')
			->setCellValue('G2', 'Calle')
            ->setCellValue('H2', 'Número externo')
            ->setCellValue('I2', 'Número interno')
            ->setCellValue('J2', 'Colonia')
			->setCellValue('K2', 'Municipio / Delegación')
            ->setCellValue('L2', 'Estado')
			->setCellValue('M2', 'Localidad')
			->setCellValue('N2', 'Código Postal')
            ->setCellValue('O2', 'Número RFC')
            ->setCellValue('P2', 'Oficina o N° Membresía')
            ->setCellValue('Q2', 'Teléfono')
			->setCellValue('R2', 'E-Mail')
            ->setCellValue('S2', 'Persona Contacto')
			->setCellValue('T2', 'Exento IVA')
			->setCellValue('U2', 'Facebook')
            ->setCellValue('V2', 'Twitter')
			->setCellValue('W2', 'Tratamiento')
			->setCellValue('X2', 'Atributos')
			->setCellValue('Y2', 'Contestación');

			if($opcion_cli != 0){
				   $spart_inner = "INNER JOIN grupos gr ON c.grupo = gr.id";
					 $spart_where = "WHERE c.id_empresa='$id_empresa' AND gr.id='$opcion_cli'";
			}else{
					$spart_inner = "";
					$spart_where = "WHERE c.id_empresa='$id_empresa'";
			}

      $query_ex1 = mysqli_query($conn,"SELECT * FROM cliente c ".$spart_inner." ".$spart_where);
			$query_ex2 = mysqli_query($conn,"SELECT * FROM general_cliente g INNER JOIN cliente c ON c.id_cliente = g.id_cliente ".$spart_inner." ".$spart_where);
			$query_ex3 = mysqli_query($conn,"SELECT * FROM protocolo_cliente p INNER JOIN cliente c ON c.id_cliente = p.id_cliente ".$spart_inner." ".$spart_where);


				$sngrupo = mysqli_query($conn, "SELECT * FROM grupos WHERE id='$opcion_cli'");
				while($sg = mysqli_fetch_array($sngrupo)){
					$nombre_grupo = $sg['nombre_grupo'];
				}

				$i=3;
				while($ex = mysqli_fetch_array($query_ex1)) {

				  $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$i, $ex['nombre']);
				  $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.$i, $nombre_grupo);
				  $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$i, date('d-M-Y h:i:s'));
				  $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$i, $modificado);
				  $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.$i, $ex['clave_telefono']);
				  $objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$i, $ex['razon']);
				  $objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$i, $ex['rfc']);
			    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$i, $ex['oficina']);

				  $i++;
			  }

			 $i=3;
			  while($ex2 = mysqli_fetch_array($query_ex2)) {
				  $objPHPExcel->setActiveSheetIndex(0)->setCellValue('I'.$i, $ex2['calle']);
				  $objPHPExcel->setActiveSheetIndex(0)->setCellValue('J'.$i, $ex2['externo']);
				  $objPHPExcel->setActiveSheetIndex(0)->setCellValue('K'.$i, $ex2['interno']);
				  $objPHPExcel->setActiveSheetIndex(0)->setCellValue('L'.$i, $ex2['colonia']);
				  $objPHPExcel->setActiveSheetIndex(0)->setCellValue('M'.$i, $ex2['municipio']);
				  $objPHPExcel->setActiveSheetIndex(0)->setCellValue('N'.$i, $ex2['estado']);
				  $objPHPExcel->setActiveSheetIndex(0)->setCellValue('O'.$i, $ex2['localidad']);
				  $objPHPExcel->setActiveSheetIndex(0)->setCellValue('P'.$i, $ex2['postal']);
				  $objPHPExcel->setActiveSheetIndex(0)->setCellValue('Q'.$i, $ex2['telefono']);
				  $objPHPExcel->setActiveSheetIndex(0)->setCellValue('R'.$i, $ex2['email']);
				  $objPHPExcel->setActiveSheetIndex(0)->setCellValue('S'.$i, $ex2['contactos']);
				  $objPHPExcel->setActiveSheetIndex(0)->setCellValue('T'.$i, ' ');
				  $objPHPExcel->setActiveSheetIndex(0)->setCellValue('U'.$i, $ex2['facebook']);
				  $objPHPExcel->setActiveSheetIndex(0)->setCellValue('V'.$i, $ex2['twitter']);
				  $i++;
			  }

			  $i=3;
			  while($ex3 = mysqli_fetch_array($query_ex3)) {
				  $objPHPExcel->setActiveSheetIndex(0)->setCellValue('W'.$i, $ex3['descripcion']);
				  $objPHPExcel->setActiveSheetIndex(0)->setCellValue('X'.$i, $ex3['slogan']);
				  $objPHPExcel->setActiveSheetIndex(0)->setCellValue('Y'.$i, $ex3['contestacion']);
				  $i++;
			  }

		$from = "A1";
		$to = "Z2";
		$objPHPExcel->getActiveSheet()->getStyle("$from:$to")->getFont()->setBold( true );
}


//exporta un cliente al editarlo
if(isset($_POST['exporta_uno'])){

	    $id_empresa = $_POST['id_empresa_cliente'];
		$modificado = $_POST['modificado_por'];
		$nom_cli = $_POST['nombre_cliente'];
		$id_cli = $_POST['id_cliente'];


		$objPHPExcel->getActiveSheet()->setTitle('Cliente '.substr($nom_cli,0,22));
		$filename = "Clientes ".substr($nom_cli,0,22).".xlsx";


			  $styleArray = array(
					'font' => array(
					'bold' => true
				));

		$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('A1', 'Clientes')
			->setCellValue('A2', 'Nombre')
            ->setCellValue('B2', 'Activo')
            ->setCellValue('C2', 'Grupo de Clientes')
            ->setCellValue('D2', 'Última Modificación')
			->setCellValue('E2', 'Modificado Por')
            ->setCellValue('F2', 'Claves Telefónicas Asignadas')
			->setCellValue('G2', 'Razón Social')
			->setCellValue('H2', 'Calle')
            ->setCellValue('I2', 'Número externo')
            ->setCellValue('J2', 'Número interno')
            ->setCellValue('K2', 'Colonia')
			->setCellValue('L2', 'Municipio / Delegación')
            ->setCellValue('M2', 'Estado')
			->setCellValue('N2', 'Localidad')
			->setCellValue('O2', 'Código Postal')
            ->setCellValue('P2', 'Número RFC')
            ->setCellValue('Q2', 'Oficina o N° Membresía')
            ->setCellValue('R2', 'Teléfono')
			->setCellValue('S2', 'E-Mail')
            ->setCellValue('T2', 'Persona Contacto')
			->setCellValue('U2', 'Exento IVA')
			->setCellValue('V2', 'Facebook')
            ->setCellValue('W2', 'Twitter')
			->setCellValue('X2', 'Tratamiento')
			->setCellValue('Y2', 'Atributos')
			->setCellValue('Z2', 'Contestación');

            $query_ex1 = mysqli_query($conn,"SELECT * FROM cliente c WHERE id_cliente='$id_cli' AND id_empresa='$id_empresa'");
			$query_ex2 = mysqli_query($conn,"SELECT * FROM general_cliente g INNER JOIN cliente c ON c.id_cliente = g.id_cliente WHERE  g.id_cliente='$id_cli' AND g.id_empresa='$id_empresa'");
			$query_ex3 = mysqli_query($conn,"SELECT * FROM protocolo_cliente p INNER JOIN cliente c ON c.id_cliente = p.id_cliente WHERE p.id_cliente='$id_cli' AND p.id_empresa='$id_empresa'");

			  $i=3;
			  while($ex = mysqli_fetch_array($query_ex1)) {

				  $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$i, $ex['nombre']);
				  $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.$i, $ex['estatus']);
				  $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$i, $ex['grupo']);
				  $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$i, date('d-M-Y h:i:s'));
				  $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.$i, $modificado);
				  $objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$i, $ex['clave_telefono']);
				  $objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$i, $ex['razon']);
				  $objPHPExcel->setActiveSheetIndex(0)->setCellValue('P'.$i, $ex['rfc']);
			      $objPHPExcel->setActiveSheetIndex(0)->setCellValue('Q'.$i, $ex['oficina']);

				  $i++;
			  }

			 $i=3;
			  while($ex2 = mysqli_fetch_array($query_ex2)) {
				  $objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$i, $ex2['calle']);
				  $objPHPExcel->setActiveSheetIndex(0)->setCellValue('I'.$i, $ex2['externo']);
				  $objPHPExcel->setActiveSheetIndex(0)->setCellValue('J'.$i, $ex2['interno']);
				  $objPHPExcel->setActiveSheetIndex(0)->setCellValue('K'.$i, $ex2['colonia']);
				  $objPHPExcel->setActiveSheetIndex(0)->setCellValue('L'.$i, $ex2['municipio']);
				  $objPHPExcel->setActiveSheetIndex(0)->setCellValue('M'.$i, $ex2['estado']);
				  $objPHPExcel->setActiveSheetIndex(0)->setCellValue('N'.$i, $ex2['localidad']);
				  $objPHPExcel->setActiveSheetIndex(0)->setCellValue('O'.$i, $ex2['postal']);
				  $objPHPExcel->setActiveSheetIndex(0)->setCellValue('R'.$i, $ex2['telefono']);
				  $objPHPExcel->setActiveSheetIndex(0)->setCellValue('S'.$i, $ex2['email']);
				  $objPHPExcel->setActiveSheetIndex(0)->setCellValue('T'.$i, $ex2['contactos']);
				  $objPHPExcel->setActiveSheetIndex(0)->setCellValue('U'.$i, ' ');
				  $objPHPExcel->setActiveSheetIndex(0)->setCellValue('V'.$i, $ex2['facebook']);
				  $objPHPExcel->setActiveSheetIndex(0)->setCellValue('W'.$i, $ex2['twitter']);
				  $i++;
			  }

			  $i=3;
			  while($ex3 = mysqli_fetch_array($query_ex3)) {
				  $objPHPExcel->setActiveSheetIndex(0)->setCellValue('X'.$i, $ex3['descripcion']);
				  $objPHPExcel->setActiveSheetIndex(0)->setCellValue('Y'.$i, $ex3['slogan']);
				  $objPHPExcel->setActiveSheetIndex(0)->setCellValue('Z'.$i, $ex3['contestacion']);
				  $i++;
			  }

		$from = "A1";
		$to = "Z2";
		$objPHPExcel->getActiveSheet()->getStyle("$from:$to")->getFont()->setBold( true );
}


// Set document properties
$objPHPExcel->getProperties()->setCreator("BRM")
							 ->setLastModifiedBy("BRM");



// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header("Content-Disposition: attachment; filename=\"$filename\"");
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 2005 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
ob_end_clean();
$objWriter->save('php://output');
?>

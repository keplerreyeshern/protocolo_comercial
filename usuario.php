<?php 

if(isset($_GET["empresa"])){
$id_empresa = $_GET["empresa"];
	}
	else {
				$id_empresa = 1;}	

    $query_admin = mysqli_query ($conn,"select * from usuario where id_empresa = '1'");
	while($row = mysqli_fetch_array($query_admin)){
		$nombre_ad = $row["nombre"];
		$nombre_com_ad = $row["nombre_completo"];
		$email_ad = $row["email"];
		$pass_ad = $row["password"];
	
		}
    $query_user = mysqli_query ($conn,"select * from usuario where id_usuario = '$id_usuario'");
	while($row = mysqli_fetch_array($query_user)){
		$nombre = $row["nombre"];
		$nombre_com = $row["nombre_completo"];
		$email = $row["email"];
		$pass = $row["password"];
	
		}	
?>
<!-- MODAL ACTUALIZAR USUARIO MAESTRO-->
<div id="modal_up_admin" class="modal fade" role="dialog">
<div class="modal-dialog">

<div class="modal-content">

<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">Actualizar usuario maestro</h4> 
</div>

<div class="modal-body">
<div class="row">
<form id="up_admin" method="post" action="accion_usuario.php">
 <div class="col-md-6">

<div class="form-group">
 <label class="col-lg-3 control-label">Datos</label>
<input type="text" class="form-control" name="username" placeholder="Nombre de usuario" value="<?php echo $nombre_ad;?>" />
</div>

<div class="form-group">
<input type="email" class="form-control" name="email" placeholder="Email" value="<?php echo $email_ad;?>"/>
</div>
</div>

  <div class="col-md-6">
  <label class="col-lg-10 control-label">Cambiar Password</label>
<div class="form-group">
<input type="password" class="form-control" name="password" placeholder="Nuevo password" value="<?php echo $pass_ad;?>"/>
</div>

<div class="form-group">
<input type="password" class="form-control" name="confirmPassword" placeholder="Confirma nuevo password" value="<?php echo $pass_ad;?>" />
</div>

</div>
</div>
</div>
<div class="modal-footer">
<div class="form-group">
<div class="col-lg-9 col-lg-offset-3">
<input type="hidden" name="id_empresa" value="<?php echo $id_empresa;?>"/>
<button type="submit" class="btn btn-primary" name="up_admin"><i class="glyphicon glyphicon-check"></i> Guardar</button>
<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
</form>
</div>
</div>

</div>
</div>
</div>
</div>



<!-- MODAL ACTUALIZAR USUARIO DESDE PANEL DE USUARIO-->
<div id="modal_up_user" class="modal fade" role="dialog">
<div class="modal-dialog">

<div class="modal-content">

<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">Actualizar usuario</h4> 
</div>

<div class="modal-body">
<div class="row">
<form id="up_user" method="post" action="accion_usuario.php">
  <div class="col-md-8">
  <label class="col-lg-10 control-label">Cambiar Password</label>
<div class="form-group">
<input type="password" class="form-control" name="password" placeholder="Nuevo password" value="<?php echo $pass;?>"/>
</div>

<div class="form-group">
<input type="password" class="form-control" name="confirmPassword" placeholder="Confirma nuevo password" value="<?php echo $pass;?>" />
</div>

</div>
</div>
</div>
<div class="modal-footer">
<div class="form-group">
<div class="col-lg-9 col-lg-offset-3">
<input type="hidden" name="id_usuario" value="<?php echo $id_usuario;?>"/>
<input type="hidden" name="cliente" value="<?php if(isset($_GET["cl"])){echo $_GET["cl"];}?>"/>
<input type="hidden" name="carpeta" value="<?php if(isset($_GET["carp"])){echo $_GET["carp"];}?>"/>
<button type="submit" class="btn btn-primary" name="up_user"><i class="glyphicon glyphicon-check"></i> Guardar</button>

<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
</form>
</div>
</div>

</div>
</div>
</div>
</div>


<script type="text/javascript">

$(function () {

   $('#up_admin').bootstrapValidator({
        message: 'This value is not valid',
        fields: {
			password: {
                validators: {
                    notEmpty: {
                        message: 'La contraseña no puede estar vacía'
                    },
					identical: {
                        field: 'confirmPassword',
                        message: 'La contraseña y su confirmación no son iguales'
                    },
                   
                    different: {
                        field: 'username',
                        message: 'La contraseña y el nombre de usuario no pueden ser iguales'
                    }
                }
		    },
			 
		}
    });
});		
	
$(function () {
	$('#up_user').bootstrapValidator({
        message: 'This value is not valid',
        fields: {
			password: {
                validators: {
                    notEmpty: {
                        message: 'La contraseña no puede estar vacía'
                    },
					identical: {
                        field: 'confirmPassword',
                        message: 'La contraseña y su confirmación no son iguales'
                    },
                   
                    different: {
                        field: 'username',
                        message: 'La contraseña y el nombre de usuario no pueden ser iguales'
                    }
                }
		    },
		}
    });
});		

$(function () {

	$('#up_user_admin').bootstrapValidator({
        message: 'This value is not valid',
        fields: {
			password: {
                validators: {
                    notEmpty: {
                        message: 'La contraseña no puede estar vacía'
                    },
					identical: {
                        field: 'confirmPassword',
                        message: 'La contraseña y su confirmación no son iguales'
                    },
                   
                    different: {
                        field: 'username',
                        message: 'La contraseña y el nombre de usuario no pueden ser iguales'
                    }
                }
		    },
		}
    });
});		
</script>
<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> 
<html class="no-js" lang=""> <!--<![endif]-->
    <head>
       <meta http-equiv="Content-Type" content="text/html; charset=utf-8"> 
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>BRM</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">

        <!-- Bootstrap -->
   
    <!-- Include the FontAwesome CSS if you want to use feedback icons provided by FontAwesome -->
    <!--<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" />-->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/formValidation.css">
    <link rel="stylesheet" href="css/bootstrapValidator.css">
	 <link rel="stylesheet" href="css/estilos.css">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	 <link rel="stylesheet" href="css/jquery-ui.css">
	 <link rel="stylesheet" href="css/jquery-ui-timepicker-addon.css">
	 
    <script type="text/javascript" src="js/jquery.min.js"></script>
	 <script type="text/javascript" src="js/jquery-ui.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/formValidation.js"></script>
	 <script type="text/javascript" src="js/formValidation.min.js"></script>
	<script type="text/javascript" src="js/bootstrapValidator.js"></script>
	 <script type="text/javascript" src="js/jquery.tablesorter.min.js"></script>
	<script type="text/javascript" src="js/masked-plugin.js"></script>
	<script type="text/javascript" src="js/jquery-ui-timepicker-addon.js"></script>
	<script type="text/javascript" src="js/jquery-dateformat.min.js"></script>
	
		
<script language="JavaScript">
function relojillo()
{
	fecha = new Date()
	hora = fecha.getHours()
	minuto = fecha.getMinutes()
	segundo = fecha.getSeconds()
	horita = hora + ":" + minuto + ":" + segundo
	document.getElementById('horeja').innerHTML = horita
	setTimeout('relojillo()',1000)
}
</script>
    </head>
<?php 
	$tipo = null;
	$cliente =null;
	$id = null;
	$id_doc = null;

	
	if (isset ($_GET['cl']) && isset($_GET['file'])) {
		$tipo = $_GET['file'];
		$cliente = $_GET['cl'];
	
	$query_cli_up1 = mysqli_query($conn,"select * from cliente where id_cliente = '$cliente'");
	while($qcup1 = mysqli_fetch_array($query_cli_up1)){
		$id_cli = $qcup1['id_cliente'];
		$id_empr = $qcup1['id_empresa'];
		$nombre = $qcup1['nombre'];
		$grupo = $qcup1['grupo'];
		$razon = $qcup1['razon'];
		$rfc = $qcup1['rfc'];
		$oficina = $qcup1['oficina'];
		$clave = $qcup1['clave_telefono'];
		$status = $qcup1['estatus'];
	}
	$query_cli_up2 = mysqli_query($conn,"select * from general_cliente where id_cliente = '$cliente'");
	while($qcup2 = mysqli_fetch_array($query_cli_up2)){
		$calle = $qcup2['calle'];
		$interno = $qcup2['interno'];
		$externo = $qcup2['externo'];
		$colonia = $qcup2['colonia'];
		$municipio = $qcup2['municipio'];
		$estado = $qcup2['estado'];
		$localidad = $qcup2['localidad'];
		$postal = $qcup2['postal'];
		$contactos = $qcup2['contactos'];
		$telefono = $qcup2['telefono'];
		$email = $qcup2['email'];
		$facebook= $qcup2['facebook'];
		$twitter = $qcup2['twitter'];
	
	}
	
		$query_cli_up3 = mysqli_query($conn,"select * from protocolo_cliente where id_cliente = '$cliente'");
		while($qcup3 = mysqli_fetch_array($query_cli_up3)){
		$descripcion = $qcup3['descripcion'];
		$slogan = $qcup3['slogan'];
		$contes = $qcup3['contestacion'];

	}
		
	    $q_emp = mysqli_query($conn,"select * from empresa where id_empresa!=1 and id_empresa!='$id_empr'");
		
		
		
		$query_cli_up4 = mysqli_query($conn,"SELECT * FROM relacion_clientes WHERE cliente_hijo = '$cliente'");
		while($qcup4 = mysqli_fetch_array($query_cli_up4)){
		
		$cliente_padre = $qcup4['cliente_padre'];
		
	}
	}

	
?>

<!--- MODAL DE CLIENTES ACTUALIZAR-->		
<!-- Modal -->
<div id="modal_cli_up1" class="modal fade" role="dialog">
  <div class="modal-dialog modal-cliente-update">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Actualizar Datos de Cliente</h4>
      </div>
      <div class="modal-body">
         <form class="cliente" name="contact" enctype="multipart/form-data" id="cliente" method="post" action="accion_cliente.php">
		 
		  <div class="row">
		 <div class="col-md-3">
		 <fieldset>
		  <legend class="the-legend">General</legend>
		  <div class="form-group">
			 <input id="nombre" type="text" name="nombre" class="form-control" value="<?php if(isset($nombre)){echo $nombre;}?>" placeholder="Nombre">
			 <div id="Info"></div>
		</div>

         <div class="form-group">		
			<input type="text" name="razon" class=" form-control" value="<?php if(isset($razon)){echo $razon;}?>" placeholder="Razón">
		</div>
         <div class="form-group">		
			<input type="text" name="rfc" class=" form-control" value="<?php if(isset($rfc)){echo $rfc;}?>" placeholder="Número RFC">
		</div>
         <div class="form-group">		
           <input type="text" name="oficina" class=" form-control" value="<?php if(isset($oficina)){echo $oficina;}?>" placeholder="Oficina">
		</div>
		 <div class="form-group">	
        <input type="text" name="clave" class="form-control" value="<?php if(isset($clave)){echo $clave;}?>" placeholder="Claves Telefónicas"><br>	
        </div>		
		
		 <div class="form-group">	
			 <label>Grupos</label>
			 <select name="grupo" class="form-control" id="up_selectgrupo">	
				<option value="0" selected>-- Seleccionar --</selected>
				<?php 
			
				$upcgrupo = mysqli_query($conn, "SELECT * FROM grupos WHERE id_empresa='$id_empr'");								
				
				while($rupcgrupo = mysqli_fetch_array($upcgrupo)){ 
				
				if(!empty($rupcgrupo['nombre_grupo'])){ 
						
				if($rupcgrupo['id']==$grupo){ ?>
						<option value="<?php echo $rupcgrupo['id'];?>" selected><?php echo $rupcgrupo['nombre_grupo'];?></option> 
				<?php } else{ ?>
						<option value="<?php echo $rupcgrupo['id'];?>"><?php echo $rupcgrupo['nombre_grupo'];?></option> 
				  <?php } } }  ?>

			</select>
		</div>
		
		 <div class="form-group">		
			<label for="status">Estatus</label>
			<select name="status" id="up_selectestatus" class="form-control">
			<?php 
		
			$upcstatus = mysqli_query($conn, "SELECT * FROM estatus WHERE id_grupo = '$grupo' AND id_empresa='$id_empr'");								
			
			while($rupcstatus = mysqli_fetch_array($upcstatus)){ 
			
			if(!empty($rupcstatus['nombre_estatus'])){ 
					
			if($rupcstatus['id']==$status){ ?>
				    <option value="<?php echo $rupcstatus['id'];?>" selected><?php echo $rupcstatus['nombre_estatus'];?></option> 
			<?php } else{ ?>
					<option value="<?php echo $rupcstatus['id'];?>"><?php echo $rupcstatus['nombre_estatus'];?></option> 
			  <?php } } }  ?>
			  </select>			
		</div>		
		
			<?php	if($tipo_usuario != 2){ ?>
		<div class="form-group">		
			<label for="status">Cliente Relacionado</label>
			<select name="upcliente_padre" class="form-control">
				<option value="" selected>-- Seleccionar -- </option>	
			<?php $uplclientes = mysqli_query($conn,"SELECT * FROM cliente WHERE id_empresa='$id_empr'");
			while($rlclientes = mysqli_fetch_array($uplclientes)){ 
					if($rlclientes['id_cliente'] == $cliente_padre){?>				
						<option value="<?php echo $rlclientes['id_cliente'];?>" selected><?php echo $rlclientes['nombre'];?></option>	
					<?php }else{	?>
						<option value="<?php echo $rlclientes['id_cliente'];?>"><?php echo $rlclientes['nombre'];?></option>						
					<?php }	
					} ?>
			</select>			
		</div>
				
		
		 <div class="form-group">
         <input type="hidden" name="empresa_cliente" value="<?php echo $id_empr;?>"/>		 
        <label for="empresa_grupo">Cambiar de empresa</label></br>
		<select class="form-control" name="empresa_grupo">
		<option value="">Seleccionar</option>
		
		<?php While ($emp =mysqli_fetch_array($q_emp)){
				          
						  $p_rel = $emp['id_empresa'];
						  $q_rel = mysqli_query($conn,"SELECT * FROM relacion_empresas WHERE (empresa_a ='$id_empr' OR empresa_b='$id_empr') AND (empresa_a='$p_rel' OR empresa_b='$p_rel')");
						  
					  if(mysqli_num_rows($q_rel) != 0){ ?>
						  
						  <option value="<?php echo $p_rel?>"><?php echo $emp['nombre_empresa'];?></option>
					  
					  <?php }
			}?>
		</select>
        </div>
	<?php } ?>	
        </fieldset>
		</div>
		<div class="form-group col-md-3">
		 <fieldset>
		  <legend class="the-legend">Localización</legend>
		     
			 <input type="text" name="calle" class="form-control"  value="<?php if(isset($calle)){echo $calle;}?>"placeholder="Calle"><br>
            <input type="text" name="interno" class="form-control" value="<?php if(isset($interno)){echo $interno;}?>"placeholder="Número Interno"><br>
			 <input type="text" name="externo" class="form-control" value="<?php if(isset($externo)){echo $externo;}?>"placeholder="Número Externo"><br>
            <input type="text" name="colonia" class="form-control" value="<?php if(isset($colonia)){echo $colonia;}?>"placeholder="Colonia"><br>
			<input type="text" name="municipio" class=" form-control" value="<?php if(isset($municipio)){echo $municipio;}?>" placeholder="Municipio"><br>
			<input type="text" name="localidad" class=" form-control" value="<?php if(isset($localidad)){echo $localidad;}?>"placeholder="Localidad"><br>	
     		<input type="text" name="estado" class=" form-control"value="<?php if(isset($estado)){echo $estado;}?>" placeholder="Estado"><br>
			<input type="text" name="postal" class="form-control" value="<?php if(isset($postal)){echo $postal;}?>"placeholder="Código Postal">
			
			
         </fieldset>
		</div>
		
				<div class="form-group col-md-3">
		 <fieldset>
		  <legend class="the-legend">Contacto</legend>
		  	<input type="text" name="contacto" class=" form-control" value="<?php if(isset($contactos)){echo $contactos;}?>" placeholder="Persona Contacto"></br>
			 <input type="text" name="telefono" class="form-control" value="<?php if(isset($telefono)){echo $telefono;}?>" placeholder="Teléfono"><br>
            <input type="email" name="email" class="form-control" value="<?php if(isset($email)){echo $email;}?>"placeholder="Email"><br>
			 <input type="text" name="facebook" class="form-control" value="<?php if(isset($facebook)){echo $facebook;}?>" placeholder="Facebook"><br>
            <input type="textl" name="twitter" class="form-control" value="<?php if(isset($twitter)){echo $twitter;}?>" placeholder="Twitter Sin @"><br>
			
         </fieldset>
		</div> 
			<div class="form-group col-md-3">
			 <fieldset>
		  <legend class="the-legend">Protocolo</legend>
		    <textarea name="descripcion" class="form-control" rows="4" placeholder="Descripción de tratamiento a clientes"><?php if(isset($descripcion)){echo $descripcion;}?></textarea></br>
			<textarea name="slogan" class="form-control" rows="4" placeholder="Atributos"><?php if(isset($slogan)){echo $slogan;}?></textarea></br>
            <textarea name="contestacion" class="form-control" rows="4" placeholder="Contestación"><?php if(isset($contes)){echo $contes;}?></textarea></br>
			</fieldset>
		</div>
        </div>
		</div>
		
	    <div class="modal-footer">
	  	<div class="col-md-6 text-left col-md-offset-8">
		  <input type="hidden" value="<?php echo $cliente; ?>"  name="id">
		  <input type="hidden" value="<?php echo $_GET['carp']; ?>"  name="carp">
		  <input type="hidden" value="<?php echo $_GET['file']; ?>"  name="file">
	     <button class="btn btn-primary" type="submit"  name="cli_up1"><i class="glyphicon glyphicon-check"></i> Guardar</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
		 </form>
		<form method="post" action="exportar.php" class="inline-forms">
		  <input type="hidden" name="id_empresa_cliente" value="<?php echo $id_empr;?>"/>
		  <input type="hidden" name="modificado_por" value="<?php echo $_SESSION['nombre_usuario'];?>"/>
		  <input type="hidden" name="nombre_cliente" value="<?php echo $nombre;?>"/>
		  <input type="hidden" name="id_cliente" value="<?php echo $id_cli;?>"/>
		  <button type="submit" class="btn btn-success" name="exporta_uno"> 
		  <i class="glyphicon glyphicon-export"></i>Exportar</button>
		  </form>
		</div>
		
	</div>
</div>
</div>
</div>


<!-- MODAL CLIENTES ACTUALIZAR 2-->
<div id="modal_cli_up2" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
	 <div class="modal-header">
	 <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Actualizar Localización de Cliente</h4>
	</div>
	 <div class="modal-body">
         <form class="cliente" name="contact" enctype="multipart/form-data" id="cliente" method="post" action="accion_cliente.php">
		 <div class="row">
		<div class="form-group col-md-6">
		 
		     
			 <input type="text" name="calle" class="form-control"  value="<?php if(isset($calle)){echo $calle;}?>"placeholder="Calle"><br>
            <input type="text" name="interno" class="form-control" value="<?php if(isset($interno)){echo $interno;}?>"placeholder="Número Interno"><br>
			 <input type="text" name="externo" class="form-control" value="<?php if(isset($externo)){echo $externo;}?>"placeholder="Número Externo"><br>
            <input type="text" name="colonia" class="form-control" value="<?php if(isset($colonia)){echo $colonia;}?>"placeholder="Colonia"><br>
			<input type="text" name="municipio" class=" form-control" value="<?php if(isset($municipio)){echo $municipio;}?>" placeholder="Municipio"><br>
			<input type="text" name="estado" class=" form-control"value="<?php if(isset($estado)){echo $estado;}?>" placeholder="Estado"><br>
			<input type="text" name="localidad" class=" form-control" value="<?php if(isset($localidad)){echo $localidad;}?>"placeholder="Localidad"><br>
		
        
		</div>
		
				<div class="form-group col-md-6">
		 
		     <input type="text" name="postal" class="form-control" value="<?php if(isset($postal)){echo $postal;}?>"placeholder="Código Postal"><br>
			<input type="text" name="contacto" class=" form-control" value="<?php if(isset($contactos)){echo $contactos;}?>" placeholder="Persona Contacto"></br>
			 <input type="text" name="telefono" class="form-control" value="<?php if(isset($telefono)){echo $telefono;}?>" placeholder="Teléfono"><br>
            <input type="email" name="email" class="form-control" value="<?php if(isset($email)){echo $email;}?>"placeholder="Email"><br>
			 <input type="text" name="facebook" class="form-control" value="<?php if(isset($facebook)){echo $facebook;}?>" placeholder="Facebook"><br>
            <input type="textl" name="twitter" class="form-control" value="<?php if(isset($twitter)){echo $twitter;}?>" placeholder="Twitter Sin @"><br>

		</div>
		</div>
		</div>
      <div class="modal-footer">
	  	<div class="col-md-5 text-left col-md-offset-7">
		<input type="hidden" value="<?php echo $cliente; ?>"  name="id">
	     <button class="btn btn-primary" type="submit"  name="cli_up2"><i class="glyphicon glyphicon-check"></i> Guardar</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
		 </form>
		</div>
      </div>
	     
    </div>

  </div>
</div>


<!--- MODAL CLIENTES ELIMINAR-->		
<!-- Modal -->
<div id="modal_cli_del" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Eliminar Cliente</h4>
      </div>
      <div class="modal-body">
         <form class="cliente" name="contact" enctype="multipart/form-data" id="cliente" method="post" action="accion_cliente.php">
		 <div class="row">
		 <div class="col-md-12">
	
	     <label>¿Esta seguro que desea eliminar este cliente?</label>
		  <label>Se eliminaran todos los datos y archivos relacionados</label>
		
		</div>
		
		</div>
		
	</div>
      <div class="modal-footer">
	  	<div class="col-md-6 text-left col-md-offset-8">
		<input type="hidden" value="<?php echo $cliente;?>" name="id">
	     <button class="btn btn-primary" type="submit" name="cli_del"><i class="glyphicon glyphicon-remove"></i> Eliminar</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
		 </form>
		</div>
		</div>
		</div>
		</div>
		</div>		


<!-- MODAL DOCUMENTOS-->
<!-- Modal eliminar documentos -->
<div id="modal_doc_del" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Eliminar Documento</h4>
      </div>
      <div class="modal-body">
         <form class="cliente" name="contact" enctype="multipart/form-data" id="cliente" method="post" action="accion_cliente.php">
		 <div class="row">
		 <div class="col-md-12">
	
	     <label>¿Esta seguro que desea eliminar este documento?</label>
		
		</div>
		
		</div>
		
	</div>
      <div class="modal-footer">
	  	<div class="col-md-6 text-left col-md-offset-8">
		<input type="hidden" value="<?php echo $cliente;?>" name="id">
	     <button class="btn btn-primary" type="submit" name="doc_del"><i class="glyphicon glyphicon-remove"></i> Eliminar</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
		 </form>
		</div>
		</div>
		</div>
		</div>
		</div>	

<!-- MODAL AGREGAR DOCUMENTOS-->		
<div id="modal_docs_up" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Agregar Documentos</h4>
      </div>
	 <div class="modal-body"> 

	 <div class="row">
	<div  class="col-md-8">
	   <form class="cliente" name="contact" enctype="multipart/form-data" id="cliente" method="post" action="accion_cliente.php">
	<div class="input-group">
                <span class="input-group-btn">
                    <span class="btn btn-default btn-file">
					<span class="glyphicon glyphicon-folder-open"></span>
                    <b>&nbsp;Doc.</b>                   
						<input type="file" name="doc[]" id="doc" multiple="true"/>
                    </span>
                </span>
                <input type="text" class="form-control" readonly>
            </div>
			</div>

			<input type="hidden" value="<?php if(isset($id_cli)){echo $id_cli;}?>"  name="id_cli">
			<input type="hidden" value="<?php if(isset($id_empr)){echo $id_empr;}?>"  name="id_empr">
		 <button class="btn btn-primary" type="submit" value="Guardar" name="cli_docs"><i class="glyphicon glyphicon-check"></i> Guardar</button>
          </form>

</div>		  
</div>	

 <div class="modal-footer">
	  	<div class="col-md-6 text-left col-md-offset-10">	    
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
		
		</div>
      </div>
</div>
</div>	  
</div>

<script>

	$("#up_selectgrupo").change(function(e){
		
       
        var idgrupo = $(this).val();        

        $.ajax({
            type: "POST",
            url: "accion_cliente.php",
            data: {idgrupo:idgrupo},
            success: function(data) {                
				$('#up_selectestatus').html(data);
            }
        });
			
	});
</script>	